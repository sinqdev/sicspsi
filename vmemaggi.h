/**
 * This file contaisn some data structure definitions and constants 
 * for the VME-MAGII program. For more information see vmemaggi.c
 *
 * copyright: GPL
 *
 * Mark Koennecke, March 2008
 */
#ifndef VMEMAGGI
#define VMEMAGGI

#define SLOTBASE 0x1000         /* change when Hitec in different VME slot */
#define MAPREG   0x424
/**
 * VME has several completely different address ranges:
 * one 16 bit wide accessed through device node vme4l_a16d16
 * one 24 bit wide accessed through device node vme4l_a24d16
 * The sharc lives in the address range of the 24 bit bus.
 */
#define VMEDEV "/dev/vme4l_a16d16"
#define MODSIZE 0x100

typedef volatile unsigned char REG8BIT;
typedef volatile unsigned short REG16BIT;

struct PSCreg {
  REG16BIT stat_and_addr;
  REG16BIT data_hi;
  REG16BIT data_lo;
  REG16BIT not_used;
};

/* Bits of return status of the stat_and_addr word */
#define PSC_REPLY_ERROR               0x20000
#define PSC_STATUS_REPLY_READFLAG      0x8000
#define PSC_STATUS_REPLY_FLAG_MISMATCH 0x4000
#define PSC_STATUS_REPLY_BUFFERFULL    0x2000
#define PSC_STATUS_REPLY_DSP_STOPPED   0x1000   /* disabled */
#define PSC_STATUS_REPLY_PS_LOCAL      0x0800
#define PSC_STATUS_REPLY_LINK_DOWN     0x0400
#define PSC_STATUS_REPLY_ADDR_MISMATCH 0x0080
#define PSC_STATUS_REPLY_DATA_MISMATCH 0x0040
#define PSC_STATUS_REPLY_SOFTTIMEOUT   0x0020
#define PSC_STATUS_REPLY_FPGATIMEOUT   0x0010
#define PSC_STATUS_REPLY_STRLEN           256

/* error codes */
#define BADSLOT  70
#define BADMAP   71
#define DSPSTOP  72
#define LINKDWN  73
#define SOFTTIM  74
#define FPGATIM  75
#define ADDRMIS  76
#define MISPS    77
#define PSRNG    78
#define NOPAR    79
#define NOVAL    80
#define BADPAR   81
#define SETRNG   82

#endif
