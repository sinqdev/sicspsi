/*------------------------------------------------------------------------
                      E L 7 5 5 D R I V
  
  A environment control driver for the EL755 magnet 
  controller.

  Mark Koennecke, November 1999 
   
   copyright: see copyright.h
---------------------------------------------------------------------------*/
#ifndef EL755DRIV
#define EL755DRIV
pEVDriver CreateEL755Driv(int argc, char *argv[]);

#endif
