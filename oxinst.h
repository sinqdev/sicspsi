/*---------------------------------------------------------------------------
oxinst.h

Communication routines for Oxford Instruments equipment
  
Markus Zolliker, March 2005
----------------------------------------------------------------------------*/

#ifndef OXINST_H
#define OXINST_H

#include "ease.h"

int OxiHandler(void *eab);
double OxiGet(EaseBase * eab, int dig, int *pdig, double old);
void OxiSet(EaseBase * eab, char *cmd, double val, int dig);

/* usage of the syntax field of EaseBase:
  syntax <= 0: old syntax, in general without decimal point
  syntax >  0: new syntax, in general decimal point given
  
  syntax <= -8: error correction on input (ascii codes >= 96 are corrected)
*/

#endif
