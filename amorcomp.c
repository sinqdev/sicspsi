/*---------------------------------------------------------------------
 AMOR component handling module. For the new (2005) calculation of the
 positions using the beam height as zero.
 
 copyright: see file COPYRIGHT
 
 Mark Koennecke, October  2005
-----------------------------------------------------------------------*/
#include "amorcomp.h"
#define ABS(x) (x < 0 ? -(x) : (x))

double calcCompPosition(pamorComp comp)
{
  return ABS(comp->scaleOffset - comp->markOffset - comp->readPosition);
}

/*-------------------------------------------------------------------*/
int handleCompCommand(pamorComp comp, SConnection * pCon,
                      int argc, char *argv[])
{
  char pBueffel[512];

  if (argc < 3) {
    SCWrite(pCon, "ERROR: not enough arguments", eError);
    return 0;
  }
  strtolower(argv[2]);
  if (strcmp(argv[2], "list") == 0) {
    snprintf(pBueffel, 511,
             "%s %s active = %d\n%s %s offset = %f\n%s %s scaleoffset = %f\n%s %s read = %f\n%s %s calc = %f\n",
             argv[0], argv[1], comp->activeFlag,
             argv[0], argv[1], comp->markOffset,
             argv[0], argv[1], comp->scaleOffset,
             argv[0], argv[1], comp->readPosition,
             argv[0], argv[1], calcCompPosition(comp));
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  } else if (strcmp(argv[2], "active") == 0) {
    if (argc > 3) {
      if (!SCMatchRights(pCon, usUser)) {
        return 0;
      }
      SCparChange(pCon);
      comp->activeFlag = atoi(argv[3]);
      SCSendOK(pCon);
      return 1;
    } else {
      snprintf(pBueffel, 511, " %s %s active = %d",
               argv[0], argv[1], comp->activeFlag);
      SCWrite(pCon, pBueffel, eValue);
      return 1;
    }
  } else if (strcmp(argv[2], "offset") == 0) {
    if (argc > 3) {
      if (!SCMatchRights(pCon, usUser)) {
        return 0;
      }
      SCparChange(pCon);
      comp->markOffset = atof(argv[3]);
      SCSendOK(pCon);
      return 1;
    } else {
      snprintf(pBueffel, 511, " %s %s offset = %f",
               argv[0], argv[1], comp->markOffset);
      SCWrite(pCon, pBueffel, eValue);
      return 1;
    }
  } else if (strcmp(argv[2], "scaleoffset") == 0) {
    if (argc > 3) {
      if (!SCMatchRights(pCon, usUser)) {
        return 0;
      }
      comp->scaleOffset = atof(argv[3]);
      SCparChange(pCon);
      SCSendOK(pCon);
      return 1;
    } else {
      snprintf(pBueffel, 511, " %s %s scaleoffset = %f",
               argv[0], argv[1], comp->scaleOffset);
      SCWrite(pCon, pBueffel, eValue);
      return 1;
    }
  } else if (strcmp(argv[2], "read") == 0) {
    if (argc > 3) {
      if (!SCMatchRights(pCon, usUser)) {
        return 0;
      }
      comp->readPosition = atof(argv[3]);
      SCparChange(pCon);
      SCSendOK(pCon);
      return 1;
    } else {
      snprintf(pBueffel, 511, " %s %s read = %f",
               argv[0], argv[1], comp->readPosition);
      SCWrite(pCon, pBueffel, eValue);
      return 1;
    }
  } else if (strcmp(argv[2], "calc") == 0) {
    snprintf(pBueffel, 511, " %s %s calc = %f",
             argv[0], argv[1], calcCompPosition(comp));
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  } else {
    snprintf(pBueffel, 511, "ERROR: subcommand %s to %s %s not understood",
             argv[2], argv[0], argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  return 1;
}

/*------------------------------------------------------------------------*/
int saveAmorComp(FILE * fd, char *name, char *compname, pamorComp comp)
{
  fprintf(fd, "%s %s active %d\n", name, compname, comp->activeFlag);
  fprintf(fd, "%s %s offset %f\n", name, compname, comp->markOffset);
  fprintf(fd, "%s %s scaleoffset %f\n", name, compname, comp->scaleOffset);
  fprintf(fd, "%s %s read %f\n", name, compname, comp->readPosition);
  return 1;
}
