
#line 64 "sinqhmdriv.w"

/*--------------------------------------------------------------------------
                         S I N Q H M 

  A driver for the SINQ histogram memory.

  Mark Koennecke, April 1997

  copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef SINQHMDRIVER
#define SINQHMDRIVER
#include "hardsup/sinqhm.h"

#line 22 "sinqhmdriv.w"

      typedef struct __SinqHMDriv {
                                    pCounter pCounter;
                                    pSINQHM  pMaster;
                                    int iLastHMError;
                                    int iLastCTError;
                                    HistMode eHistMode;
                                    int iBinWidth;
                                    OverFlowMode eFlow;
                                    int extraDetector;
                                   } SinqHMDriv;

#line 77 "sinqhmdriv.w"

/*-------------------------------------------------------------------------*/

#line 59 "sinqhmdriv.w"

   pHistDriver CreateSINQDriver(pStringDict pOption);
   int isSINQHMDriv(pHistDriver test);

#line 79 "sinqhmdriv.w"


#endif
