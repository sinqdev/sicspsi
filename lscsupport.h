/*---------------------------------------------------------------------------
lscsupport.h

Communication routines for LakeShore equipment
  
Markus Zolliker, July 2006
----------------------------------------------------------------------------*/

#ifndef LSCSUPPORT_H
#define LSCSUPPORT_H

#include "ease.h"

int LscHandler(void *eab);

#endif
