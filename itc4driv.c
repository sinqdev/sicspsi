/*--------------------------------------------------------------------------
			     I T C 4 D R I V

     This file contains the implementation of a driver for the Oxford 
     Instruments ITC4 Temperature controller.

  
      Mark Koennecke, Juli 1997

       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <fortify.h>
#include <conman.h>
#include <fortify.h>
#include <strlutil.h>
#include <evdriver.h>
#include "hardsup/itc4util.h"
#include "hardsup/el734_def.h"
#include "hardsup/el734fix.h"
#define SHITTYVALUE -777
/*------------------------- The Driver ------------------------------------*/

pEVDriver CreateITC4Driver(int argc, char *argv[]);
int ConfigITC4(pEVDriver self);


/*-----------------------------------------------------------------------*/
typedef struct {
  pITC4 pData;
  char *pHost;
  int iPort;
  int iChannel;
  int iControl;
  float fDiv;
  float fMult;
  int iRead;
  int iTmo;
  int iLastError;
} ITC4Driv, *pITC4Driv;
/*----------------------------------------------------------------------------*/
static int GetITC4Pos(pEVDriver self, float *fPos)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  iRet = ITC4_Read(&pMe->pData, fPos);
  if (iRet != 1) {
    pMe->iLastError = iRet;
    return 0;
  }
  if ((*fPos < 0) || (*fPos > 10000)) {
    *fPos = -999.;
    pMe->iLastError = SHITTYVALUE;
    return 0;
  }
  return 1;
}

/*----------------------------------------------------------------------------*/
static int ITC4Run(pEVDriver self, float fVal)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  iRet = ITC4_Set(&pMe->pData, fVal);
  if (iRet != 1) {
    pMe->iLastError = iRet;
    return 0;
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
static int ITC4Error(pEVDriver self, int *iCode, char *error, int iErrLen)
{
  pITC4Driv pMe = NULL;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  *iCode = pMe->iLastError;
  if (pMe->iLastError == SHITTYVALUE) {
    strlcpy(error, "Invalid temperature returned form ITC4, check sensor",
            iErrLen);
  } else {
    ITC4_ErrorTxt(&pMe->pData, pMe->iLastError, error, iErrLen);
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
static int ITC4Send(pEVDriver self, char *pCommand, char *pReply, int iLen)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  iRet = ITC4_Send(&pMe->pData, pCommand, pReply, iLen);
  if (iRet != 1) {
    pMe->iLastError = iRet;
    return 0;
  }
  return 1;

}

/*--------------------------------------------------------------------------*/
static int ITC4Init(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  pMe->pData = NULL;
  iRet = ITC4_Open(&pMe->pData, pMe->pHost, pMe->iPort, pMe->iChannel, 0);
  if (iRet != 1) {
    if (iRet == ITC4__NOITC) {
      return -1;
    } else {
      pMe->iLastError = iRet;
      return 0;
    }
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
static int ITC4Close(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  ITC4_Close(&pMe->pData);
  return 1;
}

/*---------------------------------------------------------------------------*/
static int ITC4Fix(pEVDriver self, int iError)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  switch (iError) {
    /* network errors */
  case EL734__BAD_FLUSH:
  case EL734__BAD_RECV:
  case EL734__BAD_RECV_NET:
  case EL734__BAD_RECV_UNKN:
  case EL734__BAD_RECVLEN:
  case EL734__BAD_RECV1:
  case EL734__BAD_RECV1_PIPE:
  case EL734__BAD_RNG:
  case EL734__BAD_SEND:
  case EL734__BAD_SEND_PIPE:
  case EL734__BAD_SEND_NET:
  case EL734__BAD_SEND_UNKN:
  case EL734__BAD_SENDLEN:
    ITC4Close(self);
    iRet = ITC4Init(self);
    if (iRet) {
      return DEVREDO;
    } else {
      return DEVFAULT;
    }
    break;
    /* handable protocoll errors */
  case EL734__BAD_TMO:
    return DEVREDO;
    break;
  case -501:                   /* Bad_COM */
    return DEVREDO;
  case -504:                   /* Badly formatted */
    return DEVREDO;
  default:
    return DEVFAULT;
    break;
  }
  return DEVFAULT;
}

/*--------------------------------------------------------------------------*/
static int ITC4Halt(pEVDriver * self)
{
  assert(self);

  return 1;
}

/*------------------------------------------------------------------------*/
void KillITC4(void *pData)
{
  pITC4Driv pMe = NULL;

  pMe = (pITC4Driv) pData;
  assert(pMe);

  if (pMe->pHost) {
    free(pMe->pHost);
  }
  free(pMe);
}

/*------------------------------------------------------------------------*/
pEVDriver CreateITC4Driver(int argc, char *argv[])
{
  pEVDriver pNew = NULL;
  pITC4Driv pSim = NULL;

  /* check for arguments */
  if (argc < 3) {
    return NULL;
  }

  pNew = CreateEVDriver(argc, argv);
  pSim = (pITC4Driv) malloc(sizeof(ITC4Driv));
  memset(pSim, 0, sizeof(ITC4Driv));
  if (!pNew || !pSim) {
    return NULL;
  }
  pNew->pPrivate = pSim;
  pNew->KillPrivate = KillITC4;

  /* initalise pITC4Driver */
  pSim->iControl = 1;
  pSim->iRead = 1;
  pSim->iLastError = 0;
  pSim->iTmo = 10;
  pSim->fDiv = 10.;
  pSim->fMult = 10;
  pSim->pHost = strdup(argv[0]);
  pSim->iPort = atoi(argv[1]);
  pSim->iChannel = atoi(argv[2]);


  /* initialise function pointers */
  pNew->SetValue = ITC4Run;
  pNew->GetValue = GetITC4Pos;
  pNew->Send = ITC4Send;
  pNew->GetError = ITC4Error;
  pNew->TryFixIt = ITC4Fix;
  pNew->Init = ITC4Init;
  pNew->Close = ITC4Close;

  return pNew;
}

/*--------------------------------------------------------------------------*/
int ConfigITC4(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  iRet = ITC4_Config(&pMe->pData, pMe->iTmo, pMe->iRead,
                     pMe->iControl, pMe->fDiv, pMe->fMult);
  if (iRet < 0) {
    pMe->iLastError = iRet;
    return 0;
  }
  return 1;
}

/*-------------------------------------------------------------------------*/
int SetSensorITC4(pEVDriver self, int iSensor)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  if ((iSensor < 1) || (iSensor > 4)) {
    return 0;
  }
  pMe->iRead = iSensor;
  return 1;
}

/*-------------------------------------------------------------------------*/
int SetControlITC4(pEVDriver self, int iSensor)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  if ((iSensor < 1) || (iSensor > 4)) {
    return 0;
  }
  pMe->iControl = iSensor;
  return 1;
}

/*-------------------------------------------------------------------------*/
int SetTMOITC4(pEVDriver self, int iSensor)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  if (iSensor < 10) {
    return 0;
  }
  pMe->iTmo = iSensor;
  return 1;
}

/*-------------------------------------------------------------------------*/
int GetControlITC4(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  return pMe->iControl;
}

/*-------------------------------------------------------------------------*/
int GetSensorITC4(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  return pMe->iRead;
}

/*-------------------------------------------------------------------------*/
int GetTMOITC4(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  return pMe->iTmo;
}

/*-------------------------------------------------------------------------*/
float GetDivisorITC4(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  return pMe->fDiv;
}

/*--------------------------------------------------------------------------*/
int SetDivisorITC4(pEVDriver self, float fDiv)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  pMe->fDiv = fDiv;
  return 1;
}

/*-------------------------------------------------------------------------*/
float GetMultITC4(pEVDriver self)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  return pMe->fMult;
}

/*--------------------------------------------------------------------------*/
int SetMultITC4(pEVDriver self, float fDiv)
{
  pITC4Driv pMe = NULL;
  int iRet;

  assert(self);
  pMe = (pITC4Driv) self->pPrivate;
  assert(pMe);

  pMe->fMult = fDiv;
  return 1;
}
