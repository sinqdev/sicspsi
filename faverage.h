
/*-----------------------------------------------------------------------
                F o c u s A v e r a g e

  An averager for FOCUS data.  See faverage.tex for more details.

  Mark Koennecke, October 1998

--------------------------------------------------------------------------*/
#ifndef FOCUSAVERAGE
#define FOCUSAVERAGE

int MakeFA(SConnection * pCon, SicsInterp * pSics, void *pData,
           int argc, char *argv[]);

int FocusAverageDo(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);


#endif
