
/*-------------------------------------------------------------------
 AMORSET together with  amorcomp and amordrive implement the position
 control facility for the reflectometer AMOR. This uses the algorithm
 with the beam height as the baseline.
 
 copyright: see file COPYRIGHT
 
 Mark Koennecke, October 2005
--------------------------------------------------------------------*/
#ifndef AMORSET
#define AMORSET
#include "amorcomp.h"

typedef struct {
        pObjectDescriptor pDes;
        pIDrivable pDriv;
        pIDrivable listDrive;
        amorComp chopper;
        amorComp M;
        amorComp DS;
        amorComp D1;
        amorComp D2;
        amorComp D3;
        amorComp EL;
        amorComp S;
        amorComp D4;
        amorComp A;
        amorComp D5;
        amorComp D;
        double targetm2t;
        double targets2t;
        double targetath;
        double actualm2t;
        double actuals2t;
        double actualath;
        int mustDrive;
        int mustRecalculate;
        int driveList;
        double dspar;
        double detectoroffset;
        int verbose;
}amorSet, *pamorSet;
/*--------------------------------------------------------------------*/
int AmorSetFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                                        int argc, char *argv[]);
int AmorSetAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                                        int argc, char *argv[]);
/*============ helper functions for the virtual motors ===============*/                                        
void amorSetMotor(pamorSet amor, int type, double value);
double amorGetMotor(pamorSet amor, SConnection *pCon, int type);

#endif

