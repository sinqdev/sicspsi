\subsection{Triple Axis Spectrometer Software}
A triple axis spectrometer performs complicated scans in Q--energy space.  
The triple axis spectrometers pose a couple of challenges to the SICS
software:  \begin{itemize}
\item The command syntax should be as close as possible to the syntax
of the ancient tasmad program.
\item A relatively complicated algorithm is used for computing the
motor positions and magnet settings. It is a selling point when the
F77 routine used for this purpose in tasmad can be reused.
\item A ILL format scan file must be written after each scan in order
to provide compatability with existing data analysis software.  
\item Provisions must be made to  execute a batch file at each scan
point in order to allow for polarisation analysis. 
\item The user interface of tasmad is largely based on parameters
which are used to store and manipulate state information. 
\item There exists a special syntax for assigning variables which
requires the parameters to be in a special storage order. 
\end{itemize}

These requirements are implemented in the following way: tasmad
variables are matched to SICS variables. The scan and drive commands
are reimplemented in order to allow for both syntax parsing and the
reuse of the triple axis calculation routines. In order to support the
strange storage order assignment system the set command is also
implemented in this modules as it can share code with scan and drive. 
All the remaining syntax adaptions will be implemented in the
scripting language.   


\subsubsection{Data Structures}
It would be inconvenient to search all necessary parameters in the
interpreter anytime a scan or drive command is used. In order to avoid
this, the parameters are cached in a local data structure which is
initialized when installing the tas module. 

@d tasdata @{
typedef struct {
	pObjectDescriptor pDes;
        pSicsVariable tasPar[MAXPAR];
        pCounter counter;
        pScanData pScan;
        int iPOL; 
	int iIgnore; /* in order to ignore writing scan points again 
                        in polarisation mode;
	            */
        int addOutput[MAXADD];
        int addType[MAXADD];
        int addCount;          
        int iFileNO;
        int iFast;
        char scanVar[80];
	float oldSRO;
   }TASdata, *pTASdata;
@}
\begin{description}
\item[pDes] The standard object descriptor required for any SICs
object. 
\item[floatPar] An array of pointers to float parameters. 
The parameters are indexed through defined constants.
\item[pScan] A pointer to a SICS scan object.
\item[iPOL] a flag which is non zer when polarisation analysis is
required.
\item[addOutput] TAS scans may have additional output fields besides
the scan variables in the scan data. This array holds the indices of
such variables.
\item[addType] This array holds the type of additional output
variables. This can be 0 for simple variables or 1 for a motor.
\item[addCount] is the number of additional output variables.
\item[iFileNO] is the number of the current data file.  
\item[iFast] is a flag denoting a fast scan. In a fast scan there is
no wait for the motors  to finish driving.
\item[scanVar] memorizes the first scan variable with a step of greater
 zero.
\item[oldSRO] keeps the old value of the motor SRO. This because the
helmholtz angle  stored in the variable helm has to be corrected when
SRO has been driven. This value is initialised in tasdrive and
TasScanDrive. The actual correction is done in TASUpdate.
\end{description}
The constants for the parameters are defined in the header file. 

\subsubsection{Exported Functions}
These are mainly the interpreter interface functions:
@d tasfunc @{
  int TASFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
  int TASDrive(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
  int TASScan(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
  int TASSet(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);
@}


@o tas.h @{
/*----------------------------------------------------------------------
   Header file for the SICS triple axis module. Implements new drive
   and scan commands for triple axis which use the old tasmad F77 
   routine for calculating the spectrometer setting angles. 

   Mark Koennecke, November 2000
------------------------------------------------------------------------*/  
#ifndef SICSTAS
#define SICSTAS
#include <sicsvar.h>

/*------------------------- parameter defines -------------------------*/
#define WAV   0
#define DM    1
#define DA    2
#define ETAM  3
#define ETAA  4
#define TI    5
#define MN    6
#define IF1V  7
#define IF2V  8
#define IF1H  9
#define IF2H 10
#define HELM 11
#define AS   12
#define BS   13
#define CS   14
#define AA   15
#define BB   16
#define CC   17
#define ETAS 18
#define AX   19
#define AY   20
#define AZ   21
#define BX   22
#define BY   23
#define BZ   24
#define EI   25
#define KI   26
#define EF   27
#define KF   28
#define QH   29
#define QK   30
#define QL   31 
#define EN   32 
#define QM   33

#define HX   34
#define HY   35
#define HZ   36

#define DA1  37 
#define DA2  38
#define DA3  39
#define DA4  40
#define DA5  41
#define DA6  42
#define DMCV 43
#define DSRO 44
#define DACH 45
#define DMTL 46
#define DMTU 47
#define DSTL 48
#define DSTU 49
#define DATL 50
#define DATU 51
#define DMGL 52
#define DSGL 53
#define DSGU 54
#define DAGL 55
#define DEI  56
#define DKI  57
#define DEF  58
#define DKF  59
#define DQH  60
#define DQK  62
#define DQL  62
#define DEN  63
#define DQM  64
#define DT   65
#define SM   66
#define SS   67
#define SA   68
#define FX   69
#define NP   70
#define F1   71
#define F2   72
#define LPA  73

#define MRX1 74
#define MRX2 75
#define ARX1 76
#define ARX2 77

#define INST 78
#define TIT  79
#define USR  80
#define COM  81
#define ALF1 82
#define ALF2 83
#define ALF3 84
#define ALF4 85
#define BET1 86
#define BET2 87
#define BET3 88
#define BET4 89
#define OUT  90
#define LOC  91
#define SWUNIT 92
#define SINFO  93
#define TEI   94
#define TKI   95
#define TEF   96
#define TKF   97
#define TQH   98
#define TQK   99
#define TQL  100 
#define TEN  101 
#define TQM  102
#define THX  103
#define THY  104
#define THZ  105

#define TI1  106
#define TI2  107
#define TI3  108
#define TI4  109
#define TI5  110
#define TI6  111
#define TI7  112
#define TI8  113
#define DI1  114
#define DI2  115
#define DI3  116
#define DI4  117
#define DI5  118
#define DI6  119
#define DI7  120
#define DI8  121
#define DHX  122
#define DHY  123
#define DHZ  124
#define HCONV1 125
#define HCONV2 126
#define HCONV3 127
#define HCONV4 128
#define POLFIL 129

#define MAXPAR 130
#define MAXADD 20
#define MAXEVAR 12

#define SROMOT 7  /* number of the SRO motor */ 
#define A4MOT  3  /* index of a4 motor */
/* --------------------- data structure -------------------------------*/
@<tasdata@>

/*---------------------- interface ----------------------------------*/
@<tasfunc@>
#endif
@}