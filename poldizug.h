/**
 * This is the implementation of a SICS access module for the POLDI
 * pull machine for testing mechanical samples. This thing is a one 
 * of a kind; therefore its implementation does not follow the usual
 * SICS distinction between hardware object and driver. The thing can 
 * operate in two modes: either a force is driven or a position. 
 *
 * There is only the creation function here, everything else is in poldizug.c
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, October 2006
 */
#ifndef POLDIZUG_H_
#define POLDIZUG_H_
#include <sics.h>
int MakePoldiReiss(SConnection * pCon, SicsInterp * pSics,
                   void *pData, int argc, char *argv[]);
#endif                          /*POLDIZUG_H_ */
