/*--------------------------------------------------------------------------
                               P O L T E R W R I T E
                            
    polterwrite is an object for writing POLDI data files. 
    
    copyright: see copyright.h
    
    Uwe Filges, November 2001
----------------------------------------------------------------------------*/
#ifndef POLTERDI
#define POLTERDI

int PolterInstall(SConnection * pCon, SicsInterp * pSics,
                  void *pData, int argc, char *argv[]);


int PolterAction(SConnection * pCon, SicsInterp * pSics,
                 void *pData, int argc, char *argv[]);

#endif
