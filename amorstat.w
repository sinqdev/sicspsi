\subsection{Amor Status Display Support}
The reflectometer AMOR has a few unique status display requirements:
\begin{itemize}
\item In scan mode up to four detector counts curves must be shown for
the two counters in spin-up or spin-down mode. This needs to be
updated after each scan point.
\item Additionally user defined curves may need to be displayed.
\item The usual helper information muste be displayed. 
\item In TOF mode it must be possible to define a region on the
detector whose summed counts are displayed versus the time
binning. This must be sent on request. 
\end{itemize}
In order to cover all this a special object within SICS is required
which deals with all this and packages information in a status display
compliant way.  

In order to do this the amorstatus object registers callbacks both
with the histogram memory and the scan object. These callback
functions are then responsible for updating the status displays. In
order for amorstatus to be able to do this, the client must register
itself with a special command.  

In order to achieve all this some data structures are needed:
@d asdata @{
/*---------------------------------------------------------------------*/
   typedef struct {
                     float *fX, *fY;
                     int iNP;
                     char *name;
                  }UserData, *pUserData;                     
/*---------------------------------------------------------------------*/
   typedef struct __AMORSTAT {
                              pObjectDescriptor pDes;
                              pICallBack pCall;
                              int iUserList;
                              pScanData pScan;
                              pHistMem  pHM;
                              int iTOF;
                              int iHTTP;
                             }AmorStat, *pAmorStat;
                
@}


The fourth data structure is the amor status object data structure. It
has the following fields:
\begin{description}
\item[pDes] The standard SICS object descriptor.
\item[pCall] The callback interface.
\item[iUserList] A list of user data loaded data.
\item[pScan] A pointer to the scan object.
\item[pHM] A pointer to the histogram memory.
\item[iTOF] A flag which is true if we are taking measurements in TOF
mode. 
\end{description} 

In terms of a function interface this object has not much to
offer. Its main purpose is really as an interface to the status
display clients and thus it is configured through the interpreter
interface function. No need for other SICS objects to access it.

@d asinter @{
 int AmorStatusFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                      int argc, char *argv[]);
 int AmorStatusAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                      int argc, char *argv[]);
 void KillAmorStatus(void *pData);
@}


@o amorstat.i @{
/*------------------------------------------------------------------------
                          A M O R S T A T U S

  Internal data structure definitions for the AMOR status display 
  facilitator object. DO NOT CHANGE. This file is automatically
  created from amorstat.w.

  Mark Koennecke, September 1999
---------------------------------------------------------------------*/
@<asdata@>
 
@}

@o amorstat.h @{
/*------------------------------------------------------------------------
                          A M O R S T A T U S

  Public definitions for the AMOR status display 
  facilitator object. DO NOT CHANGE. This file is automatically
  created from amorstat.w.

  Mark Koennecke, September 1999
---------------------------------------------------------------------*/
#ifndef AMORSTATUS
#define AMORSTATUS
@<asinter@>
#endif

@}



