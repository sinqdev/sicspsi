\subsubsection{Oxford Instruments ITC4 Temperature Controllers}
SINQ makes heavy use of Oxford Instruments ITC4 temperature controllers. In
order to support them the following software components had to be defined in
addition to the basic environmet controller interfaces:
\begin{itemize}
\item ITC4driver, naturally. 
\item A ITC4-controller object as derivation of environment controller. ITC4
's allow you to select a sensor which you read as your standard sensor and a
sensor which is used for automatic control. The ITC4 controller object adds
just that additional functionality to the statndard environment controller. 
\end{itemize}
The additional data, the selection of sensors, will be kept in the driver.
This serves  also an example for implementing inheritance without C++.

The driver interface:
@d itcd @{
   pEVDriver CreateITC4Driver(int argc, char *argv[]);
   int       ConfigITC4(pEVDriver self);
   int       SetSensorITC4(pEVDriver self, int iSensor);
   int       SetControlITC4(pEVDriver self, int iSensor);
   int       GetSensorITC4(pEVDriver self);
   int       GetControlITC4(pEVDriver self);
   int       SetDivisorITC4(pEVDriver self, float iSensor);
   float       GetDivisorITC4(pEVDriver self);
   int       SetMultITC4(pEVDriver self, float iSensor);
   float       GetMultITC4(pEVDriver self);
   int       SetTMOITC4(pEVDriver self, int iSensor);
   int       GetTMOITC4(pEVDriver self);

@}

The ConfigITC4 is special. It has to be called to commit changes to the
driver read and control parameters. 

The ITC4 object interface:
@d itco @{
     int ITC4Wrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
                     int argc, char *argv[]);
     int ITC4SetPar(pEVControl self, char *name, float fNew, 
                   SConnection *pCon);
     int ITC4GetPar(pEVControl self, char *name, float *fVal);
     int ITCList(pEVControl self, SConnection *pCon);
@}

The functions defined are: new parameter handling functions, with just
support for the two extra parameters added and a new Wrapper function for
SICS. The meaning of all these functions, their parameters and return values
are identical to those defined for an environment controller. Additionally,
the standard environment controller functions will work as described. The
functions described above are just needed to implement the extra parameters.

@o itc4.h @{
/*-------------------------------------------------------------------------
                             ITC 4
 
  Support for Oxford Instruments ITC4 Temperature controllers for SICS.
  The meaning and working of the functions defined is as desribed for a
  general environment controller.

  Mark Koennecke, Juli 1997

  copyright: see implementation file.

-----------------------------------------------------------------------------*/
#ifndef SICSITC4
#define SICSITC4
/*------------------------- The Driver ------------------------------------*/
@<itcd@>
/*------------------------- The ITC4 object ------------------------------*/
@<itco@>

#endif 
 
@}
