
 /*--------------------------------------------------------------------
  Part of the AMOR position calculation module.

  copyright: see file COPYRIGHT

  Mark Koennecke, October 2005
----------------------------------------------------------------------*/
#ifndef AMORDRIVE
#define AMORDRIVE

typedef struct{
        pObjectDescriptor pDes;
        pIDrivable pDriv;
        pamorSet mama;
        int type;
} amorDrive, *pamorDrive;
/*-----------------------------------------------------------------*/
pamorDrive makeAmorDrive(pamorSet papa, int type);
void killAmorDrive(void *data);
int AmorDriveAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                                        int argc, char *argv[]);

#endif

 
