\subsection{Tabled Driving}
This object implements driving several motors along a predefined path. The definition
 of the path happens through a table. Positions between tabulated positions are 
 interpolated by linear interpolation. Additionally, each motor may be driven a 
 bit from the tabulated positions for fine adjustments. Of course the limits are 
 variable from position to position. Thus this object also sets the software limits of the
 motors accordingly. This object assumes that motors can be driven between positions
 without watching for collisions. The original use of this module is to coordinate the
 movements of the MARS triffids or girafs.
 
The table lives in a separate file. The format of the file is very simple:
Each block starts with a line containing:
\begin{verbatim}
# motorname
\end{verbatim}
This is a hash and the name of the motor.
These lines are followed by n lines of:
\begin{verbatim}
lower position upper
\end{verbatim}
These are three numbers giving the lower and upper limit for this position in the table 
 and, as the middle value, the target position for this entry.
 

 In order to achieve all this, we need a data structure per table entry:
 @d tdentry @{
typedef struct{
	double lower, position, upper;
	int tablePos;
	}tdEntry, *ptdEntry;
 @}
The fields are the lower and upper limits, the position for this table entry and the
number of the entry.


For each motor we need another data structure:
@d tdmotor @{
typedef struct {
	char motorName[132];
	int table;
	pMotor pMot;
	}tdMotor, *ptdMotor;	
@} 
The fields:
\begin{description}
\item[motorName] The name of the motor
\item[table] A list of tabulated positions in the form of tdEntry
\item[pMot] A pointer to the motor data structure.
\end{description} 


The tabledrive object itself needs a data structure too:
@d tdobj @{
typedef struct{
	pObjectDescriptor pDes;
	pIDrivable pDriv;
	int motorTable;
	int tableLength;
	float targetPosition;
	float currentPosition;
	int state;
	char orientMotor[80];
	int debug;
	}TableDrive, *pTableDrive;
@} 
The fields:
\begin{description}
\item[pDes] The standard SICS object descriptor
\item[pDriv] The drivable interface which encapsulates most of the magic of this module.
\item[motorTable] A list of tdMotor entries.
\item[tableLength] The length of the path of positions.
\item[targetPosition] The target position we have to drive to.
\item[currentPosition] where we are now.
\item[state] A state variable used during driving the path.
\item[orientMotor] is the name of the orienting motor, i.e. the one used to determine
 the position.
\end{description}


In terms of an interface, this object implements the drivable interface which has to 
 deal with most of the work. There is just an interpreter interface which allows to 
 configure and query the object.
 
 @d tdint @{
int TableDriveFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
                  	int argc, char *argv[]);
int TableDriveAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                  	int argc, char *argv[]);
 
 @} 
 
 
 @o tabledrive.h @{
 /*---------------------------------------------------------------------------
  SICS object for driving a couple of motors along a tabulated given path.

  copyright: see file COPYRIGHT

  Mark Koennecke, July 2005
---------------------------------------------------------------------------*/
#ifndef SICSTABLEDRIVE
#define SICSTABLEDRIVE
#include <sics.h>
#include "../motor.h"
/*-------------------------------------------------------------------------*/
@<tdentry@>
/*-------------------------------------------------------------------------*/
@<tdmotor@>
/*-------------------------------------------------------------------------*/
@<tdobj@>
/*-------------------------------------------------------------------------*/
@<tdint@>

#endif

@}
 
