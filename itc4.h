
/*-------------------------------------------------------------------------
                             ITC 4
 
  Support for Oxford Instruments ITC4 Temperature controllers for SICS.
  The meaning and working of the functions defined is as desribed for a
  general environment controller.

  Mark Koennecke, Juli 1997

  copyright: see implementation file.

-----------------------------------------------------------------------------*/
#ifndef SICSITC4
#define SICSITC4
/*------------------------- The Driver ------------------------------------*/

pEVDriver CreateITC4Driver(int argc, char *argv[]);
int ConfigITC4(pEVDriver self);
int SetSensorITC4(pEVDriver self, int iSensor);
int SetControlITC4(pEVDriver self, int iSensor);
int GetSensorITC4(pEVDriver self);
int GetControlITC4(pEVDriver self);
int SetDivisorITC4(pEVDriver self, float iSensor);
float GetDivisorITC4(pEVDriver self);
int SetMultITC4(pEVDriver self, float iSensor);
float GetMultITC4(pEVDriver self);
int SetTMOITC4(pEVDriver self, int iSensor);
int GetTMOITC4(pEVDriver self);


/*------------------------- The ITC4 object ------------------------------*/

int ITC4Wrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
int ITC4SetPar(pEVControl self, char *name, float fNew,
               SConnection * pCon);
int ITC4GetPar(pEVControl self, char *name, float *fVal);
int ITCList(pEVControl self, SConnection * pCon);


#endif
