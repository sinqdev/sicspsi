
/*----------------------------------------------------------------------
  TOPSI switched motors. Three motors share a EL734 motor. Between is
  switched through a SPS. 

  Mark Koennecke, May 2001
-----------------------------------------------------------------------*/
#ifndef SWMOTOR
#define SWMOTOR
#include "obdes.h"
#include "interface.h"
#include "motor.h"

int MakeSWMotor(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);

int MakeSWHPMotor(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);

int SWMotorAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);


#endif
