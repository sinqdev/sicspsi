\subsection{SANS Wavelength Calculation}
The instrument SANS has a neutron velocity selector. A wavelength is
selected by varying tilt angle and rotation speed of the velocity selector.
The relation between tilt angle and rotation speed to wavelength is given by
a formula: \begin{eqnarray}
 \lambda & = & A + \frac{B}{rot}\\
 \lambda & = & wavelength in nm\\
 rot & = & rotation speed in revolutions per minute.
\end{eqnarray} 
A and B are coefficients which are calculated from fourth degree polynoms.
The coefficients of these polynoms have been obtained by fitting them
against experimental data. This module now provides for this calculation and
the driving of wavelength. This code does not vary tilt angle.

This object data structure is simple:
@d wavedat @{
   typedef struct __SANSwave {
                              pObjectDescriptor pDes;
                              pIDrivable        pDrivInt;
                              pVelSel           pSelector;
                             };
@}
The fields are:
\begin{description}
\item[pDes] The standard SICS object descriptor.
\item[pDrivInt] A standard drivable interface.
\item[pSelector] The velocity selector to act upon.
\end{description}

Most functionality of this module is hidden in the functions comprising the
drivable interface. Thus the external functions of this object are limited
to:

@d sanswaveint @{
  typedef struct __SANSwave *pSANSWave;
/*-----------------------------------------------------------------------*/

  int CalculateLambda(float fRot, float fTilt, float *fLambda);

  int MakeSANSWave(SConnection *pCon, SicsInterp *pSics, void *pData,
                   int argc, char *argv[]);

  int SANSWaveAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                   int argc, char *argv[]);

@}

The coefficients for the calculation of A and B are hidden in the module
static function CalculateCoefficients.

@o sanswave.h @{
/*---------------------------------------------------------------------------
                            S A N S W A V E

  Wavelength calculations for a neutron velocity selector.  Allows to drive
  the neutron wavelength directly.

  copyright: see copyright.h

  Mark Koennecke, October 1998
---------------------------------------------------------------------------*/
#ifndef SANSWAVE
#define SANSWAVE
@<sanswaveint@>
#endif 
@}  

