/*
**			TAS_SRC:[LIB]EL737_ERRCODES.H
**
** Include file generated from EL737_ERRCODES.OBJ
**
**              29-AUG-2000 09:49:21.56
*/

#define	    EL737__NO_VALUE               0x8668094
#define	    EL737__NO_SOCKET              0x866808C
#define	    EL737__NOT_OPEN               0x8668084
#define	    EL737__FORCED_CLOSED          0x866807C
#define	    EL737__CNTR_OVFL              0x8668074
#define	    EL737__BAD_TMO                0x866806C
#define	    EL737__BAD_SOCKET             0x8668064
#define	    EL737__BAD_PAR                0x866805C
#define	    EL737__BAD_OVFL               0x8668054
#define	    EL737__BAD_OFL                0x866804C
#define	    EL737__BAD_MALLOC             0x8668044
#define	    EL737__BAD_LOC                0x866803C
#define	    EL737__BAD_ILLG               0x8668034
#define	    EL737__BAD_DEV                0x866802C
#define	    EL737__BAD_CNTR               0x8668024
#define	    EL737__BAD_CMD                0x866801C
#define	    EL737__BAD_BSY                0x8668014
#define	    EL737__BAD_ASYNSRV            0x866800C
#define	    EL737__FACILITY               0x866
