#define	    ident	"1B01"
#ifdef VAXC
#module	    FailInet	ident
#endif
#ifdef __DECC
#pragma	    module	    FailInet	ident
#endif
/*
** +--------------------------------------------------------------+
** |                  Paul Scherrer Institute                     |
** |                     Computing Section                        |
** |                                                              |
** | This software may be used freely by non-profit organizations.|
** | It may be copied provided that the name of P.S.I. and of the |
** | author is included. Neither P.S.I. nor the author assume any |
** | responsibility for the use of this software outside of P.S.I.|
** +--------------------------------------------------------------+
**
** Module Name  . . . . . . . . : [...LIB.SINQ]FAILINET.C
**
** Author   . . . . . . . . . . : D. Maden
** Date of creation . . . . . . : Nov 1995
**
**  To compile this module, use:

	$ import tasmad
	$ define/group sinq_c_tlb mad_lib:sinq_c.tlb
	$ cc /debug /noopt /obj=[]FailInet -
			tasmad_disk:[mad.lib.sinq]FailInet +
			sinq_c_tlb/lib

**  To include this module in SINQ.OLB, use:

  $ import tasmad
  $ define/group sinq_c_tlb mad_lib:sinq_c.tlb
  $
  $ define/group sinq_olb mad_lib:sinq_dbg.olb
  $ @tasmad_disk:[mad.lib.sinq]sinq_olb FailInet debug
  $
  $ define/group sinq_olb mad_lib:sinq.olb
  $ @tasmad_disk:[mad.lib.sinq]sinq_olb FailInet
**
** Updates:
**  1A01  2-Nov-1995 DM.	Initial version.
**  1B01 21-Mar-1996 DM.	Move from DELTAT.OLB to SINQ.OLB.
**============================================================================
** The following entry points are included:
**
    #include <sinq_prototypes.h>

    void FailInet (char *text)
**	 --------
**  Input Args:
**	text		- A text string to be printed.
**  Output Args:
**	none
**  Modified Args:
**	none
**  Return status:
**	none
**  Global variables modified:
**	none
**  Routines called:
**	GetErrno
**	perror
**	exit
**  Description:
**	The routine is useful if a fatal TCP/IP error occurs.
**	The value of errno is printed and then "perror" is called.
**	Then "exit" is called.
**============================================================================
**			Global Definitions
*/
#ifdef VAXC
#include    stdlib
#include    stdio
#include    errno
#include    sinq_prototypes
#else
#include    <stdlib.h>
#include    <stdio.h>
#include    <errno.h>
#include    <sinq_prototypes.h>
#endif
/*--------------------------------------------------------------------------
**			Global Variables
*/

/*
**		FailInet:  Some network failure has occurred.
*/
void FailInet(char *text)
{
/*     ========
**		    Output the given text and exit the process.
*/
  int my_errno, my_vaxc_errno;

  GetErrno(&my_errno, &my_vaxc_errno);
  printf("### Internet Error ###\n");
#ifdef __VMS
  printf("                   ### errno      = %d.\n", my_errno);
  printf("                   ### vaxc$errno = %d.\n", my_vaxc_errno);
#else
  printf("                   ### errno = %d.\n", my_errno);
#endif
  perror(text);
  exit(EXIT_FAILURE);
}

/*------------------------------------------------- End of FAILINET.C =======*/
