/*---------------------------------------------------------------------------
  Fix file for David renaming lots of el734 error codes.
  
  Mark Koennecke, October 1998
----------------------------------------------------------------------------*/
#ifndef EL734FIX
#define EL734FIX
#include "asynsrv_errcodes.h"

#define EL734__BAD_HOST	 ASYNSRV__BAD_HOST
#define EL734__BAD_BIND	 ASYNSRV__BAD_BIND
#define EL734__BAD_SENDLEN   ASYNSRV__BAD_SEND_LEN
#define EL734__BAD_SEND	 ASYNSRV__BAD_SEND
#define EL734__BAD_SEND_PIPE  ASYNSRV__BAD_SEND_PIPE
#define EL734__BAD_SEND_UNKN ASYNSRV__BAD_SEND_UNKN
#define EL734__BAD_RECV    ASYNSRV__BAD_RECV
#define EL734__BAD_RECV_PIPE  ASYNSRV__BAD_RECV_PIPE
#define EL734__BAD_RECV_NET   ASYNSRV__BAD_RECV_NET
#define EL734__BAD_SEND_NET   ASYNSRV__BAD_SEND_NET
#define EL734__BAD_RECV_UNKN  ASYNSRV__BAD_RECV_UNKN
#define EL734__BAD_NOT_BCD    ASYNSRV__BAD_NOT_BCD
#define EL734__BAD_RECVLEN    ASYNSRV__BAD_RECV_LEN
#define EL734__BAD_FLUSH      ASYNSRV__BAD_FLUSH
#define EL734__BAD_RECV1      ASYNSRV__BAD_RECV1
#define EL734__BAD_RECV1_PIPE ASYNSRV__BAD_RECV1_PIPE
#define EL734__BAD_RECV1_NET  ASYNSRV__BAD_RECV1_NET
#define EL734__BAD_CONNECT ASYNSRV__BAD_CONNECT
#define EL734__BAD_ID EL734__BAD_DEV
#endif                          /* el734fix */
