/*
**			TAS_SRC:[LIB]ASYNSRV_ERRCODES.H
**
** Include file generated from ASYNSRV_ERRCODES.OBJ
**
**              29-AUG-2000 09:49:15.56
*/

#define	    ASYNSRV__NO_ROOM              0x86480CC
#define	    ASYNSRV__FORCED_CLOSED        0x86480C4
#define	    ASYNSRV__BAD_SOCKET           0x86480BC
#define	    ASYNSRV__BAD_SEND_UNKN        0x86480B4
#define	    ASYNSRV__BAD_SEND_PIPE        0x86480AC
#define	    ASYNSRV__BAD_SEND_NET         0x86480A4
#define	    ASYNSRV__BAD_SEND_LEN         0x864809C
#define	    ASYNSRV__BAD_SEND             0x8648094
#define	    ASYNSRV__BAD_REPLY            0x864808C
#define	    ASYNSRV__BAD_RECV1_PIPE       0x8648084
#define	    ASYNSRV__BAD_RECV1_NET        0x864807C
#define	    ASYNSRV__BAD_RECV1            0x8648074
#define	    ASYNSRV__BAD_RECV_UNKN        0x864806C
#define	    ASYNSRV__BAD_RECV_PIPE        0x8648064
#define	    ASYNSRV__BAD_RECV_NET         0x864805C
#define	    ASYNSRV__BAD_RECV_LEN         0x8648054
#define	    ASYNSRV__BAD_RECV             0x864804C
#define	    ASYNSRV__BAD_PROT_LVL         0x8648044
#define	    ASYNSRV__BAD_PAR              0x864803C
#define	    ASYNSRV__BAD_NOT_BCD          0x8648034
#define	    ASYNSRV__BAD_HOST             0x864802C
#define	    ASYNSRV__BAD_FLUSH            0x8648024
#define	    ASYNSRV__BAD_CONNECT          0x864801C
#define	    ASYNSRV__BAD_CMND_LEN         0x8648014
#define	    ASYNSRV__BAD_BIND             0x864800C
#define	    ASYNSRV__FACILITY             0x864
