
#line 156 "velodorn.w"

/*----------------------------------------------------------------------------
                      S E R I A L S I N Q

  Utility functions for maintaining a connection to a RS--232 port on a
  Macintosh computer running the SINQ terminal server application.

  Mark Koennecke, Juli 1997

  copyright: see implementation file
------------------------------------------------------------------------------*/
#ifndef SERIALSINQ
#define SERIALSINQ
#define NOREPLY -100
#define NOCONNECTION -121
#define SELECTFAIL -120
#define TIMEOUT    -730
#define INTERRUPTED -132
#define OPENFAILURE -133

#line 30 "velodorn.w"

int SerialOpen(void **pData, char *pHost, int iPort, int iChannel);
int SerialForceOpen(void **pData, char *pHost, int iPort, int iChannel);
int SerialConfig(void **pData, int iTmo);
int SerialGetTmo(void **pData);
int SerialATerm(void **pData, char *pTerm);
int SerialAGetTerm(void **pData, char *pTerm, int iTermLen);
int SerialSendTerm(void **pData, char *pTerm);
int SerialGetSocket(void **pData);
int SerialClose(void **pData);
int SerialForceClose(void **pData);

int SerialSend(void **pData, char *pCommand);
int SerialReceive(void **pData, char *pBuffer, int iBufLen);
int SerialReceiveWithTerm(void **pData, char *pBuffer,
                          int iBufLen, char *cTerm);
int SerialError(int iError, char *pError, int iErrLen);
int SerialWriteRead(void **pData, char *pCommand,
                    char *pBuffer, int iBufLen);
int SerialNoReply(void **pData, char *pCommand);

#line 175 "velodorn.w"

/*-------------------------- The sleeperette -----------------------------*/

#line 116 "velodorn.w"

typedef int (*SerialSleep) (void *pData, int iTime);
void SetSerialSleep(void **pData, SerialSleep pFunc, void *pUserData);

#line 177 "velodorn.w"

#endif
