/*---------------------------------------------------------------------------
                         D I L U U T I L
                         
  A few utility functions for talking to Dillution temperature controller
  CCO-510/ AVSI via the SINQ setup: TCP/IP--MAC--RS-232--DILLU.
  
  This controller is weird in that way, that is accepts temperatures as 
  resistance values in Ohms. Therefore a translation table is required
  in order to convert from Kelvin to Ohms.
  
  Mark Koennecke, October 1997
  
----------------------------------------------------------------------------*/
#ifndef SINQDILLU
#define SINQDILLU
#include <stdio.h>
#include "table.h"

/*----------------------- ERRORCODES--------------------------------------
  Most functions return a negative error code on failure. Error codes 
  defined are those defined for serialsinq plus a few additional ones:
*/
#define DILLU__FILENOTFOUND -710
#define DILLU__NODILLFILE  -711
#define DILLU__ERRORTABLE   -712
#define DILLU__BADREAD      -713
#define DILLU__SILLYANSWER  -714
#define DILLU__READONLY     -715
#define DILLU__OUTOFRANGE   -716
#define DILLU__BADMALLOC    -717
#define DILLU__NODILLUFOUND  -711
/*------------------------------------------------------------------------*/
typedef struct __DILLU {
  void *pData;
  pSTable pTranstable;
  int iReadOnly;
} DILLU;

typedef struct __DILLU *pDILLU;

/*-----------------------------------------------------------------------*/
int DILLU_Open(pDILLU * pData, char *pHost, int iPort, int iChannel,
               int iMode, char *pTransFile);
  /***** creates an DILLU  datastructure and opens a connection to the ITCL4
         controller. Input Parameters are:
                 the hostname
                 the port number
                 the RS-232 channel number on the Mac.
                 iMode: 1 for ReadOnly, 0 for normal mode
                 pTransFile: name and path of the temperature ohms
                             trnslation file.
                             
          Return values are 1 for success, a negative error code on
          failure.

  */

void DILLU_Close(pDILLU * pData);
  /****** close a connection to an DILLU controller and frees its
          data structure. The only parameter is a pointer to the data 
          structure for this controller. This pointer will be invalid after
          this call.
  */

int DILLU_Config(pDILLU * pData, int iTmo);
   /***** configure some aspects of a DILLU temperature controller.
          The parameter are:
                    - a pointer to the data structure for the controller as 
                      returned by Open_DILLU
                    - a value for the connection timeout
          The function returns 1 on success, a negative error code on 
          failure.
    */

int DILLU_Send(pDILLU * pData, char *pCommand, char *pReply, int iLen);
    /******* send a the command in pCommand to the DILLU controller.
             A possible reply is returned in the buffer pReply.
             Maximum iLen characters are copied to pReply.
             The first parameter is a pointer to a DILLU data structure
             as returned by Open_DILLU.
             
             Return values are 1 for success, a negative error code on
             failure.
     */

int DILLU_Read(pDILLU * pData, float *fVal);
     /******  
              Reads the current temperature at the controller
 
              Return values are 1 for success, a negative error code on
              failure.
     */

int DILLU_Set(pDILLU * pData, float fVal);
      /****** sets a new preset temperature in the DILL temperature
              controller. Parameters are:
              - a pointer to a DILLU data structure as returned by Open_DILLU.
              - the new preset value.

              Return values are 1 for success, a negative error code on
              failure.
      */

void DILLU_Error2Text(pDILLU * pData, int iCode, char *pError, int iLen);

#endif
