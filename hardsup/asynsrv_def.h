#ifndef	    _asynsrv_def_
#define	    _asynsrv_def_
/*------------------------------------------------ AsynSrv_DEF.H Ident V01N
*/
#ifndef OffsetOf
#define OffsetOf(type, identifier)  ((size_t)(&((type*) NULL)->identifier))
#endif

#ifndef	    _asynsrv_errcodes_
#define	    _asynsrv_errcodes_
#include    <asynsrv_errcodes.h>
#endif

#define AsynSrv_MAX_LINK	8
                    /*
                     ** Structures needed by AsynSrv_Utility.
                     */
struct AsynSrv__info {
  int skt;                      /* The socket number of the connection */
  char host[20];                /* The name of RS-232-C server */
  int port;                     /* The TCP/IP port number of server */
  int chan;                     /* The RS-232-C channel number on server */
  int msg_id;
  int protocol_code;            /* Flag to identify the server's protocol level */
  char protocol_id[4];          /* ASCII version of server's protocol level */
  int cmnd_hdr_len;             /* Header length for command strings */
  char cmnd_fmt[8];             /* "sprintf" format for cmnd header conversion */
  int rply_hdr_len;             /* Header length for response strings */
  char rply_fmt[8];             /* "sscanf" format for rply header conversion */
  char chan_char[4];            /* ASCII encoded version of chan */
  char tmo[4];                  /* ASCII encoded time-out (deci-secs) */
  char eot[4];                  /* Expected terminators */
  int max_replies;              /* Binary version of #replies in response */
  int n_replies;                /* # of last response returned to caller */
  void (*idleHandler) (int, int);       /* MZ. handler called when waiting ..
                                         ** .. on a response */
};

struct AsynSrv_HostPortSkt {
  char host[30];
  int port;
  int skt;
  int protocol_code;
  char protocol_id[4];
  int cmnd_hdr_len;
  int rply_hdr_len;
  int usage_cnt;
  int status;
};
/*------------------------------------------------ End of AsynSrv_DEF.H --*/
#endif                          /* _asynsrv_def_ */
