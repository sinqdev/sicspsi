/*
**			TAS_SRC:[LIB]EL755_ERRCODES.H
**
** Include file generated from EL755_ERRCODES.OBJ
**
**              29-AUG-2000 09:49:23.51
*/

#define	    EL755__TURNED_OFF             0x8678094
#define	    EL755__TOO_MANY               0x867808C
#define	    EL755__TOO_LARGE              0x8678084
#define	    EL755__OVFLOW                 0x867807C
#define	    EL755__OUT_OF_RANGE           0x8678074
#define	    EL755__OFFLINE                0x867806C
#define	    EL755__NO_SOCKET              0x8678064
#define	    EL755__NOT_OPEN               0x867805C
#define	    EL755__FORCED_CLOSED          0x8678054
#define	    EL755__BAD_TMO                0x867804C
#define	    EL755__BAD_SOCKET             0x8678044
#define	    EL755__BAD_PAR                0x867803C
#define	    EL755__BAD_OFL                0x8678034
#define	    EL755__BAD_MALLOC             0x867802C
#define	    EL755__BAD_ILLG               0x8678024
#define	    EL755__BAD_DEV                0x867801C
#define	    EL755__BAD_CMD                0x8678014
#define	    EL755__BAD_ASYNSRV            0x867800C
#define	    EL755__FACILITY               0x867
