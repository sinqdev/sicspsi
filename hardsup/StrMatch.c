#define	    ident	"1A01"
#ifdef VAXC
#module	    StrMatch	ident
#endif
#ifdef __DECC
#pragma	    module	    StrMatch	ident
#endif
/*
** +--------------------------------------------------------------+
** |                  Paul Scherrer Institute                     |
** |                     Department ASQ				  |
** |                                                              |
** | This software may be used freely by non-profit organizations.|
** | It may be copied provided that the name of P.S.I. and of the |
** | author is included. Neither P.S.I. nor the author assume any |
** | responsibility for the use of this software outside of P.S.I.|
** +--------------------------------------------------------------+
**
** Module Name  . . . . . . . . : [...LIB.SINQ]StrMatch.C
**
** Author   . . . . . . . . . . : D. Maden
** Date of creation . . . . . . : Nov 1999
**
**  To compile this module, use:

	$ import tasmad
	$ define/group sinq_c_tlb mad_lib:sinq_c.tlb
	$ cc /debug /noopt /obj=[]StrMatch -
			tasmad_disk:[mad.lib.sinq]StrMatch +
			sinq_c_tlb/lib

**  To include this module in SINQ.OLB, use:

  $ import tasmad
  $ define/group sinq_c_tlb mad_lib:sinq_c.tlb
  $
  $ define/group sinq_olb mad_lib:sinq_dbg.olb
  $ @tasmad_disk:[mad.lib.sinq]sinq_olb StrMatch debug
  $
  $ define/group sinq_olb mad_lib:sinq.olb
  $ @tasmad_disk:[mad.lib.sinq]sinq_olb StrMatch 
**
** Updates:
**  1A01 12-Nov-1999 DM.	Initial version.
**============================================================================
** The following entry points are included in this module:
**
**-------------------------------------------------------------------------
**    #include <sinq_prototypes.h>
**
**    char *StrMatch (&str_a, &str_b, min_len)
**	    -------
**  Input Args:
**	char *str_a	    - Pointer to first  string to be compared.
**	char *str_b	    - Pointer to second string to be compared.
**	int   min_len	    - The minimum allowed match length.
**  Output Args:
**	none
**  Modified Args:
**	none
**  Return value:
**	True (non-zero) if the 2 strings match.
**  Global variables modified:
**	none
**  Routines called:
**	None
**  Description:
**	The routine compares 2 strings, str_a and str_b, ignoring case.
**	The length of str_a must be less than or equal to the length of str_b.
**	The length of str_a must be at least min_len.
**-------------------------------------------------------------------------
**			Global Definitions
*/
#include    <ctype.h>

#define	    NIL	    '\0'
/*
**====================================================================
*/
/*
**====================================================================
**		StrMatch	- compare two strings.
*/
int StrMatch(
/*	========
*/ char *str_a,
              char *str_b, int min_len)
{

  int i = 0;

  while ((tolower(str_a[i]) == tolower(str_b[i])) && (str_a[i] != '\0'))
    i++;

  return ((str_a[i] == '\0') && (i >= min_len));
}

/*-------------------------------------------------- End of StrMatch.C =======*/
