
/*---------------------------------------------------------------------------

  Internal header file for the SINQ histogram memory utility functions.

  David Maden, Mark Koennecke April 1997
----------------------------------------------------------------------------*/
#ifndef SINQHMINTERNAL
#define SINQHMINTERNAL
#define MAXBANK 1

    typedef struct __SBANK {
                            int iStart;
                            int iEnd;
                            int iFlag;
                            int iEdgeLength;
                            int iDelay;
                            unsigned int *iEdges;
                           } SBank, *pSBank;


   typedef struct __SINQHM {
                            char *pHMComputer;
                            int  iMasterPort;
                            int  iMasterSocket;
                            int  iClientPort;
                            int  iClientSocket;
                            int  iBinWidth;
                            int  iLength;
                            int  iRank;
                            int  iPacket;
                            int  iBanks;
                            int xSize, ySize;
                            int  xOff, xFac;
                            int  yOff, yFac;
                            SBank pBank[MAXBANK];
                           } SINQHM;

/*---------------------------- Type definitions, machine dependent--------*/
  typedef short int   SQint16; /* 16 bit integer */
  typedef int         SQint32; /* 32 bit integer */



   static int OpenMasterConnection(pSINQHM self);
   static int GetMasterReply(pSINQHM self, struct rply_buff_struct *reply,
                             int iBufLen);
   static int SendDAQCommand(pSINQHM self, int iCommand, int *iDaq);


   static int SINQHMTimeBin(pSINQHM self, int iMode);

#endif

