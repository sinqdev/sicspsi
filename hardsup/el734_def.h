#ifndef	    _el734_def_
#define	    _el734_def_
/*------------------------------------------------ EL734_DEF.H Ident V01R
*/
#include    <asynsrv_def.h>
#include    <rs232c_def.h>
#include    <sinq_defs.h>

#ifndef OffsetOf
#define OffsetOf(type, identifier)  ((size_t)(&((type*) NULL)->identifier))
#endif

#ifndef	    _EL734_errcodes_
#define	    _EL734_errcodes_
#include    <el734_errcodes.h>
#endif

#define MAX_MOT		12

enum EL734_Requests { FULL__STATUS,
  SHORT__STATUS
};
/*
**	Structure to which the EL734_Open handle points.
*/
struct EL734info {
  struct AsynSrv__info asyn_info;       /* Contains skt, host, port & chan */
  int motor;
  int ored_msr, fp_cntr, fr_cntr;
  struct RS__MsgStruct to_host;
  struct RS__RespStruct from_host;
};
/*
**	Structure holding everything that is known about a VME Motor Controller.
**	It is also the structure of replies from the Server.
*/
struct Motor_State {
  int motor;                    /* Motor number */
  int exists;                   /* True if Motor exists     */
  int msr;                      /* MSR - Motor Status Register  */
  int ored_msr;                 /* Cumulated MSR        */
  int fp_cntr;                  /* Counter for *FP reports      */
  int fr_cntr;                  /* Counter for *FR reports      */
  int ss;                       /* SS - Status Flags Register   */
  char pos_real[16];            /* U  - Position as read (degrees)   */
  char name[16];                /* MN           */
  int dec_pt;                   /* A  - # of decimal places     */
  int enc_factor[2];            /* FD - Encoder scaling factors (numer/denom) */
  int mot_factor[2];            /* FM - Motor   scaling factors (numer/denom) */
  char inertia_tol[16];         /* D  - Inertia tol'nce (sec) (Schleppfehler) */
  int ramp;                     /* E  - Start/stop ramp (kHz/sec)   */
  int loop_mode;                /* F  - Open loop/Closed loop (0/1) */
  int slow_hz;                  /* G  - Start/stop frequency (Mot-S/sec)    */
  char lims[2][16];             /* H  - Lower/Upper limits  */
  int fast_hz;                  /* J  - Top speed (Mot-S/sec)  */
  int ref_mode;                 /* K  - Reference mode  */
  int backlash;                 /* L  - Backlash par (Mot-S) (Spielausgleich) */
  int pos_tol;                  /* M  - Position tolerance (Enc-Steps)  */
  char ref_param[16];           /* Q  - Parameter for "Goto Reference"  */
  int is_sided;                 /* T  - One-sided operation flag (0 = no)   */
  char null_pt[16];             /* V  - Null point  */
  int ac_par;                   /* W  - Air-cushion dependency  */
  int enc_circ;                 /* Z  - circumference of encoder (Enc-Steps) */
  int stat_pos;                 /* SP - # of positionings */
  int stat_pos_flt;             /* ST - # of positioning faults (recovered) */
  int stat_pos_fail;            /* SR - # of positioning fails (abandoned) */
  int stat_cush_fail;           /* SA - # of air-cushion fails */
  char set_real[16];            /* P  - Position as set  (degrees)  */
  int ac_state;                 /* AC - Air-cushion state (0 = down)    */
  int out;                      /* SO - State of Output Signal  */
  int in;                       /* RI - State of Input  Signal  */
};
/*------------------------------------------------ End of EL734_DEF.H --*/
#endif                          /* _el734_def_ */
