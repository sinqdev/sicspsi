#define	    ident	"1B01"
#ifdef VAXC
#module	    GetErrno	ident
#endif
#ifdef __DECC
#pragma	    module	    GetErrno	ident
#endif
/*
** +--------------------------------------------------------------+
** |                  Paul Scherrer Institute                     |
** |                     Computing Section                        |
** |                                                              |
** | This software may be used freely by non-profit organizations.|
** | It may be copied provided that the name of P.S.I. and of the |
** | author is included. Neither P.S.I. nor the author assume any |
** | responsibility for the use of this software outside of P.S.I.|
** +--------------------------------------------------------------+
**
** Module Name  . . . . . . . . : [...LIB.SINQ]GETERRNO.C
**
** Author   . . . . . . . . . . : D. Maden
** Date of creation . . . . . . : Nov 1995
**
**  To compile this module, use:

	$ import tasmad
	$ define/group sinq_c_tlb mad_lib:sinq_c.tlb
	$ cc /debug /noopt /obj=[]GetErrno -
			tasmad_disk:[mad.lib.sinq]GetErrno +
			sinq_c_tlb/lib

**  To include this module in SINQ.OLB, use:

  $ import tasmad
  $ define/group sinq_c_tlb mad_lib:sinq_c.tlb
  $
  $ define/group sinq_olb mad_lib:sinq_dbg.olb
  $ @tasmad_disk:[mad.lib.sinq]sinq_olb GetErrno debug
  $
  $ define/group sinq_olb mad_lib:sinq.olb
  $ @tasmad_disk:[mad.lib.sinq]sinq_olb GetErrno
**
** Updates:
**  1A01  2-Nov-1995 DM.	Initial version.
**  1B01 21-Mar-1996 DM.	Move from DELTAT.OLB to SINQ.OLB.
**============================================================================
** The following entry points are included:
**
    #include <sinq_prototypes.h>

    void GetErrno (int *his_errno, int *his_vaxc_errno)
**	 --------
**  Input Args:
**	none
**  Output Args:
**	his_errno	- value of "errno".
**	his_vaxc_errno	- on VMS systems only, value of "vaxc$errno". Otherwise
**			    set to 1.
**  Modified Args:
**	none
**  Return status:
**	none
**  Global variables modified:
**	none
**  Description:
**	GetErrno returns a copy of the universal error variable "errno" (and,
**	on VMS systems, vaxc$errno) to a local variable supplied by the user.
**	This can occasionally be useful when debugging since the debugger on
**	VMS can't easily examine them.
**============================================================================
**			Global Definitions
*/
#ifdef VAXC
#include    errno
#else
#include    <errno.h>
#endif
/*--------------------------------------------------------------------------
**			Global Variables
*/

/*--------------------------------------------------------------------------
**		GetErrno:  Make copies of errno and vaxc$errno for debug.
*/
void GetErrno(int *his_errno, int *his_vaxc_errno)
{
/*      ========
*/
  *his_errno = errno;           /* Make copy of errno   */
#ifdef __VMS
  *his_vaxc_errno = vaxc$errno; /* Make copy of vaxc$errno    */
#else
  *his_vaxc_errno = 1;
#endif
  return;
}

/*------------------------------------------------- End of GETERRNO.C =======*/
