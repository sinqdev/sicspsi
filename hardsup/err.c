
/*---------------------------------------------------------------------------

	EL734Error2Text converts between an EL734 error code to text
-----------------------------------------------------------------------------*/
void EL734Error2Text(char *pBuffer, int iErr)
{
  switch (iErr) {
  case -28:
    strcpy(pBuffer, "EL734__BAD_ADR");
    break;
  case -8:
    strcpy(pBuffer, "EL734__BAD_BIND");
    break;
  case -30:
    strcpy(pBuffer, "EL734__BAD_BSY");
    break;
  case -3:
    strcpy(pBuffer, "EL734__BAD_CMD");
    break;
  case -9:
    strcpy(pBuffer, "EL734__BAD_CONNECT");
    break;
  case -23:
    strcpy(pBuffer, "EL734__BAD_FLUSH");
    break;
  case -6:
    strcpy(pBuffer, "EL734__BAD_HOST");
    break;
  case -10:
    strcpy(pBuffer, "EL734__BAD_ID");
    break;
  case -5:
    strcpy(pBuffer, "EL734__BAD_ILLG");
    break;
  case -2:
    strcpy(pBuffer, "EL734__BAD_LOC");
    break;
  case -11:
    strcpy(pBuffer, "EL734__BAD_MALLOC");
    break;
  case -21:
    strcpy(pBuffer, "EL734__BAD_NOT_BCD");
    break;
  case -4:
    strcpy(pBuffer, "EL734__BAD_OFL");
    break;
  case -29:
    strcpy(pBuffer, "EL734__BAD_PAR");
    break;

  case -17:
    strcpy(pBuffer, "EL734__BAD_RECV");
    break;
  case -19:
    strcpy(pBuffer, "EL734__BAD_RECV_NET");
    break;
  case -18:
    strcpy(pBuffer, "EL734__BAD_RECV_PIPE");
    break;
  case -20:
    strcpy(pBuffer, "EL734__BAD_RECV_UNKN");
    break;
  case -22:
    strcpy(pBuffer, "EL734__BAD_RECVLEN");
    break;
  case -24:
    strcpy(pBuffer, "EL734__BAD_RECV1");
    break;
  case -26:
    strcpy(pBuffer, "EL734__BAD_RECV1_NET");
    break;
  case -25:
    strcpy(pBuffer, "EL734__BAD_RECV1_PIPE");
    break;
  case -27:
    strcpy(pBuffer, "EL734__BAD_RNG");
    break;
  case -13:
    strcpy(pBuffer, "EL734__BAD_SEND");
    break;
  case -14:
    strcpy(pBuffer, "EL734__BAD_SEND_PIPE");
    break;
  case -15:
    strcpy(pBuffer, "EL734__BAD_SEND_NET");
    break;
  case -16:
    strcpy(pBuffer, "EL734__BAD_SEND_UNKN");
    break;
  case -12:
    strcpy(pBuffer, "EL734__BAD_SENDLEN");
    break;
  case -7:
    strcpy(pBuffer, "EL734__BAD_SOCKET");
    break;
  case -1:
    strcpy(pBuffer, "EL734__BAD_TMO");
    break;
  default:
    strcpy(pBuffer, "Unknown EL734 error");
    break;
  }
}
