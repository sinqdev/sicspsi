#ifndef	    _rs232c_def_
#define	    _rs232c_def_
/*------------------------------------------------ RS232C_DEF.H Ident V02G
**	Definitions for the RS-232-C Server Protocol
**
**	On UNIX systems, this file is located in /public/lib/include
**	On VMS  systems, this file is a module in mad_lib:sinq_c.tlb
*/
#define RS__PROTOCOL_ID		"V01A"
#define RS__PROTOCOL_ID_V01B	"V01B"

#define RS__PROTOCOL_CODE	1       /* Code corresponding to RS__PROTOCOL_ID   */
#define RS__PROTOCOL_CODE_V01B	2       /* Code corresponding to RS__PROTOCOL_ID_0 */

#ifndef OffsetOf
#define OffsetOf(type, identifier)  ((size_t)(&((type*) NULL)->identifier))
#endif
/*----------------------------------------------------------------------------
** Structure of Message from Client to Server - everything is sent in ASCII
**						for LabView's benefit.
** Name		#bytes         Description
** ====		======         ===========
** msg_size	    4	Number of bytes following (rounded up to multiple
**				    of 4).
** msg_id	    4	Message ident (an incrementing counter for debugging).
** c_pcol_lvl	    4	Client-Protocol-Level (should be "V01A").
** serial_port	    4	Serial port to which commands should be sent. This
**			    is a small integer).
** tmo		    4	Time-out in units of 0.1 secs (<0 = "wait for ever").
** terms	1 + 3	Terminators. The first char gives the number of
**			    terminators (up to 3) and the following 3 chars
**			    are valid response terminators, e.g. "1\r\0\0".
** n_cmnds	    4	Number of commands following.
** cmnds	  356	The command buffer. This is a concatenated list of
**			    commands with the structure described below.
**
**  Special Cases of msg_size
**  -------------------------
**    "-001" ==> the client is just about to close his connection.
**    "-002" ==> this is a request to the server for him to turn on tracing.
**		    The reply should be simply an echo of the 4 bytes "-002".
**    "-003" ==> this is a request to the server for him to turn off tracing.
**		    The reply should be simply an echo of the 4 bytes "-003".
**    "-004" ==> this is a request to the server for him to flush his buffers.
**		    The reply should be simply an echo of the 4 bytes "-004".
**
** Structure of a command item in the cmnds buffer.
**
** a) RS__PROTOCOL_ID = "V01A"
**
**    Name	#bytes         Description
**    ====	======         ===========
**    cmnd_len	    2	    The number of bytes following encoded as 2 ASCII
**			    decimal chars.
**    cmnd     <cmnd_len>   The command to be sent on Serial Port <serial_port>.
**			    The string should contain any required terminator
**			    bytes but should not be zero-terminated (unless
**			    the zero-byte should be transmitted at the end
**			    of the command). cmnd_len should count the
**			    terminator byte.
**
**	An example of a command item might be:  "06RMT 1\r"
**
** b) RS__PROTOCOL_ID = "V01B"
**
**    Name	#bytes         Description
**    ====	======         ===========
**    cmnd_len	    4	    The number of bytes following encoded as 4 ASCII
**			    decimal chars.
**    cmnd     <cmnd_len>   The command to be sent on Serial Port <serial_port>.
**			    The string should contain any required terminator
**			    bytes but should not be zero-terminated (unless
**			    the zero-byte should be transmitted at the end
**			    of the command). <cmnd_len> should count the
**			    terminator byte.
**
**	An example of a command item might be:  "0006RMT 1\r"
**--------------------------------------------------------------------------*/
struct RS__MsgStruct {
  char msg_size[4];             /* 4 ASCII decimal chars!! */
  char msg_id[4];
  char c_pcol_lvl[4];           /* Client protocol level */
  char serial_port[4];
  char tmo[4];                  /* Units are 0.1 secs */
  char terms[4];
  char n_cmnds[4];
  char cmnds[356];
};
        /*
         ** The "cmnds" buffer in RS__MsgStruct is a concatenated
         ** list of the following structures.
         */
struct RS__CmndStruct {
  char cmnd_len[2];
  char cmnd[1];
};
struct RS__CmndStruct_V01B {
  char cmnd_len[4];
  char cmnd[1];
};
/*----------------------------------------------------------------------------
** Structure of Reply from Server to Client - everything is sent in ASCII
**					      for LabView's benefit.
**
** Name		#bytes         Description
** ====		======         ===========
** msg_size	    4	Number of bytes following (rounded up to multiple
**				    of 4).
** msg_id	    4	Message ident (this is a copy of the msg_id field
**			    in the message from Client to Server).
** s_pcol_lvl	    4	Server-Protocol-Level (should be "V01A" or "V01B").
** n_rply	    4	Number of replies following. If < 0, an error has
**			    been detected and sub_status may give additional
**			    information.
** rplys	  496	The reply buffer. This is a concatenated list of
**			    replies with the structure described below.
** sub_status	   12	A sub-status code. This field overlays the first 12
**			    bytes of rplys and may provide additional
**			    information in the case that n_rply < 0.
**
** Structure of a reply item in the rplys buffer.
**
** a) RS__PROTOCOL_ID = "V01A"
**
**    Name	#bytes         Description
**    ====	======         ===========
**    rply_len	 2	 The number of bytes following encoded as 2 ASCII
**			    decimal chars.
**    term	 1	 The terminating character which was detected at the
**			    end of the reply. This will be one of the
**			    characters specified in <terms>.    
**    rply  <rply_len-1> The zero-terminated reply. This is effectively the
**			    reply as received with the terminating character
**			    replaced by '\0'.
**
**	An example of a reply item might be:  "08\r12.345\0"
**
** b) RS__PROTOCOL_ID = "V01B"
**
**    Name	#bytes         Description
**    ====	======         ===========
**    rply_len	 4	 The number of bytes following encoded as 4 ASCII
**			    decimal chars.
**    term	 1	 The terminating character which was detected at the
**			    end of the reply. This will be one of the
**			    characters specified in <terms>.    
**    rply  <rply_len-1> The zero-terminated reply. This is effectively the
**			    reply as received with the terminating character
**			    replaced by '\0'.
**
**  An example of a reply item might be:  "0009\r12.3456\0"
**--------------------------------------------------------------------------*/
struct RS__RespStruct {
  char msg_size[4];
  char msg_id[4];
  char s_pcol_lvl[4];           /* Server protocol level */
  char n_rply[4];               /* Error if < 0 */
  union {
    char rplys[496];
    char sub_status[12];
  } u;
};
        /*
         ** The "rplys" buffer in RS__RespStruct is a
         ** concatenated list of the following structures.
         */
struct RS__RplyStruct {
  char rply_len[2];             /* 2 ASCII decimal chars!!
                                 ** The length includes the
                                 ** terminator, term, and the
                                 ** zero terminator of rply.
                                 */
  char term;                    /* The terminating character */
  char rply[1];                 /* Zero terminated string */
};
struct RS__RplyStruct_V01B {
  char rply_len[4];             /* 4 ASCII decimal chars!!
                                 ** The length includes the
                                 ** terminator, term, and the
                                 ** zero terminator of rply.
                                 */
  char term;                    /* The terminating character */
  char rply[1];                 /* Zero terminated string */
};
/*------------------------------------------------ End of RS232C_DEF.H --*/
#endif                          /* _rs232c_def_ */
