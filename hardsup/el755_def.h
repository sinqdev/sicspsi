#ifndef	    _el755_def_
#define	    _el755_def_
/*------------------------------------------------ EL755_DEF.H Ident V01C
**	Definitions for the EL755 Magnet Power Supply Controller
**
**	On UNIX systems, this file is located in /public/lib/include
**	On VMS  systems, this file is a module in mad_lib:sinq_c.tlb
*/
#include    <asynsrv_def.h>
#include    <rs232c_def.h>

#ifndef OffsetOf
#define OffsetOf(type, identifier)  ((size_t)(&((type*) NULL)->identifier))
#endif

#ifndef	    _EL755_errcodes_
#define	    _EL755_errcodes_
#include    <el755_errcodes.h>
#endif

/*
**	Structure to which the EL755_Open handle points.
*/
struct EL755info {
  struct AsynSrv__info asyn_info;       /* Contains skt, host, port & chan */
  int index;
  struct RS__MsgStruct to_host;
  struct RS__RespStruct from_host;
};
/*------------------------------------------------ End of EL755_DEF.H --*/
#endif                          /* _el755_def_ */
