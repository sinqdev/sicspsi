#ifndef _sinq_prototypes_loaded_
#define _sinq_prototypes_loaded_
/*---------------------------------------------- SINQ_PROTOTYPES.H Ident V02T
**
**	Prototype header file for entry points in SINQ.OLB
**
** Updates:
**  V01A 21-Mar-1996 DM	    Initial version.
*/
#ifdef VAXC
#include    asynsrv_def
#include    rs232c_def
#include    el734_def
#include    el737_def
#else
#include    <asynsrv_def.h>
#include    <rs232c_def.h>
#include    <el734_def.h>
#include    <el737_def.h>
#endif
/*---------------------------------------------------------------------*/
int AsynSrv_ChanClose(struct AsynSrv__info *asyn_info);
int AsynSrv_Close(struct AsynSrv__info *asyn_info, int force_flag);
int AsynSrv_Config(struct AsynSrv__info *asyn_info, ...);
int AsynSrv_ConfigDflt(char *par_id, ...);
void AsynSrv_ErrInfo(char **entry_txt,
                     int *errcode, int *my_errno, int *vaxc_errno);
int AsynSrv_Flush(struct AsynSrv__info *asyn_info);
int AsynSrv_GetLenTerm(struct AsynSrv__info *asyn_info,
                       struct RS__RespStruct *rcve_buff,
                       char *rply, int *len, char *term);
char *AsynSrv_GetReply(struct AsynSrv__info *asyn_info,
                       struct RS__RespStruct *rcve_buff, char *last_rply);
int AsynSrv_Open(struct AsynSrv__info *asyn_info);
int AsynSrv_OpenNew(struct AsynSrv__info *asyn_info);
int AsynSrv_SendCmnds(struct AsynSrv__info *asyn_info,
                      struct RS__MsgStruct *send_buff,
                      struct RS__RespStruct *rcve_buff, ...);
int AsynSrv_SendCmndsBig(struct AsynSrv__info *asyn_info,
                         struct RS__MsgStruct *send_buff,
                         int send_buff_size,
                         struct RS__RespStruct *rcve_buff,
                         int rcve_buff_size, ...);
int AsynSrv_Trace(struct AsynSrv__info *asyn_info, int state);
int AsynSrv_Trace_Write(struct AsynSrv__info *asyn_info);
/*---------------------------------------------------------------------*/
int C_log_arr_get(char *name, int arr_size, int *value, int indx);
int C_log_flt_get(char *name, float *value, int indx);
int C_log_int_get(char *name, long int *value, int indx);
int C_log_str_get(char *name, char *value, int val_size, int indx);
/*---------------------------------------------------------------------*/
int C_str_edit(char *out, char *in, char *ctrl, int *length);
/*---------------------------------------------------------------------*/
int C_tt_port_config(int *hndl, int mask);
int C_tt_port_connect(int *hndl, int *chan, char *lognam, char *pwd);
int C_tt_port_disconnect(int *hndl);
int C_tt_port_io(int *hndl,
                 char *rqst,
                 char *term,
                 char *answ, int *answ_len, int flush, int tmo);
/*---------------------------------------------------------------------*/
int EL734_Close(void **handle, int force_flag);
int EL734_Config(void **handle, ...);
char *EL734_EncodeMSR(char *text,
                      int text_len,
                      int msr, int ored_msr, int fp_cntr, int fr_cntr);
char *EL734_EncodeSS(char *text, int text_len, int ss);
void EL734_ErrInfo(char **entry_txt,
                   int *errcode, int *my_errno, int *vaxc_errno);
int EL734_GetAirCush(void **handle, int *present, int *state);
int EL734_GetEncGearing(void **handle, int *nominator, int *denominator);
int EL734_GetId(void **handle, char *id_txt, int id_len);
int EL734_GetLimits(void **handle, float *lo, float *hi);
int EL734_GetMotorGearing(void **handle, int *nominator, int *denominator);
int EL734_GetNullPoint(void **handle, int *null_pt);
int EL734_GetPosition(void **handle, float *ist_posit);
int EL734_GetPrecision(void **handle, int *n_dec);
int EL734_GetRefMode(void **handle, int *mode);
int EL734_GetRefParam(void **handle, float *param);
int EL734_GetSpeeds(void **handle, int *lo, int *hi, int *ramp);
int EL734_GetStatus(void **handle,
                    int *msr,
                    int *ored_msr,
                    int *fp_cntr, int *fr_cntr, int *ss, float *ist_posit);
int EL734_GetZeroPoint(void **handle, float *zero_pt);
int EL734_MoveNoWait(void **handle, float soll_posit);
int EL734_MoveWait(void **handle,
                   float soll_posit,
                   int *ored_msr,
                   int *fp_cntr, int *fr_cntr, float *ist_posit);
int EL734_Open(void **handle,
               char *host, int port, int chan, int motor, char *id);
int EL734_PutOffline(void **handle);
int EL734_PutOnline(void **handle, int echo);
int EL734_SendCmnd(void **handle, char *cmnd, char *rply, int rply_size);
int EL734_SetAirCush(void **handle, int state);
int EL734_SetErrcode(struct EL734info *info_ptr,
                     char *response, char *cmnd);
int EL734_SetHighSpeed(void **handle, int hi);
int EL734_SetLowSpeed(void **handle, int lo);
int EL734_SetRamp(void **handle, int ramp);
int EL734_Stop(void **handle);
int EL734_WaitIdle(void **handle,
                   int *ored_msr,
                   int *fp_cntr, int *fr_cntr, float *ist_posit);
void EL734_ZeroStatus(void **handle);
/*---------------------------------------------------------------------*/
int EL737_Close(void **handle, int force_flag);
int EL737_Config(void **handle, ...);
int EL737_Continue(void **handle, int *status);
int EL737_EnableThresh(void **handle, int indx);
void EL737_ErrInfo(char **entry_txt,
                   int *errcode, int *my_errno, int *vaxc_errno);
int EL737_GetMonIntegTime(void **handle, int indx, float *mon_integ_time);
int EL737_GetRateIntegTime(void **handle, float *rate_integ_time);
void *EL737_GetReply(void **handle, void *last_rply);
int EL737_GetStatus(void **handle,
                    int *c1,
                    int *c2, int *c3, int *c4, float *timer, int *rs);
int EL737_GetStatusExtra(void **handle,
                         int *c5, int *c6, int *c7, int *c8);
int EL737_GetThresh(void **handle, int *indx, float *val);
int EL737_Open(void **handle, char *host, int port, int chan);
int EL737_Pause(void **handle, int *status);
int EL737_SendCmnd(void **handle, char *cmnd, char *rply, int rply_size);
int EL737_SetErrcode(struct EL737info *info_ptr,
                     char *response, char *cmnd);
int EL737_SetThresh(void **handle, int indx, float val);
int EL737_StartCnt(void **handle, int preset_count, int *status);
int EL737_StartTime(void **handle, float preset_time, int *status);
int EL737_Stop(void **handle,
               int *c1,
               int *c2, int *c3, int *c4, float *timer, int *status);
int EL737_StopFast(void **handle);
int EL737_WaitIdle(void **handle,
                   int *c1, int *c2, int *c3, int *c4, float *timer);
/*---------------------------------------------------------------------*/
int EL755_Close(void **handle, int force_flag);
int EL755_Config(void **handle, ...);
void EL755_ErrInfo(char **entry_txt,
                   int *errcode, int *my_errno, int *vaxc_errno);
int EL755_ErrorLog(char *routine_name, char *text);
int EL755_GetConstant(void **handle, float *value);
int EL755_GetCurrents(void **handle, float *soll, float *ist);
int EL755_GetId(void **handle, char *id_txt, int id_len);
int EL755_GetLimit(void **handle, float *value);
int EL755_GetRamp(void **handle, float *value);
int EL755_GetTimeConstant(void **handle, float *value);
int EL755_GetVoltageRange(void **handle, float *value);
int EL755_Open(void **handle, char *host, int port, int chan, int indx);
int EL755_PutOffline(void **handle);
int EL755_PutOnline(void **handle, int echo);
int EL755_SendTillSameStr(void **handle,
                          char *cmnd, char *rply, int rply_len);
int EL755_SendTillSameVal(void **handle, char *cmnd, float *val);
int EL755_SendTillTwoVals(void **handle,
                          char *cmnd, float *val0, float *val1);
int EL755_SetConstant(void **handle, float value);
int EL755_SetCurrent(void **handle, float soll);
int EL755_SetLimit(void **handle, float value);
int EL755_SetRamp(void **handle, float value);
int EL755_SetTimeConstant(void **handle, float value);
int EL755_SetVoltageRange(void **handle, float value);
/*---------------------------------------------------------------------*/
int Fluke_Close(void **handle, int force_flag);
int Fluke_Config(void **handle, ...);
void Fluke_ErrInfo(char **entry_txt,
                   int *errcode, int *my_errno, int *vaxc_errno);
int Fluke_ErrorLog(char *routine_name, char *text);
int Fluke_Open(void **handle, char *host, int port, int chan);
int Fluke_Read(void **handle, float *ist);
int Fluke_SendTillSame(void **handle,
                       char *cmnd, char *rply, int rply_len);
int Fluke_SendTillSameVal(void **handle, char *cmnd, float *val);
/*---------------------------------------------------------------------*/
int ITC_Close(void **handle, int force_flag);
int ITC_Config(void **handle, ...);
int ITC_Dump_RAM(void **handle,
                 int buff_size, char *buff, int *dump_len, int *n_diffs);
void ITC_ErrInfo(char **entry_txt,
                 int *errcode, int *my_errno, int *vaxc_errno);
int ITC_GetConfig(void **handle, ...);
int ITC_Load_RAM(void **handle, int load_len, char *buff);
int ITC_Load_Table(void **handle, char *buff);
int ITC_Open(void **handle, char *host, int port, int chan);
int ITC_Read_ITC_Sensor(void **handle,
                        int sensor, float factor, float *value);
int ITC_Read_LTC11_Sensor(void **handle, int sensor, float *value);
int ITC_Read_LTC11_SetPt(void **handle, float *value);
int ITC_ReadAuxTemp(void **handle, float *value);
int ITC_ReadControlTemp(void **handle, float *value);
int ITC_ReadHeaterOp(void **handle, float *op_level, float *op_percent);
int ITC_ReadId(void **handle, char *id_txt, int id_txt_len, int *id_len);
int ITC_ReadPID(void **handle, float *p, float *i, float *d);
int ITC_ReadSampleTemp(void **handle, float *s_temp);
int ITC_ReadSetPoint(void **handle, float *sp_temp);
int ITC_ReadStatus(void **handle,
                   char *status_txt,
                   int status_txt_len,
                   int *status_len, int *auto_state, int *remote_state);
int ITC_SendTillAckOk(void **handle,
                      struct RS__MsgStruct *to_host,
                      struct RS__RespStruct *from_host, char *cmnd);
int ITC_SendTillSame(void **handle,
                     struct RS__MsgStruct *to_host,
                     struct RS__RespStruct *from_host,
                     char *cmnd, char *rply, int rply_len);
int ITC_SendTillSameLen(void **handle,
                        struct RS__MsgStruct *to_host,
                        struct RS__RespStruct *from_host,
                        char *cmnd, char *rply, int rply_len);
int ITC_SendTillSameLenAckOK(void **handle,
                             struct RS__MsgStruct *to_host,
                             struct RS__RespStruct *from_host,
                             char *cmnd, char *rply, int rply_len);
int ITC_SetControlTemp(void **handle, float s_temp);
int ITC_SetHeatLevel(void **handle, float heat_percent);
int ITC_ErrorLog(char *routine_name, char *text);
/*---------------------------------------------------------------------*/
int SPS_Close(void **handle, int force_flag);
int SPS_Config(void **handle, ...);
void SPS_ErrInfo(char **entry_txt,
                 int *errcode, int *my_errno, int *vaxc_errno);
int SPS_ErrorLog(char *routine_name, char *text);
int SPS_GetStatus(void **handle,
                  unsigned char *status_vals,
                  int n_status_vals, int *adc_vals, int n_adc_vals);
int SPS_Open(void **handle, char *host, int port, int chan);
int SPS_SendTillSame(void **handle,
                     struct RS__MsgStruct *to_host,
                     struct RS__RespStruct *from_host,
                     char *cmnd, char *rply, int rply_len);
int SPS_SendTillSameLen(void **handle,
                        struct RS__MsgStruct *to_host,
                        struct RS__RespStruct *from_host,
                        char *cmnd, char *rply, int rply_len);
/*---------------------------------------------------------------------*/
int VelSel_Close(void **handle, int force_flag);
void VelSel_Config(void **handle, int msec_tmo, char *eot_str);
void VelSel_ErrInfo(char **entry_txt,
                    int *errcode, int *my_errno, int *vaxc_errno);
void *VelSel_GetReply(void **handle, void *last_rply);
int VelSel_GetStatus(void **handle, char *status_str, int status_str_len);
int VelSel_Open(void **handle, char *host, int port, int chan);
int VelSel_SendCmnd(void **handle, char *cmnd, char *rply, int rply_size);
/*---------------------------------------------------------------------*/
void FailInet(char *text);
void GetErrno(int *his_errno, int *his_vaxc_errno);
int MakeCharPrintable(char *out, int out_size, char in);
char *MakePrint(char *text);
char *MakePrintable(char *out, int out_size, char *in);
void *Map_to_ACS();
char *StrEdit(char *out, char *in, char *ctrl, int *ln);
char *StrJoin(char *result, int result_size, char *str_a, char *str_b);
int StrMatch(char *str_a, char *str_b, int min_len);
int Get_TASMAD_Info(char *file_name, int *nItems, ...);
int Get_TASMAD_Info_Filename(char *file_name, char *buf, int *bufSize);
int Update_TASMAD_Info(char *file_name, int *nItems, ...);
/*--------------------------------------------- End of SINQ_PROTOTYPES.H --*/
#endif                          /* _sinq_prototypes_loaded_ */
