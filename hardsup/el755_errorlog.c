#define	    ident	"1A01"
#ifdef VAXC
#module	    EL755_ErrorLog    ident
#endif
#ifdef __DECC
#pragma	    module	    EL755_ErrorLog    ident
#endif

#include    <stdio.h>

/*
**--------------------------------------------------------------------------
**		EL755_ErrorLog:	This routine is called by EL755 routines in
**				the case of certain errors. It simply prints
**				to stderr. The user should supply his own
**				routine if he wishes to log these errors in
**				some other way.
*/
void EL755_ErrorLog(
/*	    ==============
*/ char *routine_name,
                     char *text)
{

  fprintf(stderr, "%s: %s\n", routine_name, text);
}

/*-------------------------------------------- End of EL755_ErrorLog.C =======*/
