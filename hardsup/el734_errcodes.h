/*
**			TAS_SRC:[LIB]EL734_ERRCODES.H
**
** Include file generated from EL734_ERRCODES.OBJ
**
**              29-AUG-2000 09:49:19.60
*/

#define	    EL734__VFY_ERR                0x865809C
#define	    EL734__NO_SOCKET              0x8658094
#define	    EL734__NOT_OPEN               0x865808C
#define	    EL734__FORCED_CLOSED          0x8658084
#define	    EL734__EMERG_STOP             0x865807C
#define	    EL734__BAD_TMO                0x8658074
#define	    EL734__BAD_STP                0x865806C
#define	    EL734__BAD_SOCKET             0x8658064
#define	    EL734__BAD_RNG                0x865805C
#define	    EL734__BAD_PAR                0x8658054
#define	    EL734__BAD_OVFL               0x865804C
#define	    EL734__BAD_OFL                0x8658044
#define	    EL734__BAD_MALLOC             0x865803C
#define	    EL734__BAD_LOC                0x8658034
#define	    EL734__BAD_ILLG               0x865802C
#define	    EL734__BAD_DEV                0x8658024
#define	    EL734__BAD_CMD                0x865801C
#define	    EL734__BAD_ASYNSRV            0x8658014
#define	    EL734__BAD_ADR                0x865800C
#define	    EL734__FACILITY               0x865
