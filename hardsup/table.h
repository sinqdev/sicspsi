/*---------------------------------------------------------------------------
  A general purpose translation table and interpolation module.
  Interpolation tables are read from a file, which is meant to
  contain pairs of val1 val2 per line. 
  
  
  Mark Koennecke, October 1997
  
  copyright: see copyright.h

-----------------------------------------------------------------------------*/
#ifndef SICSTABLE
#define SICSTABLE
typedef struct __SicsTable *pSTable;

/*------------------------- live & death ----------------------------------*/
pSTable CreateTable(FILE * fd);
    /* 
       creates a new table from a given file. The file is meant to have 
       been positioned to the first entry for the table in the file. 
       This leaves the caller free to examine a header, if any.
     */
void DeleteTable(pSTable self);
/*------------------------- Interpolation --------------------------------*/
int InterpolateVal1(pSTable pTable, float fKey, float *fResult);
   /*
      Returns a result from the second column for a key from the
      first column.
    */
int InterpolateVal2(pSTable pTable, float fKey, float *fResult);
   /*
      Returns a result from the first column for a key from the
      second column.
    */
#endif
