
#line 363 "sinqhm.w"

/*---------------------------------------------------------------------------
                               S I N Q H M
  Some utility functions for interfacing to the SINQ histogram memory
  server.

  David Maden, Mark Koennecke, April 1997

  copyright: see implementation file.
-----------------------------------------------------------------------------*/
#ifndef SINQHMUTILITY
#define SINQHMUTILITY
#include "sinqhm_def.h"

typedef struct __SINQHM *pSINQHM;
/*------------------------------ Error codes -----------------------------*/

#line 341 "sinqhm.w"

#define HMCOMPUTER_NOT_FOUND -2
#define SOCKET_ERROR         -3
#define BIND_ERROR           -4
#define CONNECT_ERROR        -5
#define RECEIVE_ERROR        -6
#define INSUFFICIENT_DATA    -7
#define BYTE_ORDER_CHAOS     -8
#define HIST_BAD_CREATE      -9
#define HIST_BAD_STATE       -10
#define HIST_BAD_VALUE       -11
#define HIST_BAD_RECV        -12
#define HIST_BAD_ALLOC       -13
#define HIST_BAD_CODE        -14
#define SEND_ERROR           -15
#define CLOSE_ERROR          -16
#define INVALID_HARSH        -17
#define SOFTWARE_ERROR       -18
#define DAQ_INHIBIT          -19
#define DAQ_NOTSTOPPED       -20

#line 379 "sinqhm.w"


/*------------------------------ Prototypes ------------------------------*/

#line 118 "sinqhm.w"

pSINQHM CreateSINQHM(char *pHMComputer, int iMasterPort);
pSINQHM CopySINQHM(pSINQHM self);
void DeleteSINQHM(pSINQHM self);
void SINQHMSetPar(pSINQHM self, int iRank, int iLength, int iBinWidth);
void SINQHMSetPSD(pSINQHM self, int xSize, int xOff, int xFac,
                  int ySize, int yOff, int yFac);

#line 142 "sinqhm.w"

int SINQHMError2Text(int iErr, char *pBuffer, int iBufLen);

#line 155 "sinqhm.w"

int SINQHMConfigure(pSINQHM self, int iMode, int iRank, int iLength,
                    int iBinWidth, int iLowBin, int iCompress);
int SINQHMConfigurePSD(pSINQHM self, int iMode,
                       int xSize, int xOff, int xFac,
                       int ySize, int yOff, int yFac,
                       int iBinWidth, float *iEdges, int iEdgeLength);

int SINQHMDeconfigure(pSINQHM self, int iHarsh);
int SINQHMGetStatus(pSINQHM self, int *iMode, int *iDaq,
                    int *iRank, int *iBinWidth,
                    int *iLength, int *iClients);
int SINQHMDebug(pSINQHM self, int iLevel);
int SINQHMKill(pSINQHM self);


#line 261 "sinqhm.w"

int SINQHMOpenDAQ(pSINQHM self);
int SINQHMCloseDAQ(pSINQHM self);

int SINQHMStartDAQ(pSINQHM self);
int SINQHMStopDAQ(pSINQHM self);
int SINQHMInhibitDAQ(pSINQHM self);
int SINQHMContinueDAQ(pSINQHM self);

int SINQHMWrite(pSINQHM self, int iNum, int iStart, int iEnd, void *pData);
long SINQHMSize(pSINQHM self, int iNum, int iStart, int iEnd);
int SINQHMRead(pSINQHM self, int iNum, int iStart, int iEnd,
               void *pData, int iDataLen);
int SINQHMProject(pSINQHM self, int code, int xStart, int nx,
                  int yStart, int ny, void *pData, int iDataLen);
int SINQHMZero(pSINQHM self, int iNum, int iStart, int iEnd);

#line 382 "sinqhm.w"


#line 232 "sinqhm.w"

int SINQHMDefineBank(pSINQHM self, int iBankNumber, int iStart, int iEnd,
                     float *iEdges, int iEdgeLength);

#line 383 "sinqhm.w"

#endif
