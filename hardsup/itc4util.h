/*---------------------------------------------------------------------------
                         I T C L 4 U T I L
                         
  A few utility functions for talking to a Oxford Instruments ITCL-4
  temperature controller via the SINQ setup: TCP/IP--MAC--RS-232--
  ITC-4.
  
  Mark Koennecke, Juli 1997
  
----------------------------------------------------------------------------*/
#ifndef SINQITCL4
#define SINQITCL4

/*----------------------- ERRORCODES--------------------------------------
  Most functions return a negative error code on failure. Error codes 
  defined are those defined for serialsinq plus a few additional ones:
*/

#define ITC4__BADCOM -501
/* command not recognized */
#define ITC4__BADPAR -502
/* bad parameter to command */
#define ITC4__BADMALLOC -503
/* error allocating memory */
#define ITC4__BADREAD  -504
/* error analysing command string on Read */
#define ITC4__NOITC  -510
/* Controller is no ITC-4  */
#define ITC4__BADSET -530
/* failed three times to set temperature */
#define ITC4__READONLY -531
/*------------------------------------------------------------------------*/
typedef struct __ITC4 {
  int iRead;
  int iControl;
  void *pData;
  char pAns[80];
  float fDiv;
  float fMult;
  int iReadOnly;
  int i503;                     /* flag for model 503, understanding float */
} ITC4;

typedef struct __ITC4 *pITC4;

/*-----------------------------------------------------------------------*/
int ITC4_Open(pITC4 * pData, char *pHost, int iPort, int iChannel,
              int iMode);
  /***** creates an ITC4 datastructure and opens a connection to the ITCL4
         controller. Input Parameters are:
                 the hostname
                 the port number
                 the RS-232 channel number on the Mac.
                 iMode: 1 for ReadOnly, 0 for normal mode

          Return values are 1 for success, a negative error code on
          failure.

  */

void ITC4_Close(pITC4 * pData);
  /****** close a connection to an ITC4controller and frees its
          data structure. The only parameter is a pointer to the data 
          structure for this controller. This pointer will be invalid after
          this call.
  */

int ITC4_Config(pITC4 * pData, int iTmo, int iRead,
                int iControl, float fDiv, float fMult);
   /***** configure some aspects of a ITC4temperature controller.
          The parameter are:
                    - a pointer to the data structure for the controller as 
                      returned by OpenITCL4
                    - a value for the connection timeout
                    - the temperature sensor to use for reading the 
                      temperature.
                    - the temperature sensor used by the ITC4controller 
                      for regulating the temperature.
                    - the divisor needed to calculate the real temperature
                      from the sensor.     
          The function returns 1 on success, a negative error code on 
          failure.
    */

int ITC4_Send(pITC4 * pData, char *pCommand, char *pReply, int iLen);
    /******* send a the command in pCommand to the ITC4controller.
             A possible reply is returned in the buffer pReply.
             Maximum iLen characters are copied to pReply.
             The first parameter is a pointer to a ITC4data structure
             as returned by OpenITCL4.
             
             Return values are 1 for success, a negative error code on
             failure.
     */

int ITC4_Read(pITC4 * pData, float *fVal);
     /******* reads the current actual temperature of the sensor
              configured by ConfigITC4for reading. The value is returned
              in fVal. The first parameter is a pointer to a ITCL4
              data structure as returned by OpenITCL4.
 
              Return values are 1 for success, a negative error code on
              failure.
     */

int ITC4_Set(pITC4 * pData, float fVal);
      /****** sets a new preset temperature in the ITC4temperature
              controller. Parameters are:
              - a pointer to a ITC4data structure as returned by OpenITCL4.
              - the new preset value.

              Return values are 1 for success, a negative error code on
              failure.
      */

void ITC4_ErrorTxt(pITC4 * pData, int iCode, char *pError, int iLen);
      /******* translates one of the negative error ITC4error codes
               into text. Maximum iLen bytes will be copied to the
               buffer pError;
      */


#endif
