/*---------------------------------------------------------------------------
  Fix file for David renaming lots of el734 error codes.
  
  Mark Koennecke, October 1998
----------------------------------------------------------------------------*/
#ifndef EL737FIX
#define EL737FIX
#include "asynsrv_errcodes.h"

#define EL737__BAD_HOST	 ASYNSRV__BAD_HOST
#define EL737__BAD_BIND	 ASYNSRV__BAD_BIND
#define EL737__BAD_SENDLEN   ASYNSRV__BAD_SEND_LEN
#define EL737__BAD_SEND	 ASYNSRV__BAD_SEND
#define EL737__BAD_SEND_PIPE  ASYNSRV__BAD_SEND_PIPE
#define EL737__BAD_SEND_UNKN ASYNSRV__BAD_SEND_UNKN
#define EL737__BAD_RECV    ASYNSRV__BAD_RECV
#define EL737__BAD_RECV_PIPE  ASYNSRV__BAD_RECV_PIPE
#define EL737__BAD_RECV_NET   ASYNSRV__BAD_RECV_NET
#define EL737__BAD_SEND_NET   ASYNSRV__BAD_SEND_NET
#define EL737__BAD_RECV_UNKN  ASYNSRV__BAD_RECV_UNKN
#define EL737__BAD_NOT_BCD    ASYNSRV__BAD_NOT_BCD
#define EL737__BAD_RECVLEN    ASYNSRV__BAD_RECV_LEN
#define EL737__BAD_FLUSH      ASYNSRV__BAD_FLUSH
#define EL737__BAD_RECV1      ASYNSRV__BAD_RECV1
#define EL737__BAD_RECV1_PIPE ASYNSRV__BAD_RECV1_PIPE
#define EL737__BAD_RECV1_NET  ASYNSRV__BAD_RECV1_NET
#define EL737__BAD_CONNECT ASYNSRV__BAD_CONNECT
#define EL737__BAD_ID   -99995
#define EL737__BAD_SNTX -99991
#define EL737__BAD_REPLY -99992
#define EL737__BAD_ADR -99993
#define EL737__BAD_RNG -99994
#endif                          /* el734fix */
