#ifndef	    _velsel_def_
#define	    _velsel_def_
/*------------------------------------------------ VelSel_DEF.H Ident V01B
*/
#include    <asynsrv_def.h>
#include    <rs232c_def.h>

#ifndef OffsetOf
#define OffsetOf(type, identifier)  ((size_t)(&((type*) NULL)->identifier))
#endif

enum VelSel_Errors { VELSEL__BAD_TMO = -1,
  VELSEL__BAD_CMD = -3,
  VELSEL__BAD_OFL = -4,
  VELSEL__BAD_ILLG = -5,
  VELSEL__BAD_HOST = -6,
  VELSEL__BAD_SOCKET = -7,
  VELSEL__BAD_BIND = -8,
  VELSEL__BAD_CONNECT = -9,
  VELSEL__BAD_DEV = -10,
  VELSEL__BAD_MALLOC = -11,
  VELSEL__BAD_SENDLEN = -12,
  VELSEL__BAD_SEND = -13,
  VELSEL__BAD_SEND_PIPE = -14,
  VELSEL__BAD_SEND_NET = -15,
  VELSEL__BAD_SEND_UNKN = -16,
  VELSEL__BAD_RECV = -17,
  VELSEL__BAD_RECV_PIPE = -18,
  VELSEL__BAD_RECV_NET = -19,
  VELSEL__BAD_RECV_UNKN = -20,
  VELSEL__BAD_NOT_BCD = -21,
  VELSEL__BAD_RECVLEN = -22,
  VELSEL__BAD_FLUSH = -23,
  VELSEL__BAD_RECV1 = -24,
  VELSEL__BAD_RECV1_PIPE = -25,
  VELSEL__BAD_RECV1_NET = -26,
  VELSEL__BAD_PAR = -29,
  VELSEL__BAD_BSY = -30,
  VELSEL__BAD_OPEN = -31,
  VELSEL__FORCED_CLOSED = -32,
  VELSEL__BAD_STP = -33,
  VELSEL__NOT_OPEN = -35,
  VELSEL__BAD_ASYNSRV = -36,
  VELSEL__BAD_REPLY = -34
};
/*
**	Structure to which the VelSel_Open handle points.
*/
struct VelSel_info {
  struct AsynSrv__info asyn_info;       /* Contains skt, host, port & chan */
  int tmo;
  char eot[4];
  int msg_id;
  int n_replies, max_replies;
  struct RS__MsgStruct to_host;
  struct RS__RespStruct from_host;
};
/*------------------------------------------------ End of VelSel_DEF.H --*/
#endif                          /* _velsel_def_ */
