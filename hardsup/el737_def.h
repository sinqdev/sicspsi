#ifndef	    _el737_def_
#define	    _el737_def_
/*----------------------------------------- [...LIB.SINQ]EL737_DEF.H Ident V02J
**	Definitions for the EL737 Neutron Counter
**
**	On UNIX systems, this file is located in /public/lib/include
**	On VMS  systems, this file is a module in mad_lib:sinq_c.tlb
*/
#include    <asynsrv_def.h>
#include    <rs232c_def.h>

#ifndef OffsetOf
#define OffsetOf(type, identifier)  ((size_t)(&((type*) NULL)->identifier))
#endif

#ifndef	    _EL737_errcodes_
#define	    _EL737_errcodes_
#include    <el737_errcodes.h>
#endif

enum EL737_States {
  UNKNOWN = -2,
  OFFLINE = -1,
  MS = 0x0,
  PTS = 0x1,
  PCS = 0x2,
  LRTS = 0x5,
  LRCS = 0x6,
  PTSP = 0x9,
  PCSP = 0xA,
  LRTSP = 0xD,
  LRCSP = 0xE
};

enum EL737_Consts {
  VMECNT__PRESET_COUNT,
  VMECNT__PRESET_TIME,

  VMECNT__FULL,
  VMECNT__SHORT,
  VMECNT__INCR
};
/*
**	Structure to which the EL737_Open handle points.
*/
struct EL737info {
  struct AsynSrv__info asyn_info;       /* Contains skt, host, port & chan */
  int c5, c6, c7, c8;
  struct RS__MsgStruct to_host;
  struct RS__RespStruct from_host;
};
/*
**	Structure holding everything that is known about a VME Neutron Counter.
*/
struct Counter_State {
  int state;                    /* RS                       */
  char timer[16];               /* RT               \ RA    */
  int cntrs[8];                 /* RC 1 ... RC 8    /       */
  char rates[8][16];            /* RR 1 ... RR 8            */
  char thresh_integ_time[8][16];        /* DI 1 ... DI 8            */
  char rate_integ_time[16];     /* DT                       */
  int analog_indx;              /* DA                       */
  int thresh_indx;              /* DR                       */
  char threshes[8][16];         /* DL 1  ... DL 8           */
  int mon_preset;               /* MP                       */
  char timer_preset[16];        /* TP                       */
};
/*----------------------------------------------------- End of EL737_DEF.H --*/
#endif                          /* _el737_def_ */
