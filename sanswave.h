
/*---------------------------------------------------------------------------
                            S A N S W A V E

  Wavelength calculations for a neutron velocity selector.  Allows to drive
  the neutron wavelength directly.

  copyright: see copyright.h

  Mark Koennecke, October 1998
---------------------------------------------------------------------------*/
#ifndef SANSWAVE
#define SANSWAVE

typedef struct __SANSwave *pSANSWave;
/*-----------------------------------------------------------------------*/

int CalculateLambda(float fRot, float fTilt, float *fLambda);

int MakeSANSWave(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);

int SANSWaveAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);


#endif
