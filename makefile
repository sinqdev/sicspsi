# M. Zolliker 03.2005

%: usage
	@echo

%.o: usage
	@echo

default: usage

usage:
	@ echo ""
	@ echo "Usage:"
	@ echo ""
	@ echo "    make -f makefile_xxx [target]"
	@ echo ""
	@ echo '  where makefile_xxx is one of'
	@ echo ""
	@ ls -1 makefile_* | pr -t -o 4


