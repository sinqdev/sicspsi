#!/usr/bin/tclsh
#------------------------------------------------------------
# This is a little script which issues an interrupt to SICS
# after five seconds
#
# Mark Koennecke, October 2006
#------------------------------------------------------------
if {$argc < 3} {
    puts stdout "Usage:\n\tinterrupt instrument username password"
    exit 1
} 
source sicstcldebug.tcl
initSicsDebug [lindex $argv 0] 
config rights [lindex $argv 1] [lindex $argv 2]
for {set i 0} {$i < 5} {incr i} {
    exec sleep 5
    set txt [sicscommand status]
    if {[string first Eager $txt] < 0} {
	puts $socke "INT1712 3"
        break
    }
}
sicscommand logoff
exit 0


 