#!/usr/bin/tclsh
#-------------------------------------------------------------------------
# This is the regression test for SICS on the RITA2 instrument
#
# Mark Koennecke, November 2006
#------------------------------------------------------------------------
source sicstcldebug.tcl
source insttestutil.tcl
source rita2inventory.tcl

#------ If there is another argument, test at localhost: for debugging...
if {$argc > 0} {
	set instrument local
} else {
	set instrument rita2
} 

initSicsDebug $instrument
config rights lnsmanager lnsSICSlns

set ompos [SICSValue a3]
backup beforetest.tcl

standardPrelude $instrument

#---------- configuration
exe upload
exe append sc a3 0 da3 .1 np 2 ti 2
exe append drive a3 90
exe forcesave insttestbatch.tcl

set notList [list anticollision]

lappend commandList "sc a3 0 da3 .1 np 2 ti 2"
lappend commandList "statusinfo"
lappend commandList "hmframe 0"
lappend commandList "ritamode none"
lappend commandList "ritamode flat"
lappend commandList "ritamode focus"
lappend commandList "ritamode monoim"
lappend commandList "w5s"
lappend commandList "slits"
lappend commandList "blades"
lappend commandList "motors"

#--------- the TAS set...
lappend commandList "do insttestbatch.tcl"
lappend commandList "ou a4"
lappend commandList "fi a4"
lappend commandList "fix a4"
lappend commandList "cl a4"
lappend commandList "co ti 5"
lappend commandList "pr a3,a4"
lappend commandList "se title hugo"
lappend commandList  "li"

#-------- the TASUB set
lappend commandList "ref clear"
lappend commandList "cell 3.844 3.844 20.086 90 90 90"
lappend commandList "ref 0 0 6 -158.52 -74.77 .11 -.13 5 5"
lappend commandList "ref 1 0 0  -63.06 -63.48 .11 -.13 5 5"
lappend commandList "makeub 1 2"
lappend commandList "dr qh 1 0 0 en .1"
lappend commandList "listub" 

lappend interruptList "dr om 90"
lappend interruptList "co ti 120"
lappend interruptList "exe insttestbatch.tcl"

lappend varList [list "tasub mono dd" 3.3546 .01]
lappend varList [list "tasub mono ss" 1 .01]
lappend varList [list "tasub ana dd" 3.3546 .01]
lappend varList [list "tasub ana ss" 1 .01]
lappend varList [list analyzerdistance 33.9 .1]
lappend varList [list sampledistance 120.699 .1]
lappend varList [list bladedistance 2.5 .1]
lappend varList [list detectordistance 2.5 .1]


set par(inventory) $inventory
set par(driveCommand) dr
set par(motorList) $motors
set par(notList) $notList
set par(commandList) $commandList
set par(interruptList) $interruptList
set par(instrument) $instrument
set par(user) lnsmanager
set par(password) lnsSICSlns
set par(varList) $varList

#================= actually test =====================================
set counters [runStandardTests par]

#--------- Test detection system
puts stdout "Testing detector"
set det 1
co ti 600
set counts [SICSValue "hm sum 0 128 0 128"]
if {$counts <= 0}  {
   set det 0
}
if {$det == 1} {
	puts stdout "Detector passed"
} else {
   puts stdout "Check detector, found no noise after 10 minutes counting"
}
#--------- put detector back where it was
standardRestore
recover beforetest.tcl
dr om $ompos

#------------ print a summary
set inst [string toupper $instrument]
puts stdout "Test Summary for $inst"
printStandardSummary $counters

