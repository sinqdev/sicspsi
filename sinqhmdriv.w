\subsubsection{SINQ Histogram memory}
This is a driver for the SINQ developed histogram memory. This HM can
operate in normal, TOF and stroboscopic mode. All the real time processing
for this HM is done by an on-board computer in a VME crate. This on board
computer also runs TCP/IP and a server program which allows for
configuration and communication with the HM. A tricky bit is, that
configuration and communication operate differently. For configuration an
connection to the main server is installed which handles the configuration
requests. For starting data collection and retrieval of information a second
connection is needed. This is obtained by sending a request to the main
server on the on board computer. This main server will than spawn a second
process on the on board computer which is dedicated to serving our requests.
The mainserver sends  a packet containing the new port number our secondary
server is listening to. Than the driver can connect to this secondary server
in order to exchange data. More details on this scheme can be found in the
manual for the SINQ-histogram memory. A further complication arises from the
fact that the actual counting control, such as monitor handling, starting
and stopping etc. is handled via a EL737 counter box, which needs to be kept
track off as well. All this results in a rather complicated driver. This is
already reflected by the driver private data structure:

@d SQType @{
      typedef struct __SinqHMDriv {
                                    pCounter pCounter;
                                    pSINQHM  pMaster;
                                    int iLastHMError;
                                    int iLastCTError;
				    HistMode eHistMode;
				    int iBinWidth;
                                    OverFlowMode eFlow;
			            int extraDetector;
                                   } SinqHMDriv;
@}

pCounter is a pointer to the EL737 counter box to use for count control.
pMaster is an internal data structure for the interface functions to the
SINQ histogram memory. iLastHMError keeps the last histogram memory error,
iLasyCTError is set to 1 when a counter error happened. This is necessary to
implement the GetError function.

Please note that this driver needs some options in the histogram memories
database:
\begin{itemize}
\item Control must have as value a configured EL737 counter box.
\item HMComputer is the name of the histogram memory computer.
\item HMPort is the port on which the histogram memory computer listens for
requests.
\end{itemize}
 
The driver implements all the functions specified in the driver interface.
Please note that these contain functions for the deletion of driver private
data structures which will be automatically called form DeleteHistDriver.
Therefore the only function to define is CreateSINQDriver which sets things
up. Another function is isSINQHMDriv which tests if the driver given as an
argument actually is a SINQHM driver. This is currently only used in 
amorstat.c which has to circumvent normal SICS mechanisms for performance
 reasons. 

@d Protos @{
   pHistDriver CreateSINQDriver(pStringDict pOption);
   int isSINQHMDriv(pHistDriver test);
@} 

@o sinqhmdriv.i -d @{
/*--------------------------------------------------------------------------
                    	 S I N Q H M 

  A driver for the SINQ histogram memory.

  Mark Koennecke, April 1997

  copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef SINQHMDRIVER
#define SINQHMDRIVER
#include "hardsup/sinqhm.h"
@<SQType @>
/*-------------------------------------------------------------------------*/
@<Protos @>

#endif
@}




