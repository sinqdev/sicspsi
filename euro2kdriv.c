/*---------------------------------------------------------------------------
euro2kdriv.c

Driver for the EuroTherm 2000 controllers (ModBus protocol)

Markus Zolliker, August 2005
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include <math.h>
#include <tcl.h>
#include <fortify.h>
#include <sics.h>
#include <splitter.h>
#include <obpar.h>
#include <devexec.h>
#include <nserver.h>
#include <interrupt.h>
#include <emon.h>
#include <evcontroller.h>
#include <evcontroller.i>
#include <sicsvar.h>
#include <evdriver.i>
#include <rs232controller.h>
#include "modbus.h"
#include "fsm.h"
#include "initializer.h"

#define EURO2K_SET 1
#define EURO2K_MODE 2
#define EURO2K_PAR 3

typedef enum { read_only, settable, to_set } Euro2kAccess;

typedef struct Euro2kPar {
  struct Euro2kPar *next;
  char *name;
  char *unit;
  char *fmt;
  int adr;
  ModBusType type;              /* 0: float, 1: int, 2: time */
  Euro2kAccess set;             /* 0: read-only; 1: settable; 2: to be set */
  float par;
} Euro2kPar;

typedef struct {
  EaseDriv d;
  char *unit;
  int mode;
  char *script;
  int warned;
  float value;
  float output;
  float position;
  float range;
  float asymmetry;
  float setpoint;
  Euro2kPar *pars, *readPar, *setPar;
  Statistics *stat;
  double lastRd;
  char *model;
  int rdonly;
} Euro2k;

static ParClass euro2kClass = { "EURO2K", sizeof(Euro2k) };

/*----------------------------------------------------------------------------*/
static int Euro2kMakePar(void *object, void *userarg, int argc,
                         char *argv[])
{
  Euro2k *drv = ParCast(&euro2kClass, object);
  Euro2kPar *par, **last;
  int iarg;

  assert(drv);
  if (argc < 2)
    goto Usage;
  last = &drv->pars;
  for (par = drv->pars; par != NULL; par = par->next) {
    if (strcasecmp(argv[0], par->name) == 0) {
      break;
    }
    last = &par->next;
  }
  if (par == NULL) {
    par = calloc(1, sizeof(Euro2kPar));
    if (par == NULL)
      return 0;
    *last = par;
    par->unit = NULL;
    par->fmt = NULL;
    par->par = PAR_NAN;
  } else {
    if (par->unit) {
      free(par->unit);
      par->unit = NULL;
    }
    if (par->fmt) {
      free(par->fmt);
      par->fmt = NULL;
    }
  }
  par->name = strdup(argv[0]);
  iarg = 1;
  if (strcasecmp(argv[iarg], "w") == 0) {       /* writeable */
    par->set = settable;
    iarg++;
  } else {
    par->set = read_only;
  }
  par->type = modBusFloat;
  if (iarg >= argc)
    goto Usage;
  if (strcasecmp(argv[iarg], "int") == 0) {
    par->type = modBusInt;
    iarg++;
    par->fmt = "%.0f";
  } else if (strcasecmp(argv[iarg], "time") == 0) {
    par->type = modBusTime;
    iarg++;
    par->unit = "sec";
    par->fmt = "%.4g";
  } else {
    par->type = modBusFloat;
  }
  if (iarg >= argc)
    goto Usage;
  par->adr = atoi(argv[iarg]);
  iarg++;
  if (iarg < argc) {
    if (argv[iarg] && argv[iarg][0]) {
      par->unit = argv[iarg];
    }
    iarg++;
    if (iarg < argc) {
      if (argv[iarg] && argv[iarg][0]) {
        par->fmt = argv[iarg];
      }
      iarg++;
      if (iarg < argc)
        goto Usage;
    }
  }
  if (par->unit)
    par->unit = strdup(par->unit);
  if (par->fmt)
    par->fmt = strdup(par->fmt);
  ParInitPar(object, par->name);
  EaseParHasChanged();
  drv->readPar = NULL;
  return 1;
Usage:
  ParPrintf(object, eError,
            "Usage: %s makepar <name> [w] [int|time] adr [unit] [fmt]",
            drv->d.b.p.name);
  return 0;
}

/*----------------------------------------------------------------------------*/
void Euro2kParDef(void *object)
{
  Euro2k *drv = ParCast(&euro2kClass, object);
  ParData *obj = object;
  Euro2kPar *par, *next;
  FILE *saveFile;
  char *w, *t, *u, *f;
  float old;

  static char *modeList[] = { "auto", "manual", NULL };

  ParName("");
  ParTail(drv->unit);
  ParFloat(&drv->value, PAR_NAN);

  ParName("unit");
  if (drv->unit == NULL) {
    ParAccess(usUser);
  }
  ParLogAs(NULL);
  ParStr(&drv->unit, NULL);

  ParName("mode");
  ParEnum(modeList);
  ParList(0);
  EaseUpdate(EURO2K_MODE);
  ParInt(&drv->mode, 1);

  ParName("model");
  ParLogAs(NULL);
  ParStr(&drv->model, NULL);

  /*
     ParName("pbPow"); ParAccess(usUser); ParFloat(&drv->pbPow, 0.0);
     ParName("pbMin"); ParAccess(usUser); ParFloat(&drv->pbMin, 8.0);
     ParName("pbScl"); ParAccess(usUser); ParFloat(&drv->pbScl, 3.0);
   */

  old = 0.0;
  ParName("pbPow");
  ParSave(0);
  ParFloat(&old, 0.0);
  ParName("pbMin");
  ParSave(0);
  ParFloat(&old, 0.0);
  ParName("pbScl");
  ParSave(0);
  ParFloat(&old, 0.0);

  ParName("output");
  ParTail("%");
  ParFloat(&drv->output, PAR_NAN);

  ParName("position");
  ParTail("%");
  ParAccess(usUser);
  ParFloat(&drv->position, 50.0);

  ParName("asymmetry");
  ParAccess(usUser);
  ParFloat(&drv->asymmetry, PAR_NAN);

  ParName("range");
  ParAccess(usUser);
  ParFloat(&drv->range, PAR_NAN);

  ParName("set");
  ParTail(drv->unit);
  EaseUpdate(EURO2K_SET);
  ParFloat(&drv->setpoint, PAR_NAN);

  ParName("rdonly");
  ParLogAs(NULL);
  ParAccess(usUser);
  ParInt(&drv->rdonly, 0);

  ParName("task");
  ParAccess(usUser);
  ParList(NULL);
  ParSave(1);
  ParStr(&drv->script, "0");

  saveFile = ParSaveFile();
  for (par = drv->pars; par != NULL; par = par->next) {
    if (par->adr > 0) {
      if (saveFile) {
        if (par->set) {
          w = "w ";
        } else {
          w = "";
        }
        if (par->type == modBusInt) {
          t = "int ";
        } else if (par->type == modBusTime) {
          t = "time ";
        } else {
          t = "";
        }
        if (par->unit) {
          u = par->unit;
        } else {
          u = "";
        }
        if (par->fmt) {
          f = par->fmt;
        } else {
          f = "";
        }
        fprintf(saveFile, "%s makepar %s %s%s%d \"%s\" \"%s\"\n",
                obj->name, par->name, w, t, par->adr, u, f);
      }
      ParName(par->name);
      if (par->unit) {
        if (strcmp(par->unit, "@") == 0) {
          ParTail(drv->unit);
        } else {
          ParTail(par->unit);
        }
      } else {
        ParList(NULL);
      }
      if (par->fmt) {
        ParFmt(par->fmt);
      }
      if (par->set >= settable) {
        if (EaseUpdate(EURO2K_PAR)) {
          old = par->par;
          ParFloat(&par->par, PAR_NAN);
          if (fabsf(par->par - old) > 1e-5 * (par->par + old)) {
            par->set = to_set;
          }
        } else {
          ParFloat(&par->par, PAR_NAN);
        }
      } else {
        ParFloat(&par->par, PAR_NAN);
      }
    }
  }

  ParName("makepar");
  ParAccess(usUser);
  ParCmd(Euro2kMakePar, NULL);

  EaseBasePar(drv);
  EaseDrivPar(drv, "%.5g", drv->unit);
  ParStdDef();
  EaseMsgPar(drv);
  if (ParActionIs(PAR_KILL)) {
    next = drv->pars;
    for (par = next; par != NULL; par = next) {
      next = par->next;
      if (par->name) {
        free(par->name);
        par->name = NULL;
      }
      if (par->unit) {
        free(par->unit);
        par->unit = NULL;
      }
      if (par->fmt) {
        free(par->fmt);
        par->fmt = NULL;
      }
      free(par);
    }
    drv->pars = NULL;
    drv->readPar = NULL;
    if (drv->stat) {
      StatisticsKill(drv->stat);
    }
  }
}

double getTime(void)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + 1.0e-6 * tv.tv_usec;
}

/*----------------------------------------------------------------------------*/
float Euro2kGet(EaseBase * eab, Euro2kPar * par)
{
  /* a hack for converting different time formats on 2216 and 3216 models) */
  float value;

  value = ModBusGet(eab, par->adr, par->type);
  if (par->type == modBusTime && value > 100000.0) {
    par->type = modBusFloat;
    value = ModBusGet(eab, par->adr, par->type);
  }
  return value;
}

/*----------------------------------------------------------------------------*/
static long Euro2kRead(long pc, void *object)
{
  Euro2k *drv = ParCast(&euro2kClass, object);
  EaseBase *eab = object;
  Euro2kPar *par, *par0;
  Tcl_Interp *pTcl = NULL;
  char *p;
  int l, m, iRet;
  float dif, a, pb;
  char buf[4];
  Statistics *old;
  double now, delta;

  switch (pc) {
  default:                     /* FSM BEGIN ****************************** */
    EasePchk(drv);
    if (drv->rdonly) {
      if (getTime() < drv->lastRd + 10) {
        goto fsm_quit;
      }
    }
    if (drv->script && drv->script[0] != '\0'
        && 0 != strcmp(drv->script, "0")) {
      pTcl = InterpGetTcl(pServ->pSics);
      old = StatisticsBegin(drv->stat);
      iRet = Tcl_Eval(pTcl, drv->script);
      StatisticsEnd(old);
      if (iRet == TCL_OK) {
        if (drv->warned > 0)
          drv->warned--;
      } else if (drv->warned < 3) {
        drv->warned++;
        ParPrintf(drv, eError, "ERROR: %s in %s.task '%s'", Tcl_GetStringResult(pTcl),
                  eab->p.name, drv->script);
      }
    }
    if (EaseGetUpdate(drv, EURO2K_MODE))
      goto skipMode;
    ModBusRequestValue(eab, 273, modBusInt);    /* get manual or auto */
    return __LINE__;
  case __LINE__:                 /**********************************/
    if (!EaseGetUpdate(drv, EURO2K_MODE)) {
      m = ModBusGetValue(eab, modBusInt);
      if (m != drv->mode) {     /* mode changed manually -> change disp */
        drv->mode = m;
        EaseSetUpdate(drv, EURO2K_MODE, 1);
      }
    }
  skipMode:

    ModBusRequestValues(eab, 1, 9);
    return __LINE__;
  case __LINE__:                 /**********************************/
    drv->value = ModBusGet(eab, 1, modBusFloat);
    if (!EaseGetUpdate(drv, EURO2K_SET)) {
      drv->setpoint = ModBusGet(eab, 2, modBusFloat);
    }
    drv->output = ModBusGet(eab, 3, modBusFloat);
    for (par = drv->pars; par != NULL; par = par->next) {
      if (par->adr > 3 && par->adr <= 9 && par->set != to_set) {
        par->par = Euro2kGet(eab, par);
      }
    }
    now = getTime();
    delta = now - drv->lastRd;
    drv->lastRd = now;
    if (delta > 10)
      delta = 10;
    a = drv->output * delta / drv->range;
    if ((drv->position > 50.) == (drv->output > 0)) {
      drv->position += a * (1 - drv->asymmetry * 0.01);
    } else {
      drv->position += a * (1 + drv->asymmetry * 0.01);
    }
    if (drv->position < 0)
      drv->position = 0;
    if (drv->position > 100)
      drv->position = 100;

/*
    if (drv->pbPow <= 0.0) goto getPars;
    pb = drv->pbMin + pow(drv->value * drv->pbScl, drv->pbPow);
    if (pb > 999.) pb=999.;
    ModBusPutValue(eab, 6, modBusFloat, pb);
    
  return __LINE__; case __LINE__:
  getPars:
*/
    par = drv->readPar;
    par0 = par;
    do {
      if (par == NULL) {
        par = drv->pars;
      } else {
        par = par->next;
      }
      if (par == par0)
        goto skipPar;
    } while (par == NULL || par->adr <= 9 || par->set == to_set);
    ModBusRequestValue(eab, par->adr, par->type);
    drv->readPar = par;
    return __LINE__;
  case __LINE__:                 /**********************************/
    par = drv->readPar;
    if (par->set != to_set) {
      par->par = Euro2kGet(eab, par);
    }
  skipPar:
    if (eab->p.verbose >= 3) {
      eab->p.verbose--;
      if (eab->p.verbose < 3)
        eab->p.verbose = 0;
    }

  finish:
    ParLog(drv);
  fsm_quit:return 0;
  }                             /* FSM END ******************************** */
}

/*----------------------------------------------------------------------------*/
static long Euro2kSet(long pc, void *object)
{
  Euro2k *drv = ParCast(&euro2kClass, object);
  EaseBase *eab = object;
  Euro2kPar *par;
  char *p;
  int l;
  int upd;
  char buf[4];
  float value;

  switch (pc) {
  default:                     /* FSM BEGIN ****************************** */
    EasePchk(drv);
  loop:
    if (drv->rdonly)
      goto fsm_quit;
    upd = EaseNextUpdate(drv);
    if (upd == EASE_RUN) {
      drv->setpoint = drv->d.targetValue;
      goto run;
    }
    if (upd == EURO2K_MODE)
      goto mode;
    if (upd == EURO2K_SET)
      goto run;
    for (par = drv->pars; par != NULL; par = par->next) {
      if (par->set == to_set) {
        ModBusRequestValue(eab, par->adr, par->type);
        goto readIt;
      }
    }
    goto fsm_quit;
  run:
    ModBusRequestValue(eab, 2, modBusFloat);
    return __LINE__;
  case __LINE__:                 /**********************************/
    value = ModBusGetValue(eab, modBusFloat);
    if (fabsf(value - drv->setpoint) <= 1e-5 * (value + drv->setpoint))
      goto skipset;
    ModBusPutFloats(eab, 2, 1, &drv->setpoint);
    return __LINE__;
  case __LINE__:                 /**********************************/
  skipset:
    if (drv->mode == 0)
      goto loop;
    drv->mode = 0;
  mode:
    if (drv->mode)
      drv->mode = 1;
    ModBusPutValue(eab, 106, modBusInt, drv->mode * 5); /* set display std or blank */
    return __LINE__;
  case __LINE__:                 /**********************************/
    ModBusPutValue(eab, 273, modBusInt, drv->mode);     /* set manual to 0 */
    goto setIt;
  readIt:
    drv->setPar = par;
    return __LINE__;
  case __LINE__:                 /**********************************/
    par = drv->setPar;
    value = Euro2kGet(eab, par);
    par->set = settable;
    if (fabsf(par->par - value) <= 0.00001 * fabsf(par->par + value)) {
      goto loop;
    }
    ModBusPutValue(eab, par->adr, par->type, par->par);
  setIt:
    return __LINE__;
  case __LINE__:                 /**********************************/
    goto loop;

  fsm_quit:return 0;
  }                             /* FSM END ******************************** */
}

/*----------------------------------------------------------------------------*/
static long Euro2kStart(long pc, void *object)
{
  Euro2k *drv = ParCast(&euro2kClass, object);
  EaseBase *eab = object;
  int number;
  char hex[8];

  switch (pc) {
  default:                     /* FSM BEGIN ****************************** */
    EasePchk(drv);
    ModBusRequestValue(eab, 122, modBusInt);
    return __LINE__;
  case __LINE__:                 /**********************************/
    if (eab->state != EASE_read) {
      ParPrintf(drv, eError, "bad or no response on ModBus");
      goto quit;
    }
    number = ModBusGetValue(eab, modBusInt);
    if (drv->model != NULL) {
      free(drv->model);
    }
    snprintf(hex, sizeof hex, "%4.4X", number);
    if (hex[0] == 'E')
      hex[0] = '3';
    if (hex[2] == '3') {
      hex[3] = '2';
    } else if (hex[2] == '6') {
      hex[2] = '1';
      hex[3] = '6';
    } else {
      hex[2] = '0';
      hex[3] = hex[2];
    }
    drv->model = strdup(hex);
    ModBusRequestValue(eab, 107, modBusInt);
    return __LINE__;
  case __LINE__:                 /**********************************/
    number = ModBusGetValue(eab, modBusInt);
    snprintf(eab->version, sizeof(eab->version), "%s V%4.4X", drv->model,
             number);
    ParPrintf(drv, eLog, "connected to Eurotherm %s", eab->version);
    if (drv->unit != NULL)
      goto unitGiven;
    ModBusRequestValue(eab, 516, modBusInt);
    return __LINE__;
  case __LINE__:                 /**********************************/
    number = ModBusGetValue(eab, modBusInt);
    if (number == 0) {
      drv->unit = strdup("C");
    } else if (number == 2) {
      drv->unit = strdup("K");
    }
  unitGiven:
    if (drv->rdonly)
      goto quit;
    ModBusPutValue(eab, 111, modBusFloat, drv->d.upperLimit);
    return __LINE__;
  case __LINE__:                 /**********************************/
    ModBusPutValue(eab, 112, modBusFloat, drv->d.lowerLimit);
    return __LINE__;
  case __LINE__:                 /**********************************/
    FsmCall(Euro2kRead);
    return __LINE__;
  case __LINE__:                 /**********************************/

  quit:
    return 0;
  }                             /* FSM END ******************************************* */
}

/*----------------------------------------------------------------------------*/
static int Euro2kInit(SConnection * con, int argc, char *argv[],
                      int dynamic)
{
  /* args:
     MakeObject objectname euro2k <rs232>
     <host> <port>
   */
  Euro2k *drv;
  char sn[64];

  drv = EaseMakeDriv(con, &euro2kClass, argc, argv, dynamic, 7,
                     Euro2kParDef, ModBusHandler, Euro2kStart, NULL,
                     Euro2kRead, Euro2kSet);
  if (drv == NULL)
    return 0;
  drv->pars = NULL;
  drv->readPar = NULL;
  snprintf(sn, sizeof sn, "%s task", argv[1]);
  drv->stat = StatisticsNew(sn);
  drv->lastRd = getTime();
  setRS232ReplyTerminator(drv->d.b.ser, "");
  setRS232SendTerminator(drv->d.b.ser, "");
  return 1;
}

/*----------------------------------------------------------------------------*/
void Euro2kStartup(void)
{
  ParMakeClass(&euro2kClass, EaseDrivClass());
  MakeDriver("EURO2K", Euro2kInit, 0, "Eurotherm 2xxx");
}
