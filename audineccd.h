/*
 * -*- linux-c-kernel -*-
 * Linux Kernel Module for the Audine Camera
 * Copyright (C) 2000 Peter Kirchgessner
 * http://www.kirchgessner.net, mailto:peter@kirchgessner.net
 *
 * Modified by F. Manenti <oss_astr_cav@arcanet.it> for the use in the
 * NOVA environnement (nova.sourceforge.net)
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * The sample interface routines for the module have been taken from the
 * Linux Kernel Module Programming Guide by Ori Pomerantz contained
 * in the Linux Documentation Project.
 *
 */
#ifndef _AUDINECCD_H_
#define _AUDINECCD_H_

#ifndef MODULE
#   define MODULE
#endif

/* Device Declarations **************************** */
/* The name for our device, as it will appear in /proc/devices */
#define DEVICE_NAME "audineccd"

/* Size if a buffer that must keep at least */
/* one line of image data (2 bytes per pixel) */
#define BUF_LEN 4096

#define PORT_STEP 1

#define ERR_NO_PORT  ENXIO

#undef PDEBUG
#ifdef DEBUG
# ifdef __KERNEL__
#   define PDEBUG(fmt, args...) printk(KERN_DEBUG "audine driver: ", fmt, ## args)
# else
#   define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
# endif

#else
#   define PDEBUG(fmt, args...) /* Nothing ... */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...)   /* Nothing */

#endif

struct ccd_capability {
  char name[128];               /* Name of CCD chip */
  int width;                    /* Sensible image width */
  int height;                   /* Sensible image height */
  int color;                    /* Has color */
  int minwidth;                 /* Actual geometry frame width */
  int minheight;                /* Actual geometry frame height */
};

/* IOCTL MACROS */

#define CCD_MGK 0xEE            // Very high ... to register !!!!

#define CCD_RD_CHIP  _IOR(CCD_MGK, 0x01, struct ccd_capability)
#define CCD_RD_CCDL  _IOR(CCD_MGK, 0x02, struct ccd_capability)
#define CCD_RD_GEOM  _IOR(CCD_MGK, 0x03, struct ccd_capability)
#define CCD_RD_IMG   _IOR(CCD_MGK, 0x04, unsigned long) /* Read image */

#define CCD_CLR      _IOW(CCD_MGK, 0x05, unsigned long) /* chip Clearing  */

#define CCD_SWTC_AMP _IOW(CCD_MGK, 0x06, unsigned long)
#define CCD_SWTC_SHT _IOW(CCD_MGK, 0x07, unsigned long)
#define CCD_SWTC_AX0 _IOW(CCD_MGK, 0x08, unsigned long)
#define CCD_SWTC_AX1 _IOW(CCD_MGK, 0x09, unsigned long)

#define CCD_SET_AMP _IOW(CCD_MGK, 0x0a, unsigned long)
#define CCD_SET_SHT _IOW(CCD_MGK, 0x0b, unsigned long)
#define CCD_SET_AX0 _IOW(CCD_MGK, 0x0c, unsigned long)
#define CCD_SET_AX1 _IOW(CCD_MGK, 0x0d, unsigned long)

#define CCD_SET_BNN _IOW(CCD_MGK, 0x0e, struct ccd_capability)  // Binning


#define CCD_AV_IMG  _IOR(CCD_MGK, 0x0f, unsigned long)  // ma serve ????

#define CCD_SET_WND _IOW(CCD_MGK, 0x10, struct ccd_capability)  // Window
#define CCD_RST_WND _IOW(CCD_MGK, 0x11, unsigned long)  // CCD

#define CCD_SET_PRT _IOW(CCD_MGK, 0x12, unsigned long)
#define CCD_RD_VER  _IOW(CCD_MGK, 0x13, struct ccd_capability)  // Version
