\subsection{Serial Connections}
This object allows to send arbitrary strings to a serial port in the SINQ
hardware setup system. This means a message is packaged and forwarded to  to 
the Macintosh serving the port. And data is read back again from there. This
facility has been devised in order to write environment device drivers in
the macro language and as help for SICS instrument managers. 

In contrast to the many SICS objects this object is defined for the Tcl
interpreter. This is because it is in use within the psish Tcl interpreter
as well. There is only one function, SerialInit, which adds the serial
command to the SICS interpreter. 

@o serial.h @{
/*--------------------------------------------------------------------------
                         S I C S  S E R I A L

  This object adds the serial command to the Tcl-interpreter within SICS.
  Furthermore the commands sr1-sr6 are added as predefined names for 
  possible connections. 

  Mark Koennecke, January 1998
----------------------------------------------------------------------------*/
#ifndef SICSERIAL
#define SICSERIAL
   int SerialInit(SConnection *pCon,SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);
#endif
@}
 