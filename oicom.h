/*---------------------------------------------------------------------------
oicom.h

Communication routines for Oxford Instruments equipment
  
Markus Zolliker, Aug 2004
----------------------------------------------------------------------------*/
#include "rs232controller.h"
#include "eve.h"

int OiHandler(Eve * eve);
double OiGetFlt(Eve * eve, int dig, int *pdig);
void OiSet(Eve * eve, char *cmd, double val, int dig);
