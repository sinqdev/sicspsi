/*---------------------------------------------------------------------------

		T H E   L N S    R \" U N B U F F E R
		
	The LNS has devised a special scheme to operate an instrument
	via R\"un sequeneces and buffers. A R\"unbuffer is a series of
	commands which are collected in a buffer. This buffer is
	implemented here. A buffer can be added to, printed loaded from 
	a file etc. and can be executed. 
	
	The next schem is the R\"unlist which is a stack of R\"unbuffers.
	That list can be exeuted as well. It gets a buffer from the 
	bottom of the stack and executes it and does so until the stack
	is empty. While this is happening you are able to add other
	buffers to the top of the stack. This schem is implemented in module
	ruli.
	
	So, here is all necessary to deal with an individual buffer.
	For Lists A. Reitsma's lld package will be used. This package
	identifies a list by an integer handle.
	
	Mark Koennecke, January 1996
	
	copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef RUENBUFFER
#define RUENBUFFER
#include <sics.h>


typedef struct {
  pObjectDescriptor pDes;       /* needed */
  char *name;                   /* BufferName */
  int iLineList;                /* Handle to the Line List */
} RuenBuffer, *pRuenBuffer;

/*--------------------- live & death ----------------------------------- */
pRuenBuffer CreateRuenBuffer(char *name);
void DeleteRuenBuffer(void *pSelf);
pRuenBuffer CopyRuenBuffer(pRuenBuffer pOld, char *NewName);

/*--------------------- operations --------------------------------------*/

int BufferAppendLine(pRuenBuffer self, char *line);
int BufferDel(pRuenBuffer self, int iLine);
 /*
    deletes line iLine from the RuenBuffer self

    ------------------------------------------------------------------------- */
int BufferInsertAfter(pRuenBuffer self, int iLine, char *line);
 /*
    inserts line line AFTER line number iLine in the RuenBuffer self
    ------------------------------------------------------------------------- */
int BufferPrint(pRuenBuffer self, SConnection * pCon);
 /* 
    lists the contents of the RuenBuffer on the Connection pCon
    ------------------------------------------------------------------------ */
int BufferReplace(pRuenBuffer self, char *pattern, char *pReplace);
 /*
    replaces all occurences of the string pattern in the whole RuenBuffer
    by the replacement string pReplace.
    ------------------------------------------------------------------------- */
int BufferRun(pRuenBuffer self, SConnection * pCon, SicsInterp * pSics);
 /*
    executes the lines of the Ruenbuffer one by one. 
    Returns 1 on success, 0 on error.
    ------------------------------------------------------------------------- */
int BufferSave(pRuenBuffer self, char *file);
 /*
    writes the contents of Ruenbuffer self to the file specified by
    file. 
    Returns 1 on success, 0 on error.
    -------------------------------------------------------------------------- */
int BufferLoad(pRuenBuffer self, char *file);
 /*
    reads the contents of file into the RuenBuffer self. 
    Returns 1 on success, 0 on error.
  */
/* ------------------------ object functions ----------------------------*/
int InitBufferSys(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);
int BufferCommand(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);
int BufferAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);

/* ----------------------- utility --------------------------------------*/
pRuenBuffer FindRuenBuffer(SicsInterp * pSics, char *name);
   /*
      similar to FindCommand in SCinter.h. But checks the object found if
      it is a RuenBuffer.
      Returns NULL if no RuenBuffer with this name could be found.
      Returns a pointer to the RuenBuffer, when a RuenBuffer of this
      name could be found in the interpreter pSics
      ---------------------------------------------------------------------------- */
#endif
