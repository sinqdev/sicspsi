/**
   This is a little CLi program to control the picoscope frequency 
   generator coming with the Pico Labs Picoscope 2206a oscilloscope

   Mark Koennecke, May 2015

 */

#include <sys/types.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <libps2000a-1.1/ps2000aApi.h>
#ifndef PICO_STATUS
#include <libps2000a-1.1/PicoStatus.h>
#endif


int main(int argc, char *argv[])
{
  int freq, ampl, status;
  short handle;

  if(argc < 3) {
    printf("Usage:\n\tpicosig freq ampl\n");
    exit(1);
  }
  
  freq = atoi(argv[1]);
  ampl = atoi(argv[2]);

  printf("Found freq, ample = %d %d\n", freq, ampl);

  status = ps2000aOpenUnit(&handle, NULL);
  if(status != PICO_OK){
    printf("Failed to open picoscope with %d\n", status);
    exit(1);
  }
  
  status = ps2000aSetSigGenBuiltIn(handle,
				   0,
				   (unsigned long)ampl,
				   PS2000A_SINE,
				   (float)freq,
				   (float)freq,
				   0,
				   0,
				   0,
				   0,
				   0,
				   0,
				   0,
				   0,
				   0);
  if(status != PICO_OK){
    printf("Failed to configure signal generator with %d\n", status);
    exit(1);
  }

  sleep(5);

  ps2000aCloseUnit(handle);
  printf("Done\n");
}
