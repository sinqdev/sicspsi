# Picoscope

The picoscope USB oscilloscope is used as a frequency generator for spin flippers at SINQ. 
As USB is not the bus of SINQ, we use a raspberry PI as a server for controlling the spin flipper from 
SICS. This is the code for this.

The first interface is the command line program picocontrol which is run via xinetd as a server. 
This has the disadvantage that only one connection is possible and connections rejections are possible 
because xinetd does not reliably kill picocontrol on a disconnect. 

The second interface is a libuv based own server, picoserverli. This is as of now (07/2017) the state of the art. 
I start directly from /etc/inittab. An example can be found in the end of the provided inittab file. 

On ubuntu you may use the system supplied libuv as installable via apt-get. On the raspberry PI you have 
to build libuv from sources. 
