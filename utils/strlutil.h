/*
 * strlutil.h
 * Provide strcpy, strlcat when not provided by ssystem
 *
 *  Created on: Mar 25, 2010
 *      Author: koennecke
 */

#ifndef STRLUTIL_H_
#define STRLUTIL_H_

#ifndef strlcpy
size_t strlcat(char *dst, const char *src, size_t siz);
size_t strlcpy(char *dst, const char *src, size_t siz);
#endif


#endif /* STRLUTIL_H_ */
