/*
 * This is a little utility for querying the SICS mongo DB log. 
 *
 * copyright: GPL
 *
 * Mark Koennecke, February 2016
 *
 * modified for the new GELF conforming log messages and the new organisation of the log
 *
 * Mark Koennecke, May 2017 
 */
#include <sicslogquery.h>
#include <bson.h>
#include <logv2.h>

void formatSeverity(unsigned int severity, char *buffer, unsigned int bufferLength)
{
  static const char *severityText[] = {"FATAL",
			       "ERROR",
                               "WARNING",
                               "INFO",
                               "VERBOSE",
                               "DEBUG",
                               "INVALID"
                                };

  if(severity > DEBUG){
    severity = INVALID;
  }
  strncpy(buffer,severityText[severity-1],bufferLength);
} 

/*----------------------------------------------------------------------------------------*/
static void  ResultPrint(const bson_t *doc, void *userData)
{
  bson_iter_t iter;
  unsigned int severity;
  int length;
  const char *message, *timeText, *sub;
  char sevBuf[20];

  bson_iter_init(&iter,doc);
  bson_iter_find(&iter,"_timetext");
  timeText = bson_iter_utf8(&iter,&length);
  bson_iter_init(&iter,doc);
  bson_iter_find(&iter,"level");
  severity = bson_iter_int32(&iter);
  formatSeverity(severity,sevBuf,sizeof(sevBuf));
  bson_iter_find(&iter,"_sub");
  sub = bson_iter_utf8(&iter,&length);
  bson_iter_find(&iter,"short_message");
  message = bson_iter_utf8(&iter,&length);

  fprintf(stdout,"%s    %s   %s   %s\n",timeText,sevBuf, sub, message); 
  
}

int main(int argc, char *argv[])
{
  int status;
  char *error;

  /*  
sicslogSetup("mongodb://userRW:2yfbt4AwBw4RY7j@mgdbaas001:27017,mgdbaas002:27017,mgdbaas003:27017/testDB?replicaSet=rs_prod-DataScience-01&connectTimeoutMS=300&authSource=sinqTestDB",NULL);
   */
sicslogSetup("mongodb://sinqUserRW:8Mh7]YFUE6JP]dQ7@mgdbaas001:27017,mgdbaas002:27017,mgdbaas003:27017/sinqProdDB01?replicaSet=rs_prod-DataScience-01&connectTimeoutMS=300&authSource=sinqProdDB01",NULL);
  status = sicslogQuery(argc,argv,ResultPrint,NULL);
  if(status != 0){
    error = sicslogGetError();
    fprintf(stdout,"%s\n", error);
  }
  return status;
}

