sampleangle 0.000000
sampleangle setAccess 2
delay 155.800003
delay setAccess 2
environment Void
environment setAccess 2
fermidist 3000.000000
fermidist setAccess 1
flightpathlength 3000.000000
flightpathlength setAccess 1
flightpath Standard
flightpath setAccess 1
bestatus 0
bestatus setAccess 2
# Motor a2
a2 sign 1.000000
a2 SoftZero 0.000000
a2 SoftLowerLim -184.000000
a2 SoftUpperLim 356.000000
a2 Fixed -1.000000
a2 InterruptMode 0.000000
a2 AccessCode 2.000000
# Motor a1
a1 sign 1.000000
a1 SoftZero 0.000000
a1 SoftLowerLim 17.500000
a1 SoftUpperLim 70.000000
a1 Fixed -1.000000
a1 InterruptMode 0.000000
a1 AccessCode 2.000000
batchroot /home/FOCUS/batch
batchroot setAccess 2
email fanni.juranyi@psi.ch
email setAccess 2
fax 2939
fax setAccess 2
phone 3176
phone setAccess 2
adress LNS
adress setAccess 2
comment3 UNKNOWN
comment3 setAccess 2
comment2 UNKNOWN
comment2 setAccess 2
comment1 UNKNOWN
comment1 setAccess 2
sample Zr95Al5 (42)
sample setAccess 2
user F. Juranyi
user setAccess 2
title I/02 B-9
title setAccess 2
lastscancommand cscan mth 33.94 0.05 25 10000
lastscancommand setAccess 2
hm CountMode monitor
hm preset 1000000.000000
hm genbin 9000.000000 50.000000 648
hm init
# Counter counter
counter SetPreset 1000000.000000
counter SetMode Monitor
# Motor mex
mex sign 1.000000
mex SoftZero 0.000000
mex SoftLowerLim -0.500000
mex SoftUpperLim 180.399994
mex Fixed -1.000000
mex InterruptMode 0.000000
mex AccessCode 2.000000
# Motor m2cv
m2cv sign 1.000000
m2cv SoftZero 0.000000
m2cv SoftLowerLim -0.700000
m2cv SoftUpperLim 14.300000
m2cv Fixed -1.000000
m2cv InterruptMode 0.000000
m2cv AccessCode 2.000000
# Motor m2ch
m2ch sign 1.000000
m2ch SoftZero 0.000000
m2ch SoftLowerLim -0.070000
m2ch SoftUpperLim 6.800000
m2ch Fixed -1.000000
m2ch InterruptMode 0.000000
m2ch AccessCode 2.000000
# Motor m1cv
m1cv sign 1.000000
m1cv SoftZero 0.000000
m1cv SoftLowerLim -5.000000
m1cv SoftUpperLim 10.800000
m1cv Fixed -1.000000
m1cv InterruptMode 0.000000
m1cv AccessCode 2.000000
# Motor m1ch
m1ch sign 1.000000
m1ch SoftZero 0.000000
m1ch SoftLowerLim -1.100000
m1ch SoftUpperLim 6.800000
m1ch Fixed -1.000000
m1ch InterruptMode 0.000000
m1ch AccessCode 2.000000
# Motor mgo
mgo sign 1.000000
mgo SoftZero 0.000000
mgo SoftLowerLim -0.600000
mgo SoftUpperLim 0.600000
mgo Fixed -1.000000
mgo InterruptMode 0.000000
mgo AccessCode 2.000000
# Motor mty
mty sign 1.000000
mty SoftZero 0.000000
mty SoftLowerLim -12.000000
mty SoftUpperLim 12.000000
mty Fixed -1.000000
mty InterruptMode 0.000000
mty AccessCode 2.000000
# Motor mtx
mtx sign 1.000000
mtx SoftZero 0.000000
mtx SoftLowerLim -12.000000
mtx SoftUpperLim 14.000000
mtx Fixed -1.000000
mtx InterruptMode 0.000000
mtx AccessCode 2.000000
# Motor mth
mth sign 1.000000
mth SoftZero 0.000000
mth SoftLowerLim 17.500000
mth SoftUpperLim 70.000000
mth Fixed -1.000000
mth InterruptMode 0.000000
mth AccessCode 2.000000
# Motor msl
msl sign 1.000000
msl SoftZero 0.000000
msl SoftLowerLim 0.000000
msl SoftUpperLim 218.000000
msl Fixed -1.000000
msl InterruptMode 0.000000
msl AccessCode 2.000000
# Motor mtt
mtt sign 1.000000
mtt SoftZero 0.000000
mtt SoftLowerLim -184.000000
mtt SoftUpperLim 356.000000
mtt Fixed -1.000000
mtt InterruptMode 0.000000
mtt AccessCode 2.000000
