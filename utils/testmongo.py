#!/usr/bin/python

from sics.sics.sics import Sics
import paramiko

dblist = ['amor', 'boa', 'dmc', 'eiger', 'focus', 'hrpt', 'morpheus', 
       'narziss', 'orion', 'poldi', 'rita2', 'sans', 'sans2', 'tasp', 'zebra'] 

for inst in dblist:
    try:
        con = Sics(inst,2911)
        con.connect()
        con.login('Spy','007')
        result = con.transact('mongoconfig status')
        if result.find('disconnected') >= 0:
            print('%s lost connection to MongoDB server, reconnecting...' %(inst))
            con.transact('mongoconfig reopen')
            command = 'find log -name "*.log" | xargs grep "mongo server"\''
            cl = paramiko.SSHClient()
            cl.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            pw = '%slns' % (inst)
            pw = pw.upper()
            cl.connect(inst,username=inst,password=pw)
            stdin, stdout, stderr = cl.exec_command('find log -name "*.log"|xargs grep "mongo server"')
            reply = stdout.read()
            lines = reply.split('\n')
            print(lines[len(lines)-1])
            cl.close()
        else:
            print('%s mongo connection OK' % (inst))
        con.disconnect()
    except:
        print('Failed to connect to %s' %(inst))
