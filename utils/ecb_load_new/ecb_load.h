/*******************************************************************************
* This file contains the header defs for the ECB download program
*
*	File name: ecb_load.h
*
*	Author: P.S.			May 2004
*
*******************************************************************************/
/*******************************************************************************
/*******************************************************************************
*& 'ecbdriv.h' modifications:
*&
*&
*******************************************************************************/
/****************************************************************************************
* Declaration of data types.
****************************************************************************************/
typedef int Long;             /* 32 bits integer  */

/* GPIB interface */
/****************************************************************************************
* Constants.
****************************************************************************************/
#define TC_FAR
#define F_BUFSIZ       256               /* 256 characters per line */
#define NUL             ('\0')          /* Binary null character */
#define ACKN            ('\6')          /* Acknowledge character */
#define FOUND             1           /* Symbol found in Put/Get_variable */
#define NOT_FOUND         0           /* Symbol not found Put/Get_variable */
#define GLOBAL            2           /* Sym. used in more Put/Get_variable */

#ifndef TRUE
#define TRUE              1               /* Logical true */
#endif
#ifndef FALSE
#define FALSE             0               /* Logical false */
#endif

/*******************************************************************************
* ECB type definitions.
*******************************************************************************/

typedef struct                  /* Z80 registers transferred in function calls */
  {
    unsigned char d;            /* D register in Z80 */
    unsigned char e;            /* E register in Z80 */
    unsigned char b;            /* B register in Z80 */
    unsigned char c;            /* C register in Z80 */
  } Z80_reg;

typedef union           /* Used to swap bytes in 'address' and 'byte_count' */
  {
    unsigned short word;
    struct
      {
	unsigned char msb;      /* Most significant byte */
	unsigned char lsb;      /* Least significant byte */
      }b;
  }Swap;

typedef union          /* Used to read DMA data from the P2701a CPU module */
  {
    unsigned char *byte;     /* Four 8 bit bytes per 32 bit integer word */
    Long *word;
  }Ecb_data;


/*******************************************************************************
* ECB basic functions.
*******************************************************************************/
#define READ_N_BYTES	3	/* ECB function to read n bytes */
#define WRITE_N_BYTES	4	/* ECB function to write n bytes */
#define DMA_READ	5	/* ECB function to DMA read n bytes */

#define ECB_DOWN_LOAD 178	/* ECB function to download the ECB program  */
#define LOAD_NEXT	1	/* Return code from download function */
#define ECB_TYPE      157	/* Returns ECB ROM or RAM and version */

#define TYPE_OUT        2	/* Returns type from function 157 */
#define VERS_OUT        3	/* Returns version from function 157 */


/*******************************************************************************
* GPIB and ECB implementation environment.
*******************************************************************************/
#define GPIB_H          "/usr/local/include/ugpib.h"
#define GPIB0		"gpib0"	     /* Name of adater board No. 0 */
#define ECB1		"dev5"	     /* Name of ECB system #1 */
#define ECBTEXT         "ECB SYSTEM"
#define ECB2		"dev4"	     /* Name of ECB system #2 */
#define DEFAULT_TIMO	T3s	     /* Default timeout 3 sec. */
#define ECB_BYTES	65536L	 /* Max No. of bytes to transfer in each call */

/*******************************************************************************
* Error codes
*******************************************************************************/
#define TAS_ERROR               100     /* General error */
#define OPEN_FILE               600     /* Could not open file */
#define FILE_READ               601     /* Could not read file */
#define WRITE_FILE              602     /* Could not write to file */
#define GPIB_WRITE_ERROR        700
#define GPIB_READ_ERROR         701
#define GPIB_FIND_ERROR         702     /* Could not 'open' device */
#define GPIB_NO_LISNER_ERROR    703
#define ECB_ILLEGAL_FUNC        750     /* No such ECB function */
#define ECB_OVERFLOW            751     /* Byte count exceeds array size */

/*******************************************************************************
* Globals
*******************************************************************************/
extern int x_tas_error;	           /* Global error type */
extern char *x_error_text;	   /* Error text line*/
extern int x_error_int;		   /* Global error parameter */
extern int x_gpib0;                /* Unit descriptor for board (adaptor) 0 */
extern int x_ecb1;                 /* Unit descriptor of ECB system  #1 */
extern Z80_reg x_inreg;            /* Z80 registers b,c,d and e before call */
extern Z80_reg x_outreg;           /* Z80 registers b,c,d and e after call */
extern unsigned char x_ecb_data[]; /* Array to hold data blocks for ECB */
extern int x_ecb_error_count;      /* Counter for ECB call errors */
extern Ecb_data x_ecba;            /* DMA data from P2701a ECB-CPU */
extern int x_cpu_typ;              /* =0: WP-CPU,  =1: P2701 */


/*******************************************************************************
* Functions
*******************************************************************************/
extern void Gpib_init (void);
extern int Gpib_error(void);

extern int Ecb_func (int , unsigned char);               /* Call ECB function */
extern void Ecb_read (int, unsigned short, unsigned short);   /* Read n bytes */
extern void Ecb_write (int, unsigned short, unsigned short); /* Write n bytes */
extern int Ecb_dma_read (int, unsigned short, unsigned short);    /* DMA data */
extern void Ecb_memory_allocate (int size);
extern int Ecb_type (int);

extern void Ecb_init(void);
extern int Ecb_error(void);

extern void Ecb_load_error(void);    	/* Print error messages */
extern void Error_exit(void);
extern int Base_error(void);

