/*******************************************************************************
*       This file contains the interface for the gpib option
*       in the Risoe process control and data acquisition program TASCOM.
*
*
*       Project: TASCOM
*       File name: gpib_els.c
*
*       Author:         P.S.            May 2004
*
*
*
*******************************************************************************/
/*******************************************************************************
*&  modificaions:
*&
*&   V003.101   P.S.    26-APR-2004
*&                      For the ecb_load program only.
*&
*&
*&*****************************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "ecb_load.h"

#include GPIB_H

/*******************************************************************************
* Static variables
*******************************************************************************/

/*******************************************************************************
* Extern variables defined in this file.
*******************************************************************************/
int x_gpib0;                    /* Unit descriptor for board (adaptor) 0 */

/*******************************************************************************
* Prototypes for functions defined in this file.
*******************************************************************************/
extern void Gpib_init (void);
extern int Gpib_error(void);


/*******************************************************************************
* Set the GPIB delay and if look_for_board = LOOK_FOR_BOARD, see if we have a
* GPIB module in the computer.
*******************************************************************************/

extern void
Gpib_init (void)
{

  x_gpib0 = -1;

  /* See if the GPIB module is avaiable */
  x_gpib0 = ibfind (GPIB0);
  if (x_gpib0 < 0)
  {
    x_error_text = (char *) GPIB0;
    x_tas_error = GPIB_FIND_ERROR;
    Ecb_load_error ();
    return;
  }

  ibconfig (x_gpib0, IbcTIMING, 1);
  ibsic (x_gpib0);                                /* Send interface clear */
  usleep (300000);                                /* Wait 0.3 sec. */

  x_tas_error = FALSE;


  return;
}



/*******************************************************************************
* Gpib option error routine.
*******************************************************************************/

extern int
Gpib_error(void)
{

  switch (x_tas_error)
  {
    case GPIB_WRITE_ERROR:
      printf (" GPIB write error: %s", x_error_text);
      break;

    case GPIB_READ_ERROR:
      printf (" GPIB read error: %s", x_error_text);
      break;

    case GPIB_FIND_ERROR:
      printf (" No GPIB module in the computer ?");
      break;

    case GPIB_NO_LISNER_ERROR:
      printf (" No Listener.  Communication error to the %s.\n", x_error_text);
      break;

    default:
      return(NOT_FOUND);
  }

  printf ("\n");
  fflush (stdout);
  return(FOUND);

}



