/***************************************************************************
* Download program for the ECB system with the new P2701 CPU module.
*	
*       Filename: ecb_load.c
*
*	Author: P.S.			May 2004
*
*
****************************************************************************/
/*******************************************************************************
*&  modificaions:
*&
*&   V003.100   P.S.    2-May-2004
*&                      Creation.
*&
*&*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ecb_load.h"		/* The ecb driver interface. */

/*******************************************************************************
* Default path and name for the download file..
*******************************************************************************/
#define FILE_NAME       "/scratch/users/tascomp/sans2m/tas/symfil/ecb_v501.hex"

/*******************************************************************************
* External variables defined in this file.
*******************************************************************************/
int x_tas_error = 0;		  /* Global error type */
char *x_error_text = '\0';	  /* Error text line*/
int x_error_int = 0;		  /* Global error parameter */



/****************************************************************************
* If the program is started without arguments, the file ecb_v501.hex is
* downloaded. Another file name may be specified as an argument.
****************************************************************************/
extern int
main(int argc, char *argv[])
{
  int ecb_type;
  FILE *stream;
  char file_name[F_BUFSIZ] = FILE_NAME;

  if (argc == 2)
    strcpy (file_name, argv[1]);
  if (argc > 2)
    exit(0);   /* Error exit */

  stream = fopen(file_name, "r");	/* Open in read-only mode */
  if (stream == NULL)
  {
    printf("Error: unable to open file %s \n", file_name);
    exit(0);
  }

  Gpib_init();
  Ecb_init();

  /* Check communication with the ECB system works and that we are in ROM mode */
  ecb_type = Ecb_type (TYPE_OUT);
  if (ecb_type != 0)
  {
    if (ecb_type == -1)
      printf (" old ECB CPU (not P2701a)\n");
    else
    {
      printf ("\nERROR:- the ECB system is in RAM mode\n");
      printf (" and download of the ECB program is only possible in ROM mode.\n");
      printf (" You must set the ECB system in ROM mode.\n\n");
   }
    return;    
  }

  Ecb_down_load (stream);

}


/*******************************************************************************
* Main error routine.
*******************************************************************************/


extern void
Ecb_load_error()
{

  printf("\nERROR:-");

  if ((Ecb_error()) == FOUND) return;
  if ((Gpib_error()) == FOUND) return;


  exit(0);

}

