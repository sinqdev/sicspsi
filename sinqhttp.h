/*
  This is a histogram mmeory driver for the 2005-6 version of the
  histogram mmeory software based on RTAI-Linux and an embedded WWW-server
  for communications. For all http work the ghttp library from the gnome 
  project is used.
  
  This HM is meant to be used in conjunction with a counter module
  chained through the hmcontrol module. No need to handle counters here
  when hmcontrol can do the chaining.
  
  copyright: see file COPYRIGHT
  
  Mark Koennecke, January 2005
  
----------------------------------------------------------------------*/
#ifndef SINQHTTP_H_
#define SINQHTTP_H_
#include <HistDriv.i>

pHistDriver CreateSinqHttpDriver(pStringDict pOption);

#endif                          /*SINQHTTP_H_ */
