
/*------------------------------------------------------------------------
                          A M O R S T A T U S

  Public definitions for the AMOR status display 
  facilitator object. DO NOT CHANGE. This file is automatically
  created from amorstat.w.

  Mark Koennecke, September 1999
---------------------------------------------------------------------*/
#ifndef AMORSTATUS
#define AMORSTATUS

int AmorStatusFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);
int AmorStatusAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);
void KillAmorStatus(void *pData);

#endif
