/**
 * This is a couple of functions to do rebinning. This is neeeded when 
 * you have a number of data points with random coordinates and you wish to 
 * get this back onto a regular grid for better handling. 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, May 2007
 */
#ifndef REBIN_H_
#define REBIN_H_
#include <nxdataset.h>

#define TRILINEAR 1
#define ROUND     2
/**
 * rebin a point
 * @param target The target dataset to rebin too
 * @param x The x coordiante of the point
 * @param y The y coordinate of the point
 * @param value The value of the point
 * @param method The method to use for interpolation
 * @return 1 on success, 0 else
 */
int rebinPoint2D(pNXDS target, double x, double y, double value,
                 int method);

#endif                          /*REBIN_H_ */
