/*------------------------------------------------------------------------
  This is a little helper module for SICS which implements some
  status display support functions.

  COPYRIGHT: see file COPYRIGHT
 
  Extracted from nextrics 

  Mark Koennecke, August 2004
  --------------------------------------------------------------------------*/
#ifndef TRICSSUPPORT
#define TRICSSUPPORT

#include <sics.h>

int MakeTricsSupport(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);
int TricsSupportAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                       int argc, char *argv[]);


#endif
