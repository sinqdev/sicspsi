/**
 * This is a protocol handler for the pfeiffer vacuum measurement device 
 * as used at FOCUS. It does not add very much, just the right terminators 
 * and the special feature to send a <ENQ> == ASCII 5 to the device.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, March 2009
 */
#include <ascon.h>
#include <ascon.i>

static int PfeifferHandler(Ascon *a)
{
	char c = (char)5;
	
	switch(a->state){
	case AsconWriteStart:
		if(strstr(GetCharArray(a->wrBuffer),"@ENQ@") != NULL) {
			AsconWriteChars(a->fd,&c,1);
			a->state = AsconWriteDone;
			return 1;
		} else {
			return AsconStdHandler(a);
		}
		break;
	default:
		return AsconStdHandler(a);
	}
}
/*=--------------------------------------------------------------------------*/
static int PfeifferInit(Ascon *a, SConnection *con, int argc, char *argv[])
{
  a->hostport = strdup(argv[1]);
  a->sendTerminator = strdup("\n");
  a->timeout = 10;
  a->replyTerminator = strdup("\n");
  return 1;
}

/*-------------------------------------------------------------------------*/
void AddPfeifferProtocoll()
{
  AsconProtocol *prot = NULL;

  prot = calloc(sizeof(AsconProtocol), 1);
  prot->name = strdup("pfeiffer");
  prot->init = PfeifferInit;
  prot->handler = PfeifferHandler;
  AsconInsertProtocol(prot);
}
