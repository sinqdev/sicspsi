/*--------------------------------------------------------------------------
 * A module which reads the broadcast messages from the PSI Accelerator group
 * and allows to use the information in them in SICS. This facility runs
 * as a SICS task which tries to read incoming messages and stores the 
 * messages of interest in an internal data structure. Some code is provided
 * to make the content of these messages available within SICS.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, July 2005
 */
#ifndef _SINQ_H_
#define _SINQ_H_
#include <sics.h>
/*-------------------------------------------------------------------------*/
#define MAXLOG 300
/*-------------------------------------------------------------------------*/
typedef struct {
  pObjectDescriptor pDes;
  char d110[2024];
  char a110[2024];
  int receiveSocket;
  int lastSinq[MAXLOG];
  int lastCount;
  int anet;
} Sinq, *pSinq;
/*----------------------- interpreter interface  ------------------------*/
int SinqFactory(SConnection * pCon, SicsInterp * pSics,
                void *pData, int argc, char *argv[]);
int SinqRedirectFactory(SConnection * pCon, SicsInterp * pSics,
                void *pData, int argc, char *argv[]);
int SinqWrapper(SConnection * pCon, SicsInterp * pSics,
                void *pData, int argc, char *argv[]);
/*-------------------------------------------------------------------------*/
#define SINQBEAM 1
#define RINGBEAM 2
/**
 * get the SINQ beam intensity.
 * @param self A pointer to the SINQ data structure
 * @param code either SINQBEAM or RINGBEAM
 * @return The last read intensity of the appropriate beam or -900 on error
 */
int getSinqBeam(pSinq self, int code);

#endif                          //_SINQ_H_
