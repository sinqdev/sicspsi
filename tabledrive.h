
 /*---------------------------------------------------------------------------
  SICS object for driving a couple of motors along a tabulated given path.

  copyright: see file COPYRIGHT

  Mark Koennecke, July 2005
---------------------------------------------------------------------------*/
#ifndef SICSTABLEDRIVE
#define SICSTABLEDRIVE
#include <sics.h>
#include <motor.h>
/*-------------------------------------------------------------------------*/

typedef struct {
  double lower, position, upper;
  int tablePos;
} tdEntry, *ptdEntry;

/*-------------------------------------------------------------------------*/

typedef struct {
  char motorName[132];
  int table;
  pMotor pMot;
} tdMotor, *ptdMotor;

/*-------------------------------------------------------------------------*/

typedef struct {
  pObjectDescriptor pDes;
  pIDrivable pDriv;
  int motorTable;
  int tableLength;
  float targetPosition;
  float currentPosition;
  int state;
  char orientMotor[80];
  int debug;
  tdMotor oriMotor;
  int oriInvalid;
  long groupID;
} TableDrive, *pTableDrive;

/*-------------------------------------------------------------------------*/

int TableDriveFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);
int TableDriveAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);



#endif
