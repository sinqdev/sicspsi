\subsection{Triple Axis Spectrometer Software}
A triple axis spectrometer performs complicated scans in Q--energy space.  
The triple axis spectrometers pose a couple of challenges to the SICS
software:  \begin{itemize}
\item The command syntax should be as close as possible to the syntax
of the ancient tasmad program.
\item A relatively complicated algorithm is used for computing the
motor positions and magnet settings. It is a selling point when the
F77 routine used for this purpose in tasmad can be reused.
\item A ILL format scan file must be written after each scan in order
to provide compatability with existing data analysis software.  
\item Provisions must be made to  execute a batch file at each scan
point in order to allow for polarisation analysis. 
\item The user interface of tasmad is largely based on parameters
which are used to store and manipulate state information. 
\item There exists a special syntax for assigning variables which
requires the parameters to be in a special storage order. 
\end{itemize}

These requirements are implemented in the following way: tasmad
variables are matched to SICS variables. The scan and drive commands
are reimplemented in order to allow for both syntax parsing and the
reuse of the triple axis calculation routines. In order to support the
strange storage order assignment system the set command is also
implemented in this modules as it can share code with scan and drive. 
All the remaining syntax adaptions will be implemented in the
scripting language.   


\subsubsection{Data Structures}
It would be inconvenient to search all necessary parameters in the
interpreter anytime a scan or drive command is used. In order to avoid
this, the parameters are cached in a local data structure which is
initialized when installing the tas module. 

\begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap1}
$\langle$tasdata {\footnotesize ?}$\rangle\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@typedef struct {@\\
\mbox{}\verb@        pObjectDescriptor pDes;@\\
\mbox{}\verb@        pSicsVariable tasPar[MAXPAR];@\\
\mbox{}\verb@        pCounter counter;@\\
\mbox{}\verb@        pScanData pScan;@\\
\mbox{}\verb@        int iPOL; @\\
\mbox{}\verb@        int iIgnore; /* in order to ignore writing scan points again @\\
\mbox{}\verb@                        in polarisation mode;@\\
\mbox{}\verb@                    */@\\
\mbox{}\verb@        int addOutput[MAXADD];@\\
\mbox{}\verb@        int addType[MAXADD];@\\
\mbox{}\verb@        int addCount;          @\\
\mbox{}\verb@        int iFileNO;@\\
\mbox{}\verb@        int iFast;@\\
\mbox{}\verb@        char scanVar[80];@\\
\mbox{}\verb@        float oldSRO;@\\
\mbox{}\verb@   }TASdata, *pTASdata;@\\
\mbox{}\verb@@$\diamond$
\end{list}
\vspace{-1ex}
\footnotesize\addtolength{\baselineskip}{-1ex}
\begin{list}{}{\setlength{\itemsep}{-\parsep}\setlength{\itemindent}{-\leftmargin}}
\item Macro referenced in scrap ?.
\end{list}
\end{minipage}\\[4ex]
\end{flushleft}
\begin{description}
\item[pDes] The standard object descriptor required for any SICs
object. 
\item[floatPar] An array of pointers to float parameters. 
The parameters are indexed through defined constants.
\item[pScan] A pointer to a SICS scan object.
\item[iPOL] a flag which is non zer when polarisation analysis is
required.
\item[addOutput] TAS scans may have additional output fields besides
the scan variables in the scan data. This array holds the indices of
such variables.
\item[addType] This array holds the type of additional output
variables. This can be 0 for simple variables or 1 for a motor.
\item[addCount] is the number of additional output variables.
\item[iFileNO] is the number of the current data file.  
\item[iFast] is a flag denoting a fast scan. In a fast scan there is
no wait for the motors  to finish driving.
\item[scanVar] memorizes the first scan variable with a step of greater
 zero.
\item[oldSRO] keeps the old value of the motor SRO. This because the
helmholtz angle  stored in the variable helm has to be corrected when
SRO has been driven. This value is initialised in tasdrive and
TasScanDrive. The actual correction is done in TASUpdate.
\end{description}
The constants for the parameters are defined in the header file. 

\subsubsection{Exported Functions}
These are mainly the interpreter interface functions:
\begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap2}
$\langle$tasfunc {\footnotesize ?}$\rangle\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@  int TASFactory(SConnection *pCon, SicsInterp *pSics, void *pData,@\\
\mbox{}\verb@                 int argc, char *argv[]);@\\
\mbox{}\verb@  int TASDrive(SConnection *pCon, SicsInterp *pSics, void *pData,@\\
\mbox{}\verb@                 int argc, char *argv[]);@\\
\mbox{}\verb@  int TASScan(SConnection *pCon, SicsInterp *pSics, void *pData,@\\
\mbox{}\verb@                 int argc, char *argv[]);@\\
\mbox{}\verb@  int TASSet(SConnection *pCon, SicsInterp *pSics, void *pData,@\\
\mbox{}\verb@                 int argc, char *argv[]);@\\
\mbox{}\verb@@$\diamond$
\end{list}
\vspace{-1ex}
\footnotesize\addtolength{\baselineskip}{-1ex}
\begin{list}{}{\setlength{\itemsep}{-\parsep}\setlength{\itemindent}{-\leftmargin}}
\item Macro referenced in scrap ?.
\end{list}
\end{minipage}\\[4ex]
\end{flushleft}
\begin{flushleft} \small
\begin{minipage}{\linewidth} \label{scrap3}
\verb@"tas.h"@ {\footnotesize ? }$\equiv$
\vspace{-1ex}
\begin{list}{}{} \item
\mbox{}\verb@@\\
\mbox{}\verb@/*----------------------------------------------------------------------@\\
\mbox{}\verb@   Header file for the SICS triple axis module. Implements new drive@\\
\mbox{}\verb@   and scan commands for triple axis which use the old tasmad F77 @\\
\mbox{}\verb@   routine for calculating the spectrometer setting angles. @\\
\mbox{}\verb@@\\
\mbox{}\verb@   Mark Koennecke, November 2000@\\
\mbox{}\verb@------------------------------------------------------------------------*/  @\\
\mbox{}\verb@#ifndef SICSTAS@\\
\mbox{}\verb@#define SICSTAS@\\
\mbox{}\verb@#include <sicsvar.h>@\\
\mbox{}\verb@@\\
\mbox{}\verb@/*------------------------- parameter defines -------------------------*/@\\
\mbox{}\verb@#define WAV   0@\\
\mbox{}\verb@#define DM    1@\\
\mbox{}\verb@#define DA    2@\\
\mbox{}\verb@#define ETAM  3@\\
\mbox{}\verb@#define ETAA  4@\\
\mbox{}\verb@#define TI    5@\\
\mbox{}\verb@#define MN    6@\\
\mbox{}\verb@#define IF1V  7@\\
\mbox{}\verb@#define IF2V  8@\\
\mbox{}\verb@#define IF1H  9@\\
\mbox{}\verb@#define IF2H 10@\\
\mbox{}\verb@#define HELM 11@\\
\mbox{}\verb@#define AS   12@\\
\mbox{}\verb@#define BS   13@\\
\mbox{}\verb@#define CS   14@\\
\mbox{}\verb@#define AA   15@\\
\mbox{}\verb@#define BB   16@\\
\mbox{}\verb@#define CC   17@\\
\mbox{}\verb@#define ETAS 18@\\
\mbox{}\verb@#define AX   19@\\
\mbox{}\verb@#define AY   20@\\
\mbox{}\verb@#define AZ   21@\\
\mbox{}\verb@#define BX   22@\\
\mbox{}\verb@#define BY   23@\\
\mbox{}\verb@#define BZ   24@\\
\mbox{}\verb@#define EI   25@\\
\mbox{}\verb@#define KI   26@\\
\mbox{}\verb@#define EF   27@\\
\mbox{}\verb@#define KF   28@\\
\mbox{}\verb@#define QH   29@\\
\mbox{}\verb@#define QK   30@\\
\mbox{}\verb@#define QL   31 @\\
\mbox{}\verb@#define EN   32 @\\
\mbox{}\verb@#define QM   33@\\
\mbox{}\verb@@\\
\mbox{}\verb@#define HX   34@\\
\mbox{}\verb@#define HY   35@\\
\mbox{}\verb@#define HZ   36@\\
\mbox{}\verb@@\\
\mbox{}\verb@#define DA1  37 @\\
\mbox{}\verb@#define DA2  38@\\
\mbox{}\verb@#define DA3  39@\\
\mbox{}\verb@#define DA4  40@\\
\mbox{}\verb@#define DA5  41@\\
\mbox{}\verb@#define DA6  42@\\
\mbox{}\verb@#define DMCV 43@\\
\mbox{}\verb@#define DSRO 44@\\
\mbox{}\verb@#define DACH 45@\\
\mbox{}\verb@#define DMTL 46@\\
\mbox{}\verb@#define DMTU 47@\\
\mbox{}\verb@#define DSTL 48@\\
\mbox{}\verb@#define DSTU 49@\\
\mbox{}\verb@#define DATL 50@\\
\mbox{}\verb@#define DATU 51@\\
\mbox{}\verb@#define DMGL 52@\\
\mbox{}\verb@#define DSGL 53@\\
\mbox{}\verb@#define DSGU 54@\\
\mbox{}\verb@#define DAGL 55@\\
\mbox{}\verb@#define DEI  56@\\
\mbox{}\verb@#define DKI  57@\\
\mbox{}\verb@#define DEF  58@\\
\mbox{}\verb@#define DKF  59@\\
\mbox{}\verb@#define DQH  60@\\
\mbox{}\verb@#define DQK  62@\\
\mbox{}\verb@#define DQL  62@\\
\mbox{}\verb@#define DEN  63@\\
\mbox{}\verb@#define DQM  64@\\
\mbox{}\verb@#define DT   65@\\
\mbox{}\verb@#define SM   66@\\
\mbox{}\verb@#define SS   67@\\
\mbox{}\verb@#define SA   68@\\
\mbox{}\verb@#define FX   69@\\
\mbox{}\verb@#define NP   70@\\
\mbox{}\verb@#define F1   71@\\
\mbox{}\verb@#define F2   72@\\
\mbox{}\verb@#define LPA  73@\\
\mbox{}\verb@@\\
\mbox{}\verb@#define MRX1 74@\\
\mbox{}\verb@#define MRX2 75@\\
\mbox{}\verb@#define ARX1 76@\\
\mbox{}\verb@#define ARX2 77@\\
\mbox{}\verb@@\\
\mbox{}\verb@#define INST 78@\\
\mbox{}\verb@#define TIT  79@\\
\mbox{}\verb@#define USR  80@\\
\mbox{}\verb@#define COM  81@\\
\mbox{}\verb@#define ALF1 82@\\
\mbox{}\verb@#define ALF2 83@\\
\mbox{}\verb@#define ALF3 84@\\
\mbox{}\verb@#define ALF4 85@\\
\mbox{}\verb@#define BET1 86@\\
\mbox{}\verb@#define BET2 87@\\
\mbox{}\verb@#define BET3 88@\\
\mbox{}\verb@#define BET4 89@\\
\mbox{}\verb@#define OUT  90@\\
\mbox{}\verb@#define LOC  91@\\
\mbox{}\verb@#define SWUNIT 92@\\
\mbox{}\verb@#define SINFO  93@\\
\mbox{}\verb@#define TEI   94@\\
\mbox{}\verb@#define TKI   95@\\
\mbox{}\verb@#define TEF   96@\\
\mbox{}\verb@#define TKF   97@\\
\mbox{}\verb@#define TQH   98@\\
\mbox{}\verb@#define TQK   99@\\
\mbox{}\verb@#define TQL  100 @\\
\mbox{}\verb@#define TEN  101 @\\
\mbox{}\verb@#define TQM  102@\\
\mbox{}\verb@#define THX  103@\\
\mbox{}\verb@#define THY  104@\\
\mbox{}\verb@#define THZ  105@\\
\mbox{}\verb@@\\
\mbox{}\verb@#define TI1  106@\\
\mbox{}\verb@#define TI2  107@\\
\mbox{}\verb@#define TI3  108@\\
\mbox{}\verb@#define TI4  109@\\
\mbox{}\verb@#define TI5  110@\\
\mbox{}\verb@#define TI6  111@\\
\mbox{}\verb@#define TI7  112@\\
\mbox{}\verb@#define TI8  113@\\
\mbox{}\verb@#define DI1  114@\\
\mbox{}\verb@#define DI2  115@\\
\mbox{}\verb@#define DI3  116@\\
\mbox{}\verb@#define DI4  117@\\
\mbox{}\verb@#define DI5  118@\\
\mbox{}\verb@#define DI6  119@\\
\mbox{}\verb@#define DI7  120@\\
\mbox{}\verb@#define DI8  121@\\
\mbox{}\verb@#define DHX  122@\\
\mbox{}\verb@#define DHY  123@\\
\mbox{}\verb@#define DHZ  124@\\
\mbox{}\verb@#define HCONV1 125@\\
\mbox{}\verb@#define HCONV2 126@\\
\mbox{}\verb@#define HCONV3 127@\\
\mbox{}\verb@#define HCONV4 128@\\
\mbox{}\verb@#define POLFIL 129@\\
\mbox{}\verb@@\\
\mbox{}\verb@#define MAXPAR 130@\\
\mbox{}\verb@#define MAXADD 20@\\
\mbox{}\verb@#define MAXEVAR 12@\\
\mbox{}\verb@@\\
\mbox{}\verb@#define SROMOT 7  /* number of the SRO motor */ @\\
\mbox{}\verb@#define A4MOT  3  /* index of a4 motor */@\\
\mbox{}\verb@/* --------------------- data structure -------------------------------*/@\\
\mbox{}\verb@@$\langle$tasdata {\footnotesize ?}$\rangle$\verb@@\\
\mbox{}\verb@@\\
\mbox{}\verb@/*---------------------- interface ----------------------------------*/@\\
\mbox{}\verb@@$\langle$tasfunc {\footnotesize ?}$\rangle$\verb@@\\
\mbox{}\verb@#endif@\\
\mbox{}\verb@@$\diamond$
\end{list}
\vspace{-2ex}
\end{minipage}\\[4ex]
\end{flushleft}
