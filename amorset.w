\subsection{AMOR Reflectometer Settings}
This is yet another module for calculating the settings for the AMOR
 spectrometer. This version is of 2005 and implements the new calculation 
 scheme as devised by the Jochen where the base line and height zero point 
 is the beam height at the chop, chop, chopper. 
 
 Again a lot of parameters have to be maintained. For each optical bench 
 component an active flag, a fixed offset for correcting the scale reading to
 the actual position and an offset regarding the zero point of the scale need
  to be kept. And of course the value as read. This is offloaded into a
  separate module, amorcomp. Beamline components to be controlled are:
 \begin{description}
 \item[DS] The slit at the monochromator bunker
 \item[M] The monochromator
 \item[Dx] various slits
 \item[A] the analyzer
 \item[D] the detector.
 \end{description} 
  
  A master module encloses all those beamline components and performs the 
  actual calculation. The calculation is controlled through three virtual 
  motors: m2t (monochromator 2 theta), s2t (sample 2 theta) and ath, the
  analyzer angle.  For the formula used for the exact calculation, see the 
  paper from the Jochen.
  
  
 The amorcomp module provides the following interface:
 @d amorcompint @{
 typedef struct {
 	int activeFlag;  /* component present */
 	double markOffset; /* offset mark to real */
 	double scaleOffset; /* offset of the scale */
 	double readPosition; /* the position as read */
} amorComp, *pamorComp;
/*----------------------------------------------------------------------*/
double calcCompPosition(pamorComp comp); 		
int handleCompCommand(pamorComp comp, SConnection *pCon, 
	int argc, char *argv[]);
int saveAmorComp(FILE *fd, char *name, char *compname, pamorComp comp);   		
@}

The amorset module implements the container for all the data and the actual
 calculation. Most of the interesting stuff is in the functions relating to
  the interpreter interface and the drivable interface.  Again the virtual 
  motors only set values in the amorset data structure and amorset then takes 
  control off the driving operation. 
  
@d amorsetint @{
typedef struct {
	pObjectDescriptor pDes;
	pIDrivable pDriv;
	pIDrivable listDrive;
	amorComp chopper;
	amorComp M;
	amorComp DS;
	amorComp D1;
	amorComp D2;
	amorComp D3;
	amorComp EL;
	amorComp S;
	amorComp D4;
	amorComp A;
	amorComp D5;
	amorComp D;
	double targetm2t;
	double targets2t;
	double targetath;
	double actualm2t;
	double actuals2t;
	double actualath;
	int mustDrive;
	int mustRecalculate;
	int driveList;
	double dspar;
	double detectoroffset;
	int verbose;
}amorSet, *pamorSet;
/*--------------------------------------------------------------------*/
int AmorSetFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
					int argc, char *argv[]);
int AmorSetAction(SConnection *pCon, SicsInterp *pSics, void *pData,
					int argc, char *argv[]);
/*============ helper functions for the virtual motors ===============*/					
void amorSetMotor(pamorSet amor, int type, double value);
double amorGetMotor(pamorSet amor, SConnection *pCon, int type);
@}


The virtual motors just implement the bare minimum:
@d amordriveint @{
typedef struct{
	pObjectDescriptor pDes;
	pIDrivable pDriv;
	pamorSet mama;
	int type;
} amorDrive, *pamorDrive;
/*-----------------------------------------------------------------*/
pamorDrive makeAmorDrive(pamorSet papa, int type);
void killAmorDrive(void *data);
int AmorDriveAction(SConnection *pCon, SicsInterp *pSics, void *pData,
					int argc, char *argv[]);
@}


@o amorset.h @{
/*-------------------------------------------------------------------
 AMORSET together with  amorcomp and amordrive implement the position
 control facility for the reflectometer AMOR. This uses the algorithm
 with the beam height as the baseline.
 
 copyright: see file COPYRIGHT
 
 Mark Koennecke, October 2005
--------------------------------------------------------------------*/
#ifndef AMORSET
#define AMORSET
#include "amorcomp.h"
@<amorsetint@>
#endif

@}

@o amorcomp.h @{
/*---------------------------------------------------------------------
 AMOR component handling module. For the new (2005) calculation of the
 positions using the beam height as zero.
 
 copyright: see file COPYRIGHT
 
 Mark Koennecke, October  2005
-----------------------------------------------------------------------*/
 #ifndef AMORCOMP
 #define AMORCOMP
 #include <stdio.h>
 #include <sics.h>
 @<amorcompint@>
 #endif
 
@}
 

 @o amordrive.h @{
 /*--------------------------------------------------------------------
  Part of the AMOR position calculation module.

  copyright: see file COPYRIGHT

  Mark Koennecke, October 2005
----------------------------------------------------------------------*/
#ifndef AMORDRIVE
#define AMORDRIVE
@<amordriveint@>
#endif

 @}
 