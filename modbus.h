/*---------------------------------------------------------------------------
modbus.h

Communication routines for Eurotherm ModBus instruments
  
Markus Zolliker, Aug 2005
----------------------------------------------------------------------------*/

#ifndef MODBUS_H
#define MODBUS_H

#include "ease.h"

#define MODBUS_BAD_CRC -3090

typedef enum { modBusFloat, modBusInt, modBusTime } ModBusType;

int ModBusHandler(void *eab);

void ModBusPutFloats(EaseBase * eab, int adr, int npar, float val[]);
/* put floats with contiguous adresses */

void ModBusRequestValues(EaseBase * eab, int adr, int npar);
/* request floats or time (not int) with contiguous adresses */

float ModBusGet(EaseBase * eab, int adr, int type);
/* get value from the ModBusRequestValues command (no int!) */

void ModBusRequestValue(EaseBase * eab, int adr, int type);
/* request one value of arbitrary type */

float ModBusGetValue(EaseBase * eab, int type);
/* get this value */

void ModBusPutValue(EaseBase * eab, int adr, int type, float val);
/* put one value of arbitrary type */

#endif
