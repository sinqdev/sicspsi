/*---------------------------------------------------------------------------
                      I T C 4
	
  This is the implementation for a ITC4 object derived from an more general
  environment controller.
  
  Mark Koennecke, August 1997
  
       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <tcl.h>
#include <fortify.h>
#include <sics.h>
#include <splitter.h>
#include <obpar.h>
#include <devexec.h>
#include <nserver.h>
#include <interrupt.h>
#include <emon.h>
#include <evcontroller.h>
#include <evcontroller.i>
#include "itc4.h"

/*---------------------------------------------------------------------------*/
int ITC4SetPar(pEVControl self, char *name, float fNew, SConnection * pCon)
{
  int iRet;

  /* check authorsisation */
  if (!SCMatchRights(pCon, usUser)) {
    SCWrite(pCon, "ERROR: you are not authorised to change this parameter",
            eError);
    return 0;
  }

  /* just catch those three names which we understand */
  if (strcmp(name, "sensor") == 0) {
    iRet = SetSensorITC4(self->pDriv, (int) fNew);
    if (!iRet) {
      SCWrite(pCon, "ERROR: value out of range", eError);
      return 0;
    }
    iRet = ConfigITC4(self->pDriv);
    if (iRet != 1) {
      SCWrite(pCon, "ERROR: ITC4 configuration failed! ", eError);
      SCWrite(pCon, "INFO: Probably comm problem, Retry!", eError);
      return 0;
    }
    SCSendOK(pCon);
    return 1;
  } else if (strcmp(name, "control") == 0) {
    iRet = SetControlITC4(self->pDriv, (int) fNew);
    if (!iRet) {
      SCWrite(pCon, "ERROR: value out of range", eError);
      return 0;
    }
    iRet = ConfigITC4(self->pDriv);
    if (iRet != 1) {
      SCWrite(pCon, "ERROR: ITC4 configuration failed! ", eError);
      SCWrite(pCon, "INFO: Probably comm problem, Retry!", eError);
      return 0;
    }
    SCSendOK(pCon);
    return 1;
  } else if (strcmp(name, "timeout") == 0) {
    iRet = SetTMOITC4(self->pDriv, (int) fNew);
    if (!iRet) {
      SCWrite(pCon, "ERROR: value out of range", eError);
      return 0;
    }
    iRet = ConfigITC4(self->pDriv);
    if (iRet != 1) {
      SCWrite(pCon, "ERROR: ITC4 configuration failed! ", eError);
      SCWrite(pCon, "INFO: Probably comm problem, Retry!", eError);
      return 0;
    }
    SCSendOK(pCon);
    return 1;
  } else if (strcmp(name, "divisor") == 0) {
    iRet = SetDivisorITC4(self->pDriv, fNew);
    if (!iRet) {
      SCWrite(pCon, "ERROR: value out of range", eError);
      return 0;
    }
    iRet = ConfigITC4(self->pDriv);
    if (iRet != 1) {
      SCWrite(pCon, "ERROR: ITC4 configuration failed! ", eError);
      SCWrite(pCon, "INFO: Probably comm problem, Retry!", eError);
      return 0;
    }
    SCSendOK(pCon);
    return 1;
  } else if (strcmp(name, "multiplicator") == 0) {
    iRet = SetMultITC4(self->pDriv, fNew);
    if (!iRet) {
      SCWrite(pCon, "ERROR: value out of range", eError);
      return 0;
    }
    iRet = ConfigITC4(self->pDriv);
    if (iRet != 1) {
      SCWrite(pCon, "ERROR: ITC4 configuration failed! ", eError);
      SCWrite(pCon, "INFO: Probably comm problem, Retry!", eError);
      return 0;
    }
    SCSendOK(pCon);
    return 1;
  } else
    return EVCSetPar(self, name, fNew, pCon);
}

/*--------------------------------------------------------------------------*/
int ITC4GetPar(pEVControl self, char *name, float *fNew)
{
  int iRet;
  float fDiv;

  /* just catch those two names which we understand */
  if (strcmp(name, "sensor") == 0) {
    iRet = GetSensorITC4(self->pDriv);
    *fNew = (float) iRet;
    return 1;
  } else if (strcmp(name, "control") == 0) {
    iRet = GetControlITC4(self->pDriv);
    *fNew = (float) iRet;
    return 1;
  } else if (strcmp(name, "timeout") == 0) {
    iRet = GetTMOITC4(self->pDriv);
    *fNew = (float) iRet;
    return 1;
  } else if (strcmp(name, "divisor") == 0) {
    fDiv = GetDivisorITC4(self->pDriv);
    *fNew = fDiv;
    return 1;
  } else if (strcmp(name, "multiplicator") == 0) {
    fDiv = GetMultITC4(self->pDriv);
    *fNew = fDiv;
    return 1;
  } else
    return EVCGetPar(self, name, fNew);
}

/*---------------------------------------------------------------------------*/
int ITCList(pEVControl self, SConnection * pCon)
{
  char pBueffel[132];
  int iRet;

  iRet = EVCList(self, pCon);
  sprintf(pBueffel, "%s.sensor = %d\n", self->pName,
          GetSensorITC4(self->pDriv));
  SCWrite(pCon, pBueffel, eValue);
  sprintf(pBueffel, "%s.control = %d\n", self->pName,
          GetControlITC4(self->pDriv));
  SCWrite(pCon, pBueffel, eValue);
  sprintf(pBueffel, "%s.timeout  = %d\n", self->pName,
          GetTMOITC4(self->pDriv));
  SCWrite(pCon, pBueffel, eValue);
  sprintf(pBueffel, "%s.divisor  = %f\n", self->pName,
          GetDivisorITC4(self->pDriv));
  SCWrite(pCon, pBueffel, eValue);
  sprintf(pBueffel, "%s.multiplicator  = %f\n", self->pName,
          GetMultITC4(self->pDriv));
  SCWrite(pCon, pBueffel, eValue);
  return iRet;
}

/*-------------------------------------------------------------------------*/
int ITC4Wrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[])
{
  pEVControl self = NULL;
  char pBueffel[256];
  int iRet;
  double fNum;
  float fVal;

  self = (pEVControl) pData;
  assert(self);
  assert(pCon);
  assert(pSics);

  if (argc < 2) {
    return EVControlWrapper(pCon, pSics, pData, argc, argv);
  }

  strtolower(argv[1]);
  if ((strcmp(argv[1], "sensor") == 0) || (strcmp(argv[1], "control") == 0)
      || (strcmp(argv[1], "timeout") == 0)
      || (strcmp(argv[1], "divisor") == 0)
      || (strcmp(argv[1], "multiplicator") == 0)) {
    if (argc > 2) {             /* set case */
      iRet = Tcl_GetDouble(pSics->pTcl, argv[2], &fNum);
      if (iRet != TCL_OK) {
        sprintf(pBueffel, "ERROR: expected number, got %s", argv[2]);
        SCWrite(pCon, pBueffel, eError);
        return 0;
      }
      return ITC4SetPar(self, argv[1], (float) fNum, pCon);
    } else {                    /* get case */

      iRet = ITC4GetPar(self, argv[1], &fVal);
      sprintf(pBueffel, "%s.%s = %f\n", self->pName, argv[1], fVal);
      SCWrite(pCon, pBueffel, eValue);
      return 1;
    }
  } else if (strcmp(argv[1], "list") == 0) {
    return ITCList(self, pCon);
  } else {
    return EVControlWrapper(pCon, pSics, pData, argc, argv);
  }
  /* not reached */
  return 0;
}
