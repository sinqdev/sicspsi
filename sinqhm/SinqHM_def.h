/*=================================================== [...SinqHM]SinqHM_def.h
**
**	Definition Include file for SinqHM_SRV and its clients.
**
**------------------------------------------------------------------------------
*/
#define	    SINQHM_DEF_ID	"V03C"

#ifdef __alpha
#ifndef __vms
#pragma	    pack  1
#endif
#endif
/*------------------------------------------------------------------------------
*/
#ifndef OffsetOf
#define OffsetOf(type, identifier)  ((size_t)(&((type*) NULL)->identifier))
#endif
/*------------------------------------------------------------------------------
**                      Define some defaults.
*/
#define PORT_BASE	2400	/* The Internet Port for Server Requests    */
#define	MAX_CLIENTS	8	/* The maximum number of active clients   */
#define MAX_TOF_CNTR	32768	/* The maximum number of individual counters ..
				** which can be handled in TOF mode */
#define MAX_PSD_CNTR    16777217 /* maximum number of PSD elements */
#define MAX_PSD_ED      3       /* maximum number of additional
                                   single detectors at AMOR
				*/
#define MAX_TOF_NBINS	32768	/* The maximum number of bins in a TOF histog */
#define MAX_TOF_EDGE	16	/* The maximum number of TOF edge arrays */
#define VMIO_BASE_ADDR	0x1900	/* VME address of a (possible) VMIO10 module */
#define	IDENT_MSGE_LEN	256	/* Length of Ident info for SQHM_IDENT */

#define	    uchar   unsigned char
#define	    usint   unsigned short int
#define	    uint    unsigned int
/*------------------------------------------------------------------------------
**                      Define some status values (similar to VAXeln).
*/
#define KER__SUCCESS	  1
#define KER__BAD_CREATE	 -2
#define KER__BAD_STATE	 -4
#define KER__BAD_VALUE	 -6
#define KER__EXIT_SIGNAL -10
#define KER__BAD_RECV	 -14
#define KER__BAD_ALLOC	 -16

#ifndef True
#define	True	1
#endif

#ifndef False
#define	False	0
#endif

#ifndef NIL
#define	NIL	'\0'
#endif
/*------------------------------------------------------------------------------
**                      Define command verbs to SinqHM.
*/
#define SQHM_CNCT	0x01
#define SQHM_CLOSE	0x02
#define SQHM_CONFIG	0x03
#define SQHM_DAQ	0x04
#define SQHM_DBG	0x05
#define SQHM_DECONFIG	0x06
#define SQHM_EXIT	0x07
#define SQHM_IDENT	0x0e
#define SQHM_PROJECT	0x0d
#define SQHM_READ	0x08
#define SQHM_SELECT	0x09
#define SQHM_STATUS	0x0a
#define SQHM_WRITE	0x0b
#define SQHM_ZERO	0x0c
	    /*
	    ** Define the various operation modes
	    */
#define SQHM__TRANS	0x1000	/* Transparent mode */
#define SQHM__HM_DIG	0x2000	/* Hist mode (with digitised read-out) */
#define SQHM__TOF	0x3000	/* Time-of-Flight mode */
#define SQHM__HM_PSD	0x4000	/* Hist mode (with Pos-sens-detect read-out) */
#define SQHM__HRPT	0x5000	/* Hist mode for HRPT */
	    /*
	    ** Define the various sub-mode bits of the operation modes
	    */
#define SQHM__SUB_MODE_MSK 0xff	/* Mask for extracting "sub-mode" bits */
#define SQHM__DEBUG	0x01	/* Debug flag - FILLER will suspend itself ..
				** .. after starting to allow debugging */
#define SQHM__UD	0x02	/* Use Up/Down bit information */

#define SQHM__BO_MSK	0x18	/* Mask for extracting "bin-overflow" bits */
#define SQHM__BO_IGN	0x00	/* Ignore bin-overflows (bin-contents wrap) */
#define SQHM__BO_SMAX	0x08	/* On bin-overflow, stop at maximum */
#define SQHM__BO_CNT	0x10	/* Keep counts of overflow bins */

#define SQHM__STROBO	0x20	/* Use strobo-bit information */
#define SQHM__REFLECT	0x40	/* Reflect histograms */
#define SQHM__NO_STAT	0x80	/* Suppress status info from "Filler" */
	    /*
	    ** ----------------------------------------------------------
	    **		    SQHM_DAQ sub-function codes
	    */
#define DAQ__EXIT	0xffffffff
#define DAQ__CLR	0x01
#define DAQ__GO		0x02
#define DAQ__INH	0x03
#define DAQ__STOP	0x04
#define DAQ__TST	0x05
	    /*
	    ** ----------------------------------------------------------
	    **		    SQHM_PROJECT sub-codes
	    */
#define PROJECT__ON_Y	0x0001	/* Project onto y-axis */
#define PROJECT__1_DIM	0x0002	/* Make projection of a 1-dim histogram */
#define PROJECT__COLL   0x0003  /* collapse PSD on one time channel */
#define PROJECT__SAMPLE 0x0004  /* sum a rectangular part of the PSD
                                   detector in time
				*/
#define PROJECT__FRAME  0x0005  /* select a frame  from a 3D histogram:
				   2 position dims plus time binning
				*/
	    /*
	    ** ----------------------------------------------------------
	    **		    Definition of bits in <flag> of TOF edge-array
	    */
#define FLAG__VAR_BIN	0x01	/* Bin span of histogram is variable */
	    /*
	    ** ----------------------------------------------------------
	    **		  Definition of bits in <flags> of SQHM_STATUS response
	    */
#define STATUS_FLAGS__PF	0x8000	/* PF  - Power Fail */
#define STATUS_FLAGS__SWC	0x4000	/* SWC - Status Word Changed */
#define STATUS_FLAGS__NRL	0x2000	/* NRL - Neutron Rate Low */
#define STATUS_FLAGS__DAQ	0x1000	/* DAQ on -- set if Hdr Mask Bits are
				    ** correct so that data acq is active */
#define STATUS_FLAGS__SYNC3	0x0800	/* Ext Synch Bit #3 */
#define STATUS_FLAGS__SYNC2	0x0400	/* Ext Synch Bit #2 */
#define STATUS_FLAGS__SYNC1	0x0200	/* Ext Synch Bit #1 */
#define STATUS_FLAGS__SYNC0	0x0100	/* Ext Synch Bit #0 */
#define STATUS_FLAGS__UD	0x0080	/* UD  - Up/Down */
#define STATUS_FLAGS__GU	0x0040	/* GU  - Gummi (i.e. Strobo) */
	    /*
	    ** ----------------------------------------------------------
	    */
#define N_HISTS_MAX	64	    /* Maximum number of histograms supported */
#define N_BINS_MAX	0x00ffff    /* Maximum histogram bin number permitted */
#define N_TOTAL_BYTES	0x400000    /* Maximum total bytes of histogram */
/*
**------------------------------------------------------------------------------
**				Definitions of Filler states in HRPT mode
*/
#define	HRPT__SRCH_FRAME    1
#define	HRPT__READ_FRAME    2
/*
**------------------------------------------------------------------------------
**				Definitions for the LWL Datagrams
*/
#define	LWL_HDR_TYPE_MASK   (0x1f000000)    /* Mask for extracting main dgrm ..
					    ** .. hdr command-type bits */
#define	LWL_HDR_PF_MASK	   (0x80000000)	/* Mask for extr Power Fail bit */
#define	LWL_HDR_SWC_MASK   (0x40000000)	/* Mask for extr Status Word Chng bit */
#define	LWL_HDR_NRL_MASK   (0x20000000)	/* Mask for extr Neutron Rate Low bit */
#define LWL_HDR_SYNC3_MASK (0x00800000)	/* Mask for one of ext synch bits */
#define LWL_HDR_SYNC2_MASK (0x00400000)	/* Mask for one of ext synch bits */
#define LWL_HDR_SYNC1_MASK (0x00200000)	/* Mask for one of ext synch bits */
#define LWL_HDR_SYNC0_MASK (0x00100000)	/* Mask for one of ext synch bits */
#define	LWL_HDR_UD_MASK	   LWL_HDR_SYNC1_MASK /* Mask for Up/Down bit */
#define	LWL_HDR_GU_MASK	   LWL_HDR_SYNC0_MASK /* Mask for GU bit */
#define LWL_HDR_BA_MASK	   (0x00f00000)	/* Mask for TSI Binning Addr */
#define LWL_HDR_TS_MASK	   (0x000fffff)	/* Mask for TSI Time Stamp */

#define	LWL_FIFO_EMPTY	(0x1e000000)	/* FIFO Empty */

#define	LWL_TSI_TR	(0x1f000000)  /* Time-Status-Info Transp-Mode */
#define	LWL_TSI_HM_NC	(0x1f000000)  /* Time-Status-Info Hist-Mode+No-Coinc */
#define	LWL_TSI_HM_C	(0x0e000000)  /* Time-Status-Info Hist-Mode+Coinc */
#define	LWL_TSI_TOF	(0x1f000000)  /* Time-Status-Info TOF-Mode */
#define	LWL_TSI_SM_NC   (0x1f000000)  /* Time-Status-Info Strobo-Mode+No-Coin */
#define	LWL_TSI_SM_C	(0x0e000000)  /* Time-Status-Info Strobo-Mode+Coinc */
#define LWL_TSI_DT_MSK	(0x000fffff)  /* Mask for Dead-Time in TSI */
#define LWL_TSI_DTS_MSK	(0x000fffff)  /* Mask for Delay-Time-to-Start in TSI */

#define	LWL_TR_C1	(0x00000001)	/* Transp. Mode Chan 1 */
#define	LWL_TR_C2	(0x00000002)	/* Transp. Mode Chan 2 */
#define	LWL_TR_C3	(0x00000003)	/* Transp. Mode Chan 3 */
#define	LWL_TR_C4	(0x00000004)	/* Transp. Mode Chan 4 */
#define	LWL_TR_C5	(0x00000005)	/* Transp. Mode Chan 5 */
#define	LWL_TR_C6	(0x00000006)	/* Transp. Mode Chan 6 */
#define	LWL_TR_C7	(0x00000007)	/* Transp. Mode Chan 7 */
#define	LWL_TR_C8	(0x00000008)	/* Transp. Mode Chan 8 */
#define	LWL_TR_C9	(0x00000009)	/* Transp. Mode Chan 9 */

#define	LWL_HM_NC	(0x10000000)	/* Hist-Mode/No-Coinc 0 chan dgrm hdr */
#define LWL_HM_NC_C1	(0x11000000)	/* Hist-Mode/No-Coinc 1 chan dgrm hdr */
#define LWL_HM_NC_C2	(0x12000000)	/* Hist-Mode/No-Coinc 2 chan dgrm hdr */
#define LWL_HM_NC_C3	(0x13000000)	/* Hist-Mode/No-Coinc 3 chan dgrm hdr */
#define LWL_HM_NC_C4	(0x14000000)	/* Hist-Mode/No-Coinc 4 chan dgrm hdr */
#define LWL_HM_NC_C5	(0x15000000)	/* Hist-Mode/No-Coinc 5 chan dgrm hdr */
#define LWL_HM_NC_C6	(0x16000000)	/* Hist-Mode/No-Coinc 6 chan dgrm hdr */
#define LWL_HM_NC_C7	(0x17000000)	/* Hist-Mode/No-Coinc 7 chan dgrm hdr */
#define LWL_HM_NC_C8	(0x18000000)	/* Hist-Mode/No-Coinc 8 chan dgrm hdr */
#define LWL_HM_NC_C9	(0x19000000)	/* Hist-Mode/No-Coinc 9 chan dgrm hdr */

#define	LWL_HM_CO	(0x10000000)	/* Hist-Mode+Coinc 0 chan dgrm hdr */
#define LWL_HM_CO_C2	(0x12000000)	/* Hist-Mode+Coinc 2 chan dgrm hdr */
#define LWL_HM_CO_C3	(0x13000000)	/* Hist-Mode+Coinc 3 chan dgrm hdr */
#define LWL_HM_CO_C4	(0x14000000)	/* Hist-Mode+Coinc 4 chan dgrm hdr */
#define LWL_HM_CO_C5	(0x15000000)	/* Hist-Mode+Coinc 5 chan dgrm hdr */
#define LWL_HM_CO_C6	(0x16000000)	/* Hist-Mode+Coinc 6 chan dgrm hdr */
#define LWL_HM_CO_C7	(0x17000000)	/* Hist-Mode+Coinc 7 chan dgrm hdr */
#define LWL_HM_CO_C8	(0x18000000)	/* Hist-Mode+Coinc 8 chan dgrm hdr */
#define LWL_HM_CO_C9	(0x19000000)	/* Hist-Mode+Coinc 9 chan dgrm hdr */

#define LWL_TOF_C1	(0x01000000)	/* TOF-Mode 1 chan dgrm hdr */
#define LWL_TOF_C2	(0x02000000)	/* TOF-Mode 2 chan dgrm hdr */
#define LWL_TOF_C3	(0x03000000)	/* TOF-Mode 3 chan dgrm hdr */
#define LWL_TOF_C4	(0x04000000)	/* TOF-Mode 4 chan dgrm hdr */
#define LWL_TOF_C5	(0x05000000)	/* TOF-Mode 5 chan dgrm hdr */
#define LWL_TOF_C6	(0x06000000)	/* TOF-Mode 6 chan dgrm hdr */
#define LWL_TOF_C7	(0x07000000)	/* TOF-Mode 7 chan dgrm hdr */
#define LWL_TOF_C8	(0x08000000)	/* TOF-Mode 8 chan dgrm hdr */
#define LWL_TOF_C9	(0x09000000)	/* TOF-Mode 9 chan dgrm hdr */

#define LWL_GUMMI_MON   (0x14000000)    /* GUMMI-Mode Monitor datagram */

#define LWL_PSD_TSI      0x0E000000     /* PSD-Mode TSI datagram */
#define LWL_PSD_DATA     0x12000000     /* PSD-mode data datagram */ 
#define LWL_PSD_ED       0x03000000     /* PSD-mode single detector datagram */
#define LWL_PSD_PWF      0x20000000     /* PSD-mode Power Fail bit */
#define LWL_PSD_TIME     0x000fffff     /* PSD-mode time stamp extraction
                                           mask */
#define LWL_PSD_FLASH_MASK 0x00ff       /* mask for flash count     */
#define LWL_PSD_XORF       0x2000       /* mask for TDC-XORF bit */
#define LWL_PSD_CONF       0x0100       /* mask for TDC-CONF flag */
#define LWL_HDR_ED_MASK  0x1C000000     /* mask for extracting ds4-ds2 */

#define	LWL_SM_NC	(0x10000000)  /* Strobo-Mode/No-Coinc 0 chan dgrm hdr */
#define LWL_SM_NC_C1	(0x11000000)  /* Strobo-Mode/No-Coinc 1 chan dgrm hdr */
#define LWL_SM_NC_C2	(0x12000000)  /* Strobo-Mode/No-Coinc 2 chan dgrm hdr */
#define LWL_SM_NC_C3	(0x13000000)  /* Strobo-Mode/No-Coinc 3 chan dgrm hdr */
#define LWL_SM_NC_C4	(0x14000000)  /* Strobo-Mode/No-Coinc 4 chan dgrm hdr */
#define LWL_SM_NC_C5	(0x15000000)  /* Strobo-Mode/No-Coinc 5 chan dgrm hdr */
#define LWL_SM_NC_C6	(0x16000000)  /* Strobo-Mode/No-Coinc 6 chan dgrm hdr */
#define LWL_SM_NC_C7	(0x17000000)  /* Strobo-Mode/No-Coinc 7 chan dgrm hdr */
#define LWL_SM_NC_C8	(0x18000000)  /* Strobo-Mode/No-Coinc 8 chan dgrm hdr */
#define LWL_SM_NC_C9	(0x19000000)  /* Strobo-Mode/No-Coinc 9 chan dgrm hdr */

#define	LWL_SM_CO	(0x10000000)  /* Strobo-Mode + Coinc 0 chan dgrm hdr */
#define LWL_SM_CO_C1	(0x11000000)  /* Strobo-Mode + Coinc 1 chan dgrm hdr */
#define LWL_SM_CO_C2	(0x12000000)  /* Strobo-Mode + Coinc 2 chan dgrm hdr */
#define LWL_SM_CO_C3	(0x13000000)  /* Strobo-Mode + Coinc 3 chan dgrm hdr */
#define LWL_SM_CO_C4	(0x14000000)  /* Strobo-Mode + Coinc 4 chan dgrm hdr */
#define LWL_SM_CO_C5	(0x15000000)  /* Strobo-Mode + Coinc 5 chan dgrm hdr */
#define LWL_SM_CO_C6	(0x16000000)  /* Strobo-Mode + Coinc 6 chan dgrm hdr */
#define LWL_SM_CO_C7	(0x17000000)  /* Strobo-Mode + Coinc 7 chan dgrm hdr */
#define LWL_SM_CO_C8	(0x18000000)  /* Strobo-Mode + Coinc 8 chan dgrm hdr */
#define LWL_SM_CO_C9	(0x19000000)  /* Strobo-Mode + Coinc 9 chan dgrm hdr */

#define LWL_TSI_MODE_MASK   (0x000e)	/* Mask for mode in Time Status Info */
#define LWL_TSI_MODE_TR     (0x0000)	/* TSI Transparent-Mode */
#define LWL_TSI_MODE_HM	    (0x0002)	/* TSI Hist-Mode */
#define LWL_TSI_MODE_TOF    (0x0004)	/* TSI TOF-Mode */
#define LWL_TSI_MODE_SM1    (0x0006)	/* TSI Strobo-Mode 1 - time-stamp coded */
#define LWL_TSI_MODE_TR_UD  (0x0008)	/* TSI Transparent-Mode Up-Down */
#define LWL_TSI_MODE_HM_UD  (0x000a)	/* TSI Hist-Mode Up-Down */
#define LWL_TSI_MODE_TOF_UD (0x000c)	/* TSI TOF-Mode Up-Down */
#define LWL_TSI_MODE_SM2    (0x000e)	/* TSI Strobo-Mode 2 - h/w coded */
/*
**------------------------------------------------------------------------------
**				Define structure of a TOF histogram data item.
*/
    struct tof_histog {
      int	cntr_nmbr;	/* Counter number */
      uint	lo_edge;	/* Low edge of first bin (20-bit value) */
      uint	hi_edge;	/* Top edge of last  bin (20-bit value) */
      usint	flag;		/* Bit mask giving info on histog -- may be
				** used to help optimise the code */
      usint	bytes_per_bin;	/* Number of bytes in each histogram bin */
      uint	n_bins;		/* Number of bins in histogram */
      uint	cnt_early_up;	/* Count of early events (pol'n up) */
      uint	cnt_late_up;	/* Count of late events (pol'n up) */
      uint	cnt_early_down;	/* Count of early events (pol'n down) */
      uint	cnt_late_down;	/* Count of late events (pol'n down) */
      uint     *bin_edge;	/* Pointer to array of bin edges */
      union {			/* Pointer to histogram array */
        uchar	  *b_bin_data;	/*  .. pointer if it's  8-bit bins */
        usint	  *w_bin_data;	/*  .. pointer if it's 16-bit bins */
        uint	  *l_bin_data;	/*  .. pointer if it's 32-bit bins */
      } u;
    };

/*		Define a TOF 'edge-info' structure. This structure is created
**		as a result of a TOF 'edge-array' in a SQHM__TOF config cmnd.
*/
    struct tof_edge_info {
      uint	n_bins;		/* Number of bins in histogram */
      uint	flag;		/* Flag bits defining type of histo */
      uint	bin_span;	/* Time spanned by a histogram bin (20-bit
				**  value) if bin width is constant. Otherwise
				**  it is zero. */
      uint	hi_edge;	/* Top edge of last  bin (20-bit value) */
      uint	edges[2];	/* Array of edge data (20-bit values). There
				**  are actually (n_bins+1) items in the array
				**  and give the bottom edges of the bin */
    };

/*		Define structure of a TOF 'edge-array' in SQHM__TOF config cmnd
*/
    struct tof_edge_arr {
      uint	n_bins;		/* Number of bins in histogram */
      uint	flag;		/* Flag (0/1) for fixed/variable bin size */
      uint	edges[2];	/* Array of bottom edges (20-bit values) */
    };

/*		Define structure of a TOF 'bank' in SQHM__TOF config command
*/
    struct tof_bank {
      usint	first;		/* Number of first counter in bank */
      usint	n_cntrs;	/* Number of counters in bank */
      usint	edge_indx;	/* Index of edge array */
      usint	bytes_per_bin;	/* Number of bytes per bin */
    };
/*
**------------------------------------------------------------------------------
**				Define command structure.
*/
    struct req_buff_struct {			/* For messages to SinqHM */
      uint		bigend;
      uint		cmnd;
      union {
        char		filler[56];

	struct {uint	max_pkt,
			strt_mode;}	cnct;

	struct {uint     mode;
	  union {
	    struct {
	      uint   n_buffs;
	      uint   n_bytes;
	    } trans;
	    struct {
	      uint   n_hists;
	      uint   lo_bin;
	      uint   num_bins;
	      uint   bytes_per_bin;
	      uint   compress;
	    } hm_dig;
	    struct {
	      uint	n_extra_bytes;
	      usint	n_edges;
	      usint	n_banks;
	      uint	preset_delay;
	      struct tof_edge_arr   edge_0;
	      struct tof_bank	    bank_0;
	    } tof;
	    struct {
	      uint	n_extra_bytes;
	      usint	n_edges;
	      usint	n_banks;
	      uint	preset_delay;
              usint       xFactor;
              usint       yFactor;
              usint       xOffset;
              usint       yOffset;
              usint       xSize;
              usint       ySize;
	      struct tof_edge_arr   edge_0;
	      struct tof_bank	    bank_0;
	    } psd;
	  } u;
	}				cnfg;

	struct {uint	mask;}		dbg;

	struct {uint	sub_code;}	decnfg;

	struct {uint	sub_cmnd;}	daq;

	struct {uint	sub_code, 
			x_lo,
			nx,
			y_lo,
			ny,
			xdim,
			nhist;}		project;

	struct {uint	hist_no,
			first_bin,
			n_bins;}	read;

	struct {uint	hist_no;}	select;

	struct {uint	hist_no,
			first_bin,
			n_bins,
			bytes_per_bin;}	write;

	struct {uint	hist_no,
			first_bin,
			n_bins;}	zero;
      } u;
    };
/*
**------------------------------------------------------------------------------
**				Define status response structure.
*/
    struct rply_buff_struct {	    /* For messages from SinqHM */
      uint		bigend;
      uint		status;
      uint		sub_status;
      union {
        char		    message[52];

	struct {uint	    port;
		uint	    pkt_size;
		uint	    hm_mode;
		uint	    n_hists;
		uint	    num_bins;
		uint	    bytes_per_bin;
		uint	    curr_hist;
		uint	    max_block;
		uint	    total_bytes;
		uint	    lo_cntr;
		uint	    lo_bin;
		uint	    compress;
		uint	    up_time;}		cnct;

	struct {usint   daq_now;
		usint   daq_was;
		usint   filler_mask;
		usint   server_mask;}	daq;

	struct {uint	n_extra_bytes;
		uint	up_time;
		usint	offset_vxWorks_ident;
		usint	offset_vxWorks_date;
		usint	offset_instr;
		usint	offset_def_ident;
		usint	offset_sinqhm_main_ident;
		usint	offset_sinqhm_main_date;
		usint	offset_sinqhm_server_ident;
		usint	offset_sinqhm_server_date;
		usint	offset_sinqhm_filler_ident;
		usint	offset_sinqhm_filler_date;
		usint	offset_sinqhm_routines_ident;
		usint	offset_sinqhm_routines_date;}	ident;

	struct {uint	    n_bins;
		uint	    bytes_per_bin;
		uint	    cnts_lo;
		uint	    cnts_hi;}		project;

	struct {uint	    first_bin;
		uint	    n_bins;
		uint	    bytes_per_bin;
		uint	    cnts_lo;
		uint	    cnts_hi;}		read;

	struct {uint	    cfg_state;
		usint	    n_hists, curr_hist;
		uint	    num_bins;
		uint	    max_n_hists;
		uint	    max_num_bins;
		uchar	    max_srvrs, act_srvrs, bytes_per_bin, compress;
		usint	    daq_now, filler_mask;
		uint	    max_block;
		usint	    tsi_status, flags;
		union {
		  uint	    dead_time;
		  uint	    dts;
		  uint	    both;
			}   dt_or_dts;
		uint	    num_bad_events;
		uint	    up_time;}		status;
      } u;
    };
/*
**------------------------------------------------------------------------------
**				Define structure of message to SinqHM-filler.
*/
    struct msg_to_filler_struct {	/* For messages to SinqHM-filler */
      union {
        char		    message[32];   /* Ensure buffer is 32 bytes total */
	struct {
	    uint	cmnd;
	    uint	index;
	    usint	new_mask;} uu;
      } u;
    };
/*======================================================= End of SinqHM_def.h */
