/*======================================= [...SinqHM]SinqHM_gbl.h - Ident V01N
**
**	Include file of Gbl Variables for SinqHM_srv and its children.
*/

#ifndef	uint
#define	    uint    unsigned int
#endif

#ifndef	usint
#define	    usint    unsigned short int
#endif

#ifndef	uchar
#define	    uchar    unsigned char
#endif
/*
**====================	Global Variables ======================================
** Note:
**	On VxWorks, all these global variables are visible by all tasks.
**	Any changes to them should therefore only be done if protected
**	by a semaphore.
**
**	At present, only the parent process changes these variables in a
**	'significant' way (I don't count updating diagnostic counters such
**	as Rw_bytes_got!).
*/
    int		My_errno;
    int		Cnt_SIGUSR1;
    char	Inet_host[32];
    char	Inet_addr[20];

    int	     Dbg_mask;		    /* This is the log'l or of Dbg_lev0, etc */
    int	     Dbg_lev0, Dbg_lev1, Dbg_lev2, Dbg_lev3;

    int	     Cfgn_done;		/* == 0 if SinqHM_srv not configured */
  /*
  **		The buffer for holding Hist Mem data is pointed to
  **		by Hist_base_addr.
  */
    void    *Hist_base_addr;
  /*
  ** The MEN board has a divided memory architecture: below 32 MB everything
  ** is normal. The area above has to be allocated in a separate memory 
  ** partition with the partition number stored in sysHighMemPart. This
  ** now is a flag which determines if Hist_base_addr has been allocated in 
  ** such a way in order to be able to free it properly again. 
  **/
  int Hist_base_MEN_alloc;

  /*
  **		The buffer for holding a frame of data for CERCA type
  **		interfaces (e.g. HRPT) is pointed to by Frame_base_addr.
  */
    void    *Frame_base_addr;

    uint	Hm_mode_ALL;	 /* The "total" mode of the histogram memory */
    uint	Hm_mode;	 /* The basic mode of the histogram memory */
    uint	Hm_mode_DEBUG;	 /* The SQHM__DEBUG   bit  of Hm_mode_ALL */
    uint	Hm_mode_UD;	 /* The SQHM__UD      bit  of Hm_mode_ALL */
    uint	Hm_mode_BO;	 /* The SQHM__BO      bits of Hm_mode_ALL */
    uint	Hm_mode_STROBO;	 /* The SQHM__STROBO  bit  of Hm_mode_ALL */
    uint	Hm_mode_REFLECT; /* The SQHM__REFLECT bit  of Hm_mode_ALL */
    uint	Hm_mode_NO_STAT; /* The SQHM__NO_STAT bit  of Hm_mode_ALL */

    uint	N_hists;	/* The number of histograms */
    uint	N_bins;		/* The number of bins per histogram */
    uint	Lo_cntr;	/* The first counter in TOF mode */
    uint	Lo_bin;		/* The first bin of the histogram */
    uint	Hi_bin;		/* The top bin of the histogram */
    uint	Bytes_per_bin;	/* The number of bytes per bin */
    uint	Compress;	/* The bin compression factor */
    uint	Curr_hist;	/* The currently selected histogram */
    uint	Total_bytes;	/* The total number of bytes of histogram mem */
    uint	Cnts_lo;	/* Count of hits below histogram(s) */
    uint	Cnts_hi;	/* Count of hits above histogram(s) */
    uchar      *Tran_buff_base;	/* Pointer to start of curr buff in TRANS mode */
    uchar      *Next_char;	/* Pointer to next free byte in TRANS mode */
    uint	Bytes_free;	/* Space left in current buffer in TRANS mode */
    uint	MDI_mode;	/* Mode of Multi-detector interface as obtained
				**  from the Extended Time-Stamp-Info packets */
    uint	Frame_hdr;	/* The frame header (in HRPT mode) */
    uint	Frame_num;	/* The frame number (in HRPT mode) */

    usint	SinqHM_Dsbl_Mask;	      /* The data-acq disable mask */
    usint	Server_masks[MAX_CLIENTS+1];  /* The daq masks for ..
					      ** ..servers (0 is Filler) */
  /*--------------------------------------*/
    time_t  Sqhm_start_time;	    /* The time that SinqHM-main started */
    uint    Port_base;
    pid_t   Parent_pid;
    pid_t   Child_pid[MAX_CLIENTS+1];
    int	    Child_exit_status[MAX_CLIENTS];
    int	    Child_cnct_skt[MAX_CLIENTS+1];
    int	    Child_rw_skt[MAX_CLIENTS+1];
    pid_t   Filler_pid;		      /* The process ID of the SinqHM-filler */
    char    Filler_name[20];		    /* The name of the SinqHM-filler */
    char    Tsk_name[MAX_CLIENTS+1][20];    /* The name of the SinqHM-servers */

    char    Instr_name[16];	    /* This identifies the instrument */
    char    Def_ident[8];	    /* This is Ident of SinqHM_def.h */
    char    Sqhm_main_ident[8];	    /* This is Ident of main program */
    char    Sqhm_main_date[32];	    /* This is compil date of main program */
    char    Sqhm_serv_ident[8];	    /* This is Ident of server program */
    char    Sqhm_serv_date[32];	    /* This is compil date of server program */
    char    Sqhm_fill_ident[8];	    /* This is Ident of filler program */
    char    Sqhm_fill_date[32];	    /* This is compil date of filler program */
    char    Sqhm_rout_ident[8];	    /* This is Ident of routines */
    char    Sqhm_rout_date[32];	    /* This is compil date of routines */

    uint    Lwl_hdr_daq_mask;	/* Mask of header bits to be tested before
				** histogramming a packet */
    uint    Lwl_hdr_daq_soll;	/* Required value of masked hdr bits */


    uint	Rw_bytes_got, Rw_bytes_put;
    uint	Nrl_active;	 /* True if Neutron Rate Low bit set */
    uint	Daq_active;	 /* True if Hdr Mask Bits are correct so that
					** data acquisition is active */
    int		Print_hdr;	 /* Set to True to get header printed in Filler */
    uint	N_events;	 /* # of events collected */
    uint	N_skipped;	 /* # of events skipped */
    uint	N_no_coin_tsi;	 /* # of "normal" TSI packets */
    uint	N_coin_tsi;	 /* # of "coincidence-mode" TSI packets */
    uint	Tof_dts_soll;    /* Delay-Time-to-Start (soll-wert) */
    uint	Tof_count_bad_counter_number;	/* Counter of number
					    ** of TOF packets received in which
					    ** the counter number is bad.
					    */
    uint	Num_bad_events;	    /* Number of bad events (from last "coinc"
				    ** type TSI */
    union {
      	uint dead_time;
      	uint dts;
      	uint both;
	    } Dt_or_dts; /* Dead-time or Delay-Time-to-Start from TSI packets */
    usint	Tsi_status_info;    /* Status info from last TSI */
    usint	Tsi_flags;	    /* Various flags from last TSI */

    uint   *Lwl_fifo;	    /* This gets set to 0x80810080. ..
			    ** .. It is the address via which ..
			    ** .. one reads data from the fibre ..
			    ** .. optic link (Licht-Wellen-Leiter)
			    */
    uint   *Lwl_csr;	    /* When we have one, this will get ..
			    ** .. set to the addr of the optic ..
			    ** .. link CSR. In the mean time, it ..
			    ** .. is dummy (Lwl_dummy_csr).
			    */
    uint    Lwl_dummy_csr;  /* This is the dummy CSR till we have ..
			    ** .. a real one.
			    */
    uint	Filler_flag;
    SEM_ID	Sem_Filler;
    SEM_ID	Sem_Server;
    MSG_Q_ID	MsgQ_to_filler;
    MSG_Q_ID	MsgQ_to_server[MAX_CLIENTS+1];	/* Index 0 is for messages ..
						** .. to SinqHM-master. The ..
						** others are to the various ..
						** servers.
						*/
    uint	Sem_tmo;
    uint	Use_sem_tmo;    /* If zero, disable sem timeouts */
  /*--------------------------------------*/
    struct tof_histog   *Tof_descr[MAX_TOF_CNTR]; /* An array of pointers to
					    ** the data structures for the
					    ** histos in TOF mode.
					    */
    uint		  N_tof_edges;	    /* The number of edge arrays */
    struct tof_edge_info *Tof_edges[MAX_TOF_EDGE];   /* An array of pointers to
					    ** data structures for the bin
					    ** boundary information.
					    */
  /*--------------------------------------*/
    int			FillTimer_active;   /* Set True if FillTimer active */
    int			FillTimer_expired;  /* Set True in handler */
    timer_t		FillTimerId;	    /* Identifier for Filler timer */
    struct itimerspec	FillerTime;	    /* Set to Filler timer interval */
  /*--------------------------------------*/
    uchar	*VmioBase;	 /* Base address of VMIO10 module, if present */
    uchar	 Vmio_Dummy[64]; /* Dummy VMIO10 addr space, in case absent */

    jmp_buf	 Vmio_trap_env;
/*==========================================================================
  Special variables for decoding PSD data: The PSD detector position is
  calculated as: coord = TXPOS*calibfactor + caliboffset. Thus we need two
  factors for each direction. In addition we need to keep track of flashes 
  and hits on the detector for diagnostic reasons.
  ---------------------------------------------------------------------------*/
  int psdXOffset;
  int psdYOffset;
  int psdXFactor;
  int psdYFactor;
  int psdFlashCount;
  int psdHitCount;
  int psdXSize;
  int psdYSize;
  int psdXORF, psdConf; 
/*========================== Define the function prototypes ================*/

  void	    catch_int_signal (
		int signo);
  void	    catch_machine_check (
		int signo);
  void	    catch_signal (
		int signo);
  int	    chk_config_alloc (
		int			 skt,
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  void	    cleanup_children ();
  int	    close_down_offspring (
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  void	    do_daq (
		struct rply_buff_struct *reply,
		int			 index,
		int			 sub_cmnd);
  int	    do_command (
		int			 index,
		int			 skt,
		int			 pkt_size,
		struct req_buff_struct  *rqst,
		struct rply_buff_struct *reply);
  int	    do_project (
		int			index,
		struct rply_buff_struct	*rply_bf,
		int			rw_skt,
		int			pkt_size,
		uint			sub_code,
		uint			x_lo,
		uint			nx,
		uint			y_lo,
		uint			ny,
		uint			xdim,
		uint			h_slct);
  int	    do_read (
		int			index,
		struct rply_buff_struct	*rply_bf,
		int			rw_skt,
		int			pkt_size,
		uint			nbins,
		uint			first,
		uint			hist_no);
  int	    do_SQHM__HM_DIG_alloc (
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  int	    do_SQHM__HRPT_alloc (
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  int	    do_SQHM__TOF_alloc (
		int			skt,
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  int	    do_SQHM__PSD_alloc (
		int			skt,
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  int	    do_SQHM__TRANS_alloc (
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  int	    do_write (
		int			index,
		struct req_buff_struct	*req_bf,
		struct rply_buff_struct	*rply_bf,
		int			rw_skt,
		int			pkt_size,
		int			nbins,
		int			first,
		int			bytes_per_bin,
		int			hist_no);
  void	    do_zero (
		struct rply_buff_struct *reply,
		int			 index,
		int			 nbins,
		int			 fst_bin,
		int			 hist_no);
  void	    failInet (
		char *text);
  void	    failInet_port (
		char *text, int port);
  void	    free_HM_memory (void *extras);
  void	    getErrno (
		int *his_errno);
  pid_t	    get_pid ();
  int	    hm_zero (
		int index, int nbins, int first, int hist_no);
  void	    kill_children ();
  void	    lwl_Fifo_Flush ();
  int	    lwl_Packet_Read (
		uint   hdr,
		uchar *buff);
  int	    make_child (
		int proc (),
		int flag,
		int pkt_size);
  int	    make_filler (
		int suspendMode);
  int	    net_flush (
		int skt,
		int nbytes);
  void	    process_coinc_tsi (
		uint hdr);
  void	    process_psd_tsi (
		uint hdr);
  void	    process_no_coinc_tsi (
		uint hdr);
  void      process_gummi_monitor(
		uint hdr);

  int	    rply_status_send (
		int			 skt,
		struct rply_buff_struct	*rply_bf);
  void	    rply_status_setup (
		struct rply_buff_struct	*rply_bf,
		int			 status,
		int			 sub_status,
		char			*msg);
  int	    rply_status_setup_and_send (
		int			 skt,
		struct rply_buff_struct	*rply_bf,
		int			 status,
		int			 sub_status,
		char			*msg);
  uchar	   *selectNewBuffer (uchar *nxt);
  void	    setupLevelGenerator (
		int   use_level_gen,
		char *base_addr);
  void	    setup_inet_info (
		char	*host,
		int	 host_size,
		char	*addr,
		int	 addr_size);
  int	    sleep (
		int xx);
  char	   *StrJoin (
		char	*result,
		int	 result_size,
		char	*str_a,
		char	*str_b);
  int	    SinqHM_filler (
		int suspend_flag);
  void	    SinqHM_filler_setup_id ();
  void	    SinqHM_routines_setup_id ();
  void	    SinqHM_server_setup_id ();
  int	    SinqHM_server (
		int suspend_flag,
		int index,
		int pkt_size);

/*===================================================== End of SinqHM_gbl.h  */
