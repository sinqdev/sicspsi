/*======================================= [...SinqHM]FOCUS_gbl.h - Ident V01B
**
**	Include file of Gbl Variables for FOCUS_srv and its children.
*/

#ifndef	uint
#define	    uint    unsigned int
#endif

#ifndef	usint
#define	    usint    unsigned short int
#endif

#ifndef	uchar
#define	    uchar    unsigned char
#endif

#ifndef MAX_SLV
#define MAX_SLV 3
#endif
/*
**====================	Global Variables ======================================
** Note:
**	On VxWorks, all these global variables are visible by all tasks.
**	Any changes to them should therefore only be done if protected
**	by a semaphore.
**
**	At present, only the parent process changes these variables in a
**	'significant' way (I don't count updating diagnostic counters such
**	as FS_bytes_got!).
*/
    int		FS_errno;
    int		FS_cnt_SIGUSR1;
    char	FS_host[32];
    char	FS_addr[20];

    int	     FS_dbg_mask;	/* This is the log'l or of FS_dbg_lev0, etc */
    int	     FS_dbg_lev0, FS_dbg_lev1, FS_dbg_lev2, FS_dbg_lev3;

    int	     FS_cfgn_done;		/* == 0 if FOCUS_srv not configured */
  /*
  **		The buffer for holding Hist Mem data is pointed to
  **		by FS_base_addr.
  */
    void    *FS_base_addr;

    uint	FS_mode_ALL;	  /* The "total" mode of the histogram memory */
    uint	FS_mode;	  /* The basic mode of the histogram memory */
    uint	FS_mode_DEBUG;	  /* The SQHM__DEBUG   bit  of FS_mode_ALL */
    uint	FS_mode_UD;	  /* The SQHM__UD      bit  of FS_mode_ALL */
    uint	FS_mode_BO;	  /* The SQHM__BO      bits of FS_mode_ALL */
    uint	FS_mode_STROBO;	  /* The SQHM__STROBO  bit  of FS_mode_ALL */
    uint	FS_mode_REFLECT;  /* The SQHM__REFLECT bit  of FS_mode_ALL */
    uint	FS_mode_NO_STAT;  /* The SQHM__NO_STAT bit  of FS_mode_ALL */

    uint	FS_lo_cntr;	/* The first counter in TOF mode */
    uint	FS_hi_cntr;	/* The last  counter in TOF mode */
    uint	FS_lo_bin;	/* The first bin of the histogram */
    uint	FS_hi_bin;	/* The last  bin of the histogram */
    uint	FS_bpb;		/* The number of bytes per bin */
    uint	FS_compress;	/* The bin compression factor */
    uint	FS_totl_bytes;	/* The total number of bytes of histogram mem */

    SEM_ID	FS_sem_daq;		   /* Semaphore to guard FS_dsbl_mask */
    usint	FS_dsbl_mask;		      /* The data-acq disable mask */
    usint	FS_srv_masks[MAX_CLIENTS+1];  /* The daq masks for ..
					      ** .. servers (0 is mask of ..
					      ** .. master process). */
  /*--------------------------------------*/
    time_t  FS_start_time;	    /* The time that FOCUS-main started */
    uint    FS_port;
    int	    FS_cnct_skt;
    int	    FS_rw_skt;
    pid_t   FS_pid;			/* PID of Master Process */
    pid_t   FS_pids[MAX_CLIENTS+1];	/* Index 0 = Filler (not present for
					**		FOCUS_srv)
					** Index 1... = Child servers */
    int	    FS_chld_exit_status[MAX_CLIENTS+1];
    int	    FS_chld_cnct_skt[MAX_CLIENTS+1];
    int	    FS_chld_rw_skt[MAX_CLIENTS+1];
    char    FS_name[MAX_CLIENTS+1][20];    /* The name of the FOCUS-servers */
    char   *FS_p_names[MAX_CLIENTS+1];	   /* Pointers to the names */

    char    FS_inst[16];	    /* This identifies the instrument */
    char    FS_def_ident[8];	    /* This is Ident of FOCUS_def.h */
    char    FS_main_ident[8];	    /* This is Ident of main program */
    char    FS_main_date[32];	    /* This is compil date of main program */
    char    FS_serv_ident[8];	    /* This is Ident of server program */
    char    FS_serv_date[32];	    /* This is compil date of server program */
    char    FS_rout_ident[8];	    /* This is Ident of routines */
    char    FS_rout_date[32];	    /* This is compil date of routines */

    uint	FS_bytes_got, FS_bytes_put;

    MSG_Q_ID	FS_msgQ_to_srv[MAX_CLIENTS+1];	/* Index 0 is for messages ..
						** .. to FOCUS-master. The ..
						** others are to the various ..
						** servers.
						*/
    uint	FS_sem_tmo;
    uint	FS_use_sem_tmo;    /* If True (== 1), enable sem timeouts */

    int			 FS_n_slaves;		  /* The number of slaves */
    char		*FS_slaves[MAX_SLV];	  /* The names of the slaves */
    struct sockaddr_in	 FS_rmt_socknam[MAX_SLV]; /* Internet addresses of ..
						  **	.. our slaves */
    uint		 FS_lo_det[MAX_SLV];	  /* # of first det'r on each ..
						  **	.. slave */
    uint		 FS_hi_det[MAX_SLV];	  /* # of last  det'r on each ..
						  **	.. slave */
    int			 FS_slv_active[MAX_SLV];  /* True if slave has been ..
						  **	.. cfg'd */
    int			 FS_skt[MAX_CLIENTS+1][MAX_SLV];   /* Client skts for ..
							   **	.. children */
    int			 FS_pkt_siz[MAX_CLIENTS+1][MAX_SLV]; /* Packet sizes ..
							   **  .. for childrn */
/*========================== Define the function prototypes ================*/

  int	    chk_config_alloc (
		int			 skt,
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  void	    failInet (
		char *text);
  void	    failInet_port (
		char *text, int port);
  int	    FOCUS_server (
		pid_t	 parent_pid,
		SEM_ID	 sem,
		int	 index,
		int	 pkt_size,
		int	 suspend_flag,
		int	*status);
  void	    FS_catch_int_signal (
		int signo);
  void	    FS_catch_signal (
		int signo);
  void	    FS_cleanup_children (
		int	  max_chld,
		pid_t	 *pids,
		int	 *pcnt_SIGUSR1,
		int	  verbose,
		char	**tsk_names,
		char	 *filler_name);
  int	    FS_close_all (
		int	  index);
  int	    FS_close_down_offspring (
		int	  harshness,
		int	  max_chld,
		pid_t	 *pids,
		int	 *exit_stat,
		int	 *pcnt_SIGUSR1,
		int	  verbose,
		char	**tsk_names,
		char	 *filler_name);
  int	    FS_deconfig_all (
		int	harshness);
  int	    FS_do_command (
		int			 index,
		int			 skt,
		int			 pkt_size,
		struct req_buff_struct  *rqst,
		struct rply_buff_struct *reply);
  int	    FS_do_config (
		int			 skt,
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  int	    FS_do_daq (
		int			 index,
		int			 sub_cmnd,
		struct req_buff_struct  *request,
		struct rply_buff_struct *reply);
  int	    FS_do_project (
		int			index,
		struct rply_buff_struct	*rply_bf,
		int			rw_skt,
		int			pkt_size,
		uint			sub_code,
		uint			x_lo,
		uint			nx,
		uint			y_lo,
		uint			ny,
		uint			xdim,
		uint			h_slct);
  int	    FS_do_read (
		int			index,
		struct rply_buff_struct	*rply_bf,
		int			rw_skt,
		int			pkt_size,
		uint			nbins,
		uint			first,
		uint			hist_no);
  int	    FS_do_write (
		int			index,
		struct req_buff_struct	*req_bf,
		struct rply_buff_struct	*rply_bf,
		int			rw_skt,
		int			pkt_size,
		int			nbins,
		int			first,
		int			bytes_per_bin,
		int			hist_no);
  int	    FS_do_zero (
		struct rply_buff_struct *reply,
		int			 index,
		int			 nbins,
		int			 fst_bin,
		int			 hist_no);
  void	    FS_free_memory (
		void	*extras);
  void	    FS_kill_children ();
  int	    FS_make_child (
		int	 proc (),
		int	 prty,
		int	 spnd,
		int	 pkt_size,
		int	 max_chld,
		pid_t	*pids,
		int	*exit_stat,
		int	*pcnt_SIGUSR1,
		char	*name);
  int	    FS_project_hm (
		int	 index,
		int	 pkt_size,
		uint	*buff,
		uint	*w_buff,
		uint	 blen_in_bins,
		uint	 sub_code,
		uint	 lo_cntr,
		uint	 hi_cntr,
		uint	 fst_bin,
		uint	 lst_bin);
  int	    FS_read_hm (
		int	 skt,
		int	 pkt_size,
		uchar	*buff,
		uint	 blen_in_bins,
		uint	 first_bin,
		uint	 last_bin);
  int	    FS_rply_status_send (
		int			 skt,
		struct rply_buff_struct	*rply_bf,
		uint			*out_cntr);
  int	    FS_rply_status_setup_and_send (
		int			 skt,
		struct rply_buff_struct	*rply_bf,
		int			 status,
		int			 sub_status,
		char			*msg,
		uint			*out_cntr);
  int	    FS_send_cmnd_to_mstr (
		struct sockaddr_in	*rmt_sockname,
		struct req_buff_struct	*request,
		int			 requ_len,
		struct rply_buff_struct	*reply,
		uint			*in_cntr,
		uint			*out_cntr);
  void	    getErrno (
		int *his_errno);
  pid_t	    get_pid ();
  int	    net_flush (
		int skt,
		int nbytes);
  void	    rply_status_setup (
		struct rply_buff_struct	*rply_bf,
		int			 status,
		int			 sub_status,
		char			*msg);
  int	    rqst_send_get (
		int			 skt,
		struct req_buff_struct	*rqst_bf,
		int			 rq_len,
		struct rply_buff_struct	*rply_bf,
		int			 rp_size,
		int			*out_cntr,
		int			*in_cntr,
		char			*tsk_name);
  int	    setup_host (
		char		   *host,
		struct sockaddr_in *sockname,
		int		    port,
		int		    verbose);
  void	    setup_inet_info (
		char	*host,
		int	 host_size,
		char	*addr,
		int	 addr_size);
  int	    sleep (
		int xx);
  int	    socket_bind (
		int	 port,
		char	*tsk_name);
  char	   *StrJoin (
		char	*result,
		int	 result_size,
		char	*str_a,
		char	*str_b);

/*===================================================== End of FOCUS_gbl.h  */
