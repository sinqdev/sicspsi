#ifndef	    VMIO10_DEF_ID
#define	    VMIO10_DEF_ID   "V01A"
/*=================================================== [...SINQHM]VMIO10_DEF.H
**
**	Definition Include file for VMIO10 VME Module.
**
**  The module has 2 Z8536-CIO chips, each of which has 2 I/O ports (A and B)
**  and 3 counter/timer registers. Hence, the module has 4 I/O ports and
**  6 counter/timers. Just to confuse things, we call the 4 ports A, B, C and D!
**  Ports A & B are associated with the first Z8536-CIO and Ports C & D are
**  associated with the second Z8536-CIO.
**------------------------------------------------------------------------------
**	    VME offsets for the first Z8536-CIO.
*/
#define	VMIO_PORT_AB	0x01
#define	VMIO_PORT_B	0x11
#define	VMIO_PORT_A	0x21
#define	VMIO_AB_CNTL	0x31
/*
**	    VME offsets for the second Z8536-CIO.
*/
#define	VMIO_PORT_CD	0x09
#define	VMIO_PORT_D	0x19
#define	VMIO_PORT_C	0x29
#define	VMIO_CD_CNTL	0x39
/*
**	    Z8536-CIO register definitions
*/
#define Z8536_MICR	0x00	/* Master Interrupt Control Register */
#define	Z8536_MCCR	0x01	/* Master Config Control Register */

#define Z8536_DPPR_A	0x22	/* Data Path Polarity Register - Port A */
#define Z8536_DPPR_B	0x2a	/* Data Path Polarity Register - Port B */
#define Z8536_DPPR_C	0x05	/* Data Path Polarity Register - Port C */

#define Z8536_DDR_A	0x23	/* Data Direction Register - Port A */
#define Z8536_DDR_B	0x2b	/* Data Direction Register - Port B */
#define Z8536_DDR_C	0x06	/* Data Direction Register - Port C */

#define Z8536_SIOCR_A	0x24	/* Special I/O Control Register - Port A */
#define Z8536_SIOCR_B	0x2c	/* Special I/O Control Register - Port B */
#define Z8536_SIOCR_C	0x07	/* Special I/O Control Register - Port C */
/*======================================================= End of VMIO10_DEF.H */
#endif
