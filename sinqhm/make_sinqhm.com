$!	TAS_SRC:[SINQHM]MAKE_SINQHM.COM		Ident V01A
$!	=============================
$!
$! V01D 12-May-2000 DM.	Initial version
$!-------------------------------------------------------------------------
$	progs = "lwl_client," + -
		"sinqhm_bootutil_client," + -
		"sinqhm_client," + -
		"sinqhm_ctrl"
$	progs = f$edit (progs, "collapse, upcase") + ","
$!-------------------------------------------------------------------------
$	if p1 .nes. "?" .and. -
	   p1 .nes. "H" .and. -
	   p1 .nes. "HE" .and. -
	   p1 .nes. "HEL" .and. -
	   p1 .nes. "HELP" then goto skip_help
$	  type sys$input

 -------------------------------------------------------------------------
  Usage:
		@TAS_SRC:[SINQHM]MAKE_SINQHM.COM  <exe-list>
	where
		<exe-list> = list of programs to make.

  <exe-list> must be a comma separated list without white space chars.

  If <exe-list> is empty, everything is created. Otherwise, just the named
  programs are created. The programs which may be selected are:
$
$	  cnt = 0
$ exe_loop:
$	exe = f$element (cnt, ",", progs)
$	cnt = cnt + 1
$	if (exe .nes. ",") .and. (exe .nes. "") then say "	''exe'
$	if exe .nes. "," then goto exe_loop
$	  type sys$input

  All programs are compiled with debug. If MAD_EXE is not
  "TASMAD_DISK:[MAD.EXE]", _DBG versions of .EXE files are also created.
 -------------------------------------------------------------------------
$	  exit
$ skip_help:
$	say := write sys$output
$	bel[0,8] = 7
$
$	fopt = "/debug/noopt/include=tas_src:[def]"
$	if f$getsyi("ARCH_TYPE") .eq. 2 then fopt = fopt + "/sep"
$
$	copt = "/decc/prefix=all/debug/noopt"
$
$	no_dbg := false
$	if f$trnlnm ("mad_exe") .eqs. "TASMAD_DISK:[MAD.EXE]" then -
	  no_dbg := true
$	if .not. no_dbg then say "Debug execs will also be created ...
$! ***********************************************************
$	targ = f$element (0, "]", f$environment ("procedure")) + "]"
$	targ_obj = f$element (0, "]", f$environment ("procedure")) + ".OBJ]"
$
$	mad_exe = f$trnlnm ("mad_exe")
$
$	if p1 .eqs. "" then p1 := all
$	if p1 .eqs. "ALL" then goto do_all
$!
$!		Get here to build selected utilities.
$!
$	  cnt = 0		! First loop selecting no progs.
$ lp0:
$	nxt = f$element (cnt, ",", progs)
$	cnt = cnt + 1
$	if nxt .eqs. "" then goto lp0_end
$	  do_'nxt' := false
$	  goto lp0
$ lp0_end:
$!				Now pick out the items in P1
$!
$	pp1 = f$edit (p1, "collapse, upcase") + ","
$	u_len = f$length (progs)
$	cnt = 0
$ lp1:
$	nxt = f$element (cnt, ",", pp1)
$	nxtt = nxt + ","
$	cnt = cnt + 1
$	if nxt .eqs. "" then goto lp1_end
$	  if f$locate (nxtt, progs) .lt. u_len then goto lp1_ok
$	    say "''bel'''nxt' is not a recognised TASMAD utility!
$	    exit
$ lp1_ok:
$	do_'nxt' := true
$	goto lp1
$ lp1_end:
$	goto do_comn
$!----------------------------------------------------------------------
$ do_all:
$	cnt = 0			! Loop selecting all progs.
$ lp2:
$	nxt = f$element (cnt, ",", progs)
$	cnt = cnt + 1
$	if nxt .eqs. "" then goto lp2_end
$	  do_'nxt' := true
$	  goto lp2
$ lp2_end:
$ do_comn:
$!-----------------------------------------------------------------
$!		Compile and build LWL_CLIENT
$!
$	if .not. do_lwl_client then goto skp_lwl_client
$	  say "Compiling and linking LWL_CLIENT ...
$	  cc 'copt' 'targ'lwl_client + tas_src:[lib]sinq_c.tlb/lib
$	  link /exe=mad_exe:lwl_client sys$input/opt
		lwl_client
		tas_src:[lib]sinq_dbg/lib
		sys$share:decw$xlibshr/share
$	  purge mad_exe:lwl_client.exe
$	  rename mad_exe:lwl_client.exe;0 lwl_client.exe;1
$
$	  if no_dbg then goto lwl_client_no_dbg
$	    link /debug /exe=mad_exe:lwl_client_dbg sys$input/opt
		lwl_client
		tas_src:[lib]sinq_dbg/lib
		sys$share:decw$xlibshr/share
$	    purge mad_exe:lwl_client_dbg.exe
$	    rename mad_exe:lwl_client_dbg.exe;0 lwl_client_dbg.exe;1
$ lwl_client_no_dbg:
$	delete/nolog lwl_client.obj;*
$ skp_lwl_client:
$!-----------------------------------------------------------------
$!		Compile and build SINQHM_BOOTUTIL_CLIENT
$!
$	if .not. do_sinqhm_bootutil_client then goto skp_sinqhm_bootutil_client
$	  say "Compiling and linking SINQHM_BOOTUTIL_CLIENT ...
$	  cc 'copt' 'targ'sinqhm_bootutil_client + tas_src:[lib]sinq_c.tlb/lib
$	  link /exe=mad_exe:sinqhm_bootutil_client sys$input/opt
		sinqhm_bootutil_client
		tas_src:[lib]sinq_dbg/lib
		sys$share:decw$xlibshr/share
$	  purge mad_exe:sinqhm_bootutil_client.exe
$	  rename mad_exe:sinqhm_bootutil_client.exe;0 -
	    sinqhm_bootutil_client.exe;1
$
$	  if no_dbg then goto sinqhm_bootutil_client_no_dbg
$	    link /debug /exe=mad_exe:sinqhm_bootutil_client_dbg sys$input/opt
		sinqhm_bootutil_client
		tas_src:[lib]sinq_dbg/lib
		sys$share:decw$xlibshr/share
$	    purge mad_exe:sinqhm_bootutil_client_dbg.exe
$	    rename mad_exe:sinqhm_bootutil_client_dbg.exe;0 -
	      sinqhm_bootutil_client_dbg.exe;1
$ sinqhm_bootutil_client_no_dbg:
$	delete/nolog sinqhm_bootutil_client.obj;*
$ skp_sinqhm_bootutil_client:
$!-----------------------------------------------------------------
$!		Compile and build SINQHM_CLIENT
$!
$	if .not. do_sinqhm_client then goto skp_sinqhm_client
$	  say "Compiling and linking SINQHM_CLIENT ...
$	  cc 'copt' 'targ'sinqhm_client + tas_src:[lib]sinq_c.tlb/lib
$	  link /exe=mad_exe:sinqhm_client sys$input/opt
		sinqhm_client
		tas_src:[lib]sinq_dbg/lib
		pgplot_dir:cpgplot.olb/lib
		pgplot_dir:grpckg.olb/lib
		sys$share:decw$xlibshr/share
$	  purge mad_exe:sinqhm_client.exe
$	  rename mad_exe:sinqhm_client.exe;0 sinqhm_client.exe;1
$
$	  if no_dbg then goto sinqhm_client_no_dbg
$	    link /debug /exe=mad_exe:sinqhm_client_dbg sys$input/opt
		sinqhm_client
		tas_src:[lib]sinq_dbg/lib
		pgplot_dir:cpgplot.olb/lib
		pgplot_dir:grpckg.olb/lib
		sys$share:decw$xlibshr/share
$	    purge mad_exe:sinqhm_client_dbg.exe
$	    rename mad_exe:sinqhm_client_dbg.exe;0 sinqhm_client_dbg.exe;1
$ sinqhm_client_no_dbg:
$	delete/nolog sinqhm_client.obj;*
$ skp_sinqhm_client:
$!-----------------------------------------------------------------
$!		Compile and build SINQHM_CTRL
$!
$	if .not. do_sinqhm_ctrl then goto skp_sinqhm_ctrl
$	  say "Compiling and linking SINQHM_CTRL ...
$	  cc 'copt' 'targ'sinqhm_ctrl + tas_src:[lib]sinq_c.tlb/lib
$	  link /exe=mad_exe:sinqhm_ctrl sys$input/opt
		sinqhm_ctrl
		tas_src:[lib]sinq_dbg/lib
		sys$share:decw$xlibshr/share
$	  purge mad_exe:sinqhm_ctrl.exe
$	  rename mad_exe:sinqhm_ctrl.exe;0 sinqhm_ctrl.exe;1
$
$	  if no_dbg then goto sinqhm_ctrl_no_dbg
$	    link /debug /exe=mad_exe:sinqhm_ctrl_dbg sys$input/opt
		sinqhm_ctrl
		tas_src:[lib]sinq_dbg/lib
		sys$share:decw$xlibshr/share
$	    purge mad_exe:sinqhm_ctrl_dbg.exe
$	    rename mad_exe:sinqhm_ctrl_dbg.exe;0 sinqhm_ctrl_dbg.exe;1
$ sinqhm_ctrl_no_dbg:
$	delete/nolog sinqhm_ctrl.obj;*
$ skp_sinqhm_ctrl:
$!-----------------------------------------------------------------
$	exit
