/*
 * This is a special version of the second generation counter for ZEBRA.
 * ZEBRA has two detector units. The analyser units detetcor is patched to 
 * the monitor channel 2. Thus sometimes the counts are normal, in index 0 
 * of the data, sometimes in counter 2. This special version takes care of 
 * this.
 *
 * copyright: see file COPYRIGHT
 *
 * Mark Koennecke, July 2018
 */
#include <sics.h>
#include <counter.h>
#include <sicshipadaba.h>

/*
  in countersec.c in the sics kernel directory
*/
extern long  SecCtrGetMonitor(pCounter self, int iNum, SConnection *pCon);
/*--------------------------------------------------------------------------*/
static long ZebraCtrGetCounts(pCounter self, SConnection *pCon)
{
  int idx = 0;
  pHdb node = NULL;

  node = GetHipadabaNode(self->objectNode,"countidx");
  assert(node != NULL);

  idx = node->value.v.intValue;
  return SecCtrGetMonitor(self,idx,pCon);
}
/*--------------------------------------------------------------------------*/
int MakeZebraCter(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[])
{
  pCounter pRes = NULL;
  int status, length;
  pHdb node, child;

  if(argc < 2) {
    SCWrite(pCon,"ERROR: need at least a name to create a counter",
	    eError);
    return 0;
  }

  pRes = CreateSecCounter(pCon,"SingleCounter", argv[1], 8);
  if(pRes == NULL){
    return 0;
  }

  pRes->getCounts = ZebraCtrGetCounts;

  child = MakeSICSHdbPar("countidx", usMugger, MakeHdbInt(0));
  if (child != NULL) {
    SetHdbProperty(child, "__save", "true");
    AddHipadabaChild(pRes->objectNode, child, NULL);
  }

  status =
        AddCommand(pSics, argv[1], InterInvokeSICSOBJ, DeleteCounter,
		      (void *) pRes);
  if (status != 1) {
    SCPrintf(pCon,eError, "ERROR: duplicate command %s not created", argv[1]);
    return 0;
  }
  return 1;
}
