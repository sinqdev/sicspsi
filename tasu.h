/*
  This is a helper header file which contains the prototypes for some
  functions to be used in the implementation of the triple axis spectrometer
  module. For more info on the functions see the comments in the
  implementation file tasutil.c

  Initial Coding: Mark Koennecke, November 2000
  Added TASUpdate: Mark Koennecke, May 2001  

*/
#ifndef TASUSICS
#define TASUSICS

extern char *tasMotorOrder[];
extern char *tasVariableOrder[];

/* maximum number of motors in the list */
#define MAXMOT 35
/* offset to the currents, if available */
#define CURMOT 21


/*
  Note: the defines below MUST map the range between EI - HZ in the list
  of variables as defined in tas.h. Otherwise quite interesting things
  can happen. ETARGET is the variable order index for the energy targets.
*/
#define EMIN 25
#define EMAX 36
#define ETARGET 94

int isTASMotor(char *val);
int isTASCurrent(char *val);
int isTASVar(char *val);
int isTASEnergy(char *val);
void prepare2Parse(char *line);
int getSRO(SConnection * pCon, float *sroVal);
int tasNumeric(char *pText);
float readDrivable(char *val, SConnection * pCon);
void startCurrent(char *val, SConnection * pCon, float fVal);
void readConversionFactors(pTASdata self, float convH[4]);

int TASCalc(pTASdata self, SConnection * pCon,
            unsigned char tasMask[10],
            float motorTargets[20], unsigned char motorMask[20]);
int TASStart(pTASdata self, SConnection * pCon, SicsInterp * pSics,
             float motorTargets[20], unsigned char motorMask[20]);

int TASUpdate(pTASdata self, SConnection * pCon);


#endif
