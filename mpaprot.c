/*
 * This is scriptcontext protocol handler for the FastCOM MPA 
 * module used for the movable chopper at SINQ. We talk to the device 
 * through the serial port. This requires a windows machine running the 
 * MPA3 program with the communications extensions. Again, this is an 
 * awful protocol provided by a company who does not really do software, 
 * to say it in a polite way. Ths protocol driver will probably be brittle:
 * - It relies on a certain version of the MPA software running on the windows 
 *   composter
 * - It is tailored to how Masako uses the thing. The thing can do more. The 
 *   more is not supported by this implementation.
 *
 * The protocol is difficult: there is no proper message termination. The 
 * only way to get at the data is to save a file and have that file sent. So, 
 * this is what is supported by this protocol driver:
 * - echo:command splits of command and sends ist, expects an echo of the 
 *   command as reply.
 * - q:mpa sends MPA? to the device and reads a fixed number of lines as 
 *   a response. This is the status request. The contained On/Off is used to 
 *   update the status. The remainder is stored as text in an mpa node.
 * - q:getdata:mpafilename does the sequence of mpafile=fname, mpasave and sendfile. The 
 *   response is analyzed: the TDAT1 stuff is taken as data. The rest is put 
 *   into a node called sfile.
 *
 * Thus this protocol also requires a special node structure which is 
 * basically a histogram memory with some added nodes. Thus there will be 
 * an accompanying mpa.tcl file which sets all that shit up.
 *
 * Mark Koennecke, April 2018       
 */
#include <errno.h>
#include <ascon.h>
#include <ascon.i>
#include <dynstring.h>

#define ECHO 1
#define QMPA 2
#define SNDFILE 3

typedef struct {
  unsigned int mode;
}MPA, *pMPA;
/*--------------------------------------------------------------------------*/
static int MPAProtHandler(Ascon *a)
{
  char *writeData, *readData, *pPtr, *pTmp;
  pMPA mpa;
  int ret;

  mpa  = (pMPA)a->private;

  switch(a->state){
  case AsconWriteStart:
    writeData = DynStringGetArray(a->wrBuffer);
    if(strstr(writeData,"echo:") != NULL){
      mpa->mode = ECHO;
      a->lineCount = 1;
      a->wrPos = 5; /* skip over the echo: */
    }
    if(strstr(writeData,"q:mpa") != NULL){
      mpa->mode = QMPA;
      a->lineCount = 8;
      a->wrPos = 0;
      DynStringCopy(a->wrBuffer,"MPA?\r\n");
    }
    if(strstr(writeData,"q:getdata") != NULL){
      mpa->mode = SNDFILE;
      a->wrPos = 0;
      a->lineCount = 1;
      pPtr = writeData + strlen("q:getdata:");
      pTmp = strdup(pPtr);
      DynStringCopy(a->wrBuffer,"sendfile ");
      DynStringConcat(a->wrBuffer,pTmp);
      DynStringConcat(a->wrBuffer,"\r\n");
      free(pTmp);
    }
    a->state = AsconWriting;
    return 1;
    break;
  case AsconReading:
    if(mpa->mode != SNDFILE){
      return AsconStdHandler(a);
    } else {
      /*
	find the data start header, adjust line count and go on
       */ 
      ret = AsconStdHandler(a);
      if(a->state == AsconReadDone){
	readData = DynStringGetArray(a->rdBuffer);
	if(strstr(readData,"[CDAT4") != NULL){
	  readData = readData+7; /* jump forward to , */
	  pPtr = strchr(readData,(int)']');
	  *pPtr = '\0'; /* nuke the terminating ] */
	  a->lineCount = atoi(readData);
	  mpa->mode = QMPA;
	}
	DynStringClear(a->rdBuffer);
	a->state = AsconReading; /* read more lines */
      }
      if(a->state == AsconTimeout){
	DynStringCopy(a->rdBuffer,"AscErr: Timeout");
      }
      return ret; 
    }
    break;
  default:
    return AsconStdHandler(a);
  }
}
/*---------------------------------------------------------------------------*/
static int MPAInit(Ascon * a, SConnection * con,
                        int argc, char *argv[])
{
  pMPA mpa;
  int ret; 

  ret = AsconStdInit(a,con,argc,argv);

  mpa = calloc(sizeof(MPA),1);
  a->private = mpa;
  a->killPrivate = free;

  return ret;
}
/*---------------------------------------------------------------------------*/
void AddMPAProtocoll()
{
  AsconProtocol *prot = NULL;

  prot = calloc(sizeof(AsconProtocol), 1);
  prot->name = strdup("mpa");
  prot->init = MPAInit;
  prot->handler = MPAProtHandler;
  AsconInsertProtocol(prot);
}
