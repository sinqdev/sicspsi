
/*--------------------------------------------------------------------------
                         S I C S  S E R I A L

  This object adds the serial command to the Tcl-interpreter within SICS.
  Furthermore the commands sr1-sr6 are added as predefined names for 
  possible connections. 

  Mark Koennecke, January 1998
----------------------------------------------------------------------------*/
#include <sics.h>
#include <tcl.h>

int Controller(ClientData clientData, Tcl_Interp * interp,
               int argc, char *argv[]);

int SerialInit(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[])
{
  int iRet;

  assert(pSics);
  Tcl_CreateCommand(pSics->pTcl, "serialport", Controller, NULL, NULL);
  return 1;
}
