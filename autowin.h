/**
 * This is the calculation of automatic windows for RITA's 
 * monochromatic imaging mode. For more see comment in autowin.c.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, July 2009
 */ 
#ifndef AUTOWIN_H_
#define AUTOWIN_H_
int MakeRitaWin(SConnection *pCon, SicsInterp *pSics, void *pData, 
		int argc, char *argv[]);

#endif /*AUTOWIN_H_*/
