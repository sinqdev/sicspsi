/*
 * This is yet another protocol handler for scriptcontext. This one collects response 
 * data until the timeout has expired. And then suppresses the timeout error. It if for 
 * the radial collimator at HRPT which does not have a proper terminator. 
 * Mark Koennecke, July 2017
 */
#include <errno.h>
#include <ascon.h>
#include <ascon.i>
#include <dynstring.h>

int CollectProtHandler(Ascon *a)
{
  int ret;
  char chr;

  switch(a->state){
  case AsconReading:
    ret = AsconReadChar(a->fd, &chr);
    if (ret <= 0) {
      if (ret < 0) {
        AsconError(a, "ASC5", errno);
        return 0;
      }
      if (a->timeout > 0) {
        if (DoubleTime() - a->start > a->timeout) {
	  a->state = AsconReadDone;
	  return 1;
        }
      }
      return 0;
    }
    a->lastChar = chr;
    a->start = DoubleTime();
    if (DynStringConcatChar(a->rdBuffer, chr) == 0) {
      AsconError(a, "ASC6", errno);
      return 0;
    }
    break;
  default:
    return AsconStdHandler(a);
  }
  return 1;
}
/*----------------------------------------------------------------------------------------------*/
void AddCollectProtocoll()
{
  AsconProtocol *prot = NULL;

  prot = calloc(sizeof(AsconProtocol), 1);
  prot->name = strdup("collect");
  prot->init = AsconStdInit;
  prot->handler = CollectProtHandler;
  AsconInsertProtocol(prot);
}
