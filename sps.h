
/*------------------------------------------------------------------------
                            S P S

  A module to handle Siemens SPS controllers at SINQ. For more information 
  see sps.tex

  Mark Koennecke, Juli 1998
----------------------------------------------------------------------------*/
#ifndef SICSSPS
#define SICSSPS

typedef struct __SPS *pSPS;
/*-------------------------------------------------------------------------*/
int SPSSetButton(pSPS self, SConnection * pCon, int iByte, int iBit);
int SPSGetStatus(pSPS self, int iStatus, int *iSet);
int SPSGetSANS(pSPS self, float *fVal);
int SPSGetADC(pSPS self, int iWhich, int *iValue);
void SPSAddPermission(pSPS self, int iByte, int iBit, int iRight);
/*------------------------------------------------------------------------*/
int SPSFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);

int SPSAction(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);

#endif
