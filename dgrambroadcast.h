
#define MAX_BROADCAST_MSG_LEN 1472

int openBroadcastSocket( /* void */ );
void closeBroadcastSocket( /* sendSocket */ );
int sendBroadcastMessage( /* sendSocket, sendPort, sndMsg, sndMsgLen */ );

int openReceiveSocket( /* port */ );
int readReceiveSocket( /* recvSocket, buff, len */ );
int selectReceiveSocket( /* recvSocket, usec */ );
