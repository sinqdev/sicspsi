/*---------------------------------------------------------------------------
strobj.c

a string object based on pardef, logging is on by default

Markus Zolliker, March 2005
----------------------------------------------------------------------------
*/

#include <stdio.h>
#include "sics.h"
#include "splitter.h"
#include "pardef.h"
#include "initializer.h"

typedef struct {
  ParData p;
  char *str;
} StrObj;

static ParClass strObjClass = { "string", sizeof(StrObj) };

/*----------------------------------------------------------------------------*/
static void StrObjParDef(void *object)
{
  StrObj *so = ParCast(&strObjClass, object);
  ParName("");
  ParAccess(usUser);
  ParSave(1);
  ParList(NULL);
  ParStr(&so->str, NULL);
}

/*----------------------------------------------------------------------------*/
static int StrObjInit(SConnection * con, int argc, char *argv[],
                      int dynamic)
{
  StrObj *so = NULL;
  char *creationCmd = NULL;

  if (dynamic) {
    creationCmd = Arg2Tcl(argc, argv, NULL, 0);
  }
  so = ParMake(con, argv[1], &strObjClass, StrObjParDef, creationCmd);
  if (so) {
    so->str = NULL;
  }
  return so != NULL;
}

/*----------------------------------------------------------------------------*/
void StrObjStartup(void)
{
  ParMakeClass(&strObjClass, NULL);
  MakeDriver("string", StrObjInit, 0, "String Object");
}
