/*--------------------------------------------------------------------------
  This is a driver for the TDC histogram memory as supplied with the
  Risoe instruments. This system uses a Z80 processor internally. 
  Communication is via a GPIB interface. For more information on this
  device consult documentation available from Risoe.

  copyright: see file COPYRIGHT

  Mark Koennecke, February 2003
  --------------------------------------------------------------------------*/
#ifndef TDCHM
#define TDCHM

pHistDriver MakeTDCHM(pStringDict pOption);
#endif
