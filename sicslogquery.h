/**
 * This does the query execution for the new mongodb based sicslog. 
 * 
 * COPYRIGHT GPL
 *
 * Mark Koennecke, February 2016
 */

#ifndef __SICSLOGMONGO
#define __SICSLOGMONGO
#include <mongoc.h>
/*
  callback called with result data
*/
typedef void (*ResultCallback_t)(const bson_t *doc, void *userData);

/*
  setup of the data base connection and the default instrument
*/
int sicslogSetup(char *url, char *inst);

/*
  do the query. Retruns 0 on success, 1 on failure
*/
int sicslogQuery(int argc, char *argv[], ResultCallback_t func, void *userData);

/*
  If sicslogQuery or sicslogSetup returns an error, retrieve a description
  of it. Only valid right after the error inducing call. 
*/
char *sicslogGetError();


#endif

