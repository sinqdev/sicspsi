
/*------------------------------------------------------------------------
                          A M O R S T A T U S

  Internal data structure definitions for the AMOR status display 
  facilitator object. DO NOT CHANGE. This file is automatically
  created from amorstat.w.

  Mark Koennecke, September 1999
---------------------------------------------------------------------*/

/*---------------------------------------------------------------------*/
   typedef struct {
                     float *fX, *fY;
                     int iNP;
                     char *name;
                  }UserData, *pUserData;                     
/*---------------------------------------------------------------------*/
   typedef struct __AMORSTAT {
                              pObjectDescriptor pDes;
                              pICallBack pCall;
                              int iUserList;
                              pScanData pScan;
                              pHistMem  pHM;
                              int iTOF;
                              int iHTTP;
                             }AmorStat, *pAmorStat;
                

 
