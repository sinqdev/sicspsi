
/*--------------------------------------------------------------------------
  SPS Controller module internal header file. Do not include. Leave 
  alone. Is automatically created from another file.
---------------------------------------------------------------------------*/

   typedef struct {
                   int iByte;
                   int iBit;
                   int iPrivilege;
                  }Permission, *pPermission;



   typedef struct __SPS {
                           pObjectDescriptor pDes;
                           int iMode;
                           int iLastError;
                           char *pHost;
                           int iPort;
                           int iChannel;
                           int lPermissions;
                           void *pData;
                         } SPS;

      
