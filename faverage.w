\subsection{The FOCUS Averager}
This is a special object for the instrument FOCUS and its Status display
client. In the FOCUS status display the averaged data from a number of
detectors is displayed. Thus there is already a reduced form of data. The
actual raw data would be 150*1024*4 bytes of data and possibly more. Rather
then transporting all this data to the status display client at regular
intervalls it was choosen to implement this averaging process at the server
and only send the reduced form to the status display client. Which is two
arrays of floating point data 1024 items long. This little object implements this
averager. 

As all SICS objects this object has a little data structure:
\begin{verbatim}
  typedef struct __FocusAverager {
                                  pObjectDescriptor pDes;
                                  pHistMem          pHist;
                                 } FocusAverager;
\end{verbatim}  

The two fields are the standard object descriptor and a pointer to the
histogram memory object holding the data. 

The interface is minimal: it consists just of the factory function for
installing this object into SICS and the actual function doing the
averaging in the interpreter.

@d faint @{
    int MakeFA(SConnection *pCon, SicsInterp *pSics, void *pData,
               int argc, char *argv[]);

    int FocusAverageDo(SConnection *pCon, SicsInterp *pSics, void *pData,
               int argc, char *argv[]);

@} 

@o faverage.h @{
/*-----------------------------------------------------------------------
                F o c u s A v e r a g e

  An averager for FOCUS data.  See faverage.tex for more details.

  Mark Koennecke, October 1998

--------------------------------------------------------------------------*/
#ifndef FOCUSAVERAGE
#define FOCUSAVERAGE
@<faint@>
#endif
@}

