# --------------------------------------------------------------------------
# Initialization script for testing the DELcam driver for SICS
#
# Started: Dr. Mark Koennecke, March 2007
#---------------------------------------------------------------------------
#                        O P T I O N S

# --------------- Initialize Tcl internals --------------------------------

# first all the server options are set

ServerOption ReadTimeOut  10
# timeout when checking for commands. In the main loop SICS checks for
# pending commands on each connection with the above timeout, has 
# PERFORMANCE impact!

ServerOption AcceptTimeOut  10 
# timeout when checking for connection req.
# Similar to above, but for connections

ServerOption ReadUserPasswdTimeout  500000 
# time to wiat for a user/passwd to be sent from a client. Increase this
# if there is a problem connecting to a server due to network overload\

ServerOption ServerPort 2911
# the port number the server is going to listen at. The client MUST know
# this number in order to connect. It is in client.ini

ServerOption InterruptPort 2913
# The UDP port where the server will wait for Interrupts from clients.
# Obviously, clients wishing to interrupt need to know this number.

ServerOption TelnetPort 1301
ServerOption TelWord sicslogin

#---------------------------------------------------------------------------
#                        U S E R S
# Syntax: SicsUser name password userRightsCode
SicsUser Spy    007       1

#----------------- Counters -------------------------------------------------
MakeCounter counter regress
counter setpar errortype 1 0
counter setpar recover 1 1
#------------------------------
proc SICSValue {command} {
	set txt [eval $command]
	set l [split $txt =]
	return [string trim [lindex $l 1]]
}
#-------------------------------------------------------------------------
# Histogram Memory 
#------------------------------------------------------------------------
MakeHM hm delcam
hm configure rank 2
hm configure dim0 769
hm configure dim1 512
hm configure amplictrl 1
hm configure shutterctrl 2
hm configure clear 1
hm configure counter counter
hm configure init 1
hm init
#------------------------------------------------------------------------
# SicsData
#------------------------------------------------------------------------
sicsdatafactory new data
#------------------------------------------------------------------------
# Adapters for SansStatus 
#------------------------------------------------------------------------
proc hmframe {no {update 0}} {
	set dim [SICSValue "hm configure dim0"]
	data putint 0 $dim
	set dim [SICSValue "hm configure dim1"]
	data putint 1 $dim
	data copyhm 2 hm
	data writeuu framedata
}
#---------------------------------------------------------------------------
proc statusinfo {} {
	return "Dummy info for DELcam"
}
#----------------------------------------------------------------------------
Publish hmframe Spy
Publish statusinfo Spy

restore
