
/*--------------------------------------------------------------------------
                         S I C S  S E R I A L

  This object adds the serial command to the Tcl-interpreter within SICS.
  Furthermore the commands sr1-sr6 are added as predefined names for 
  possible connections. 

  Mark Koennecke, January 1998
----------------------------------------------------------------------------*/
#ifndef SICSERIAL
#define SICSERIAL
int SerialInit(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
#endif
