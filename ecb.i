
/*-----------------------------------------------------------------------
  The ECB is a rack controller from Risoe based on a Z80 processor. 
  This module provides some functions for communicating with such a
  device. This is an internal data structure definition file.

  copyright: see file COPYRIGHT

  Mark Koennecke, January 2002, with some bits taken out of the
  original tascom code.
------------------------------------------------------------------------*/

        struct __ECB {
                        pObjectDescriptor pDes;
                        pGPIB gpib;
                        int boardNumber;
                        int ecbAddress;
                        int ecbDeviceID;
                        int lastError;
                        int encoder[3];
                        int encoderDirty;
                        }ECB;

