
/*---------------------------------------------------------------------
 AMOR component handling module. For the new (2005) calculation of the
 positions using the beam height as zero.
 
 copyright: see file COPYRIGHT
 
 Mark Koennecke, October  2005
-----------------------------------------------------------------------*/
 #ifndef AMORCOMP
 #define AMORCOMP
 #include <stdio.h>
 #include <sics.h>
 
  typedef struct {
         int activeFlag;  /* component present */
         double markOffset; /* offset mark to real */
         double scaleOffset; /* offset of the scale */
         double readPosition; /* the position as read */
 } amorComp, *pamorComp;
 /*----------------------------------------------------------------------*/
 double calcCompPosition(pamorComp comp);                
 int handleCompCommand(pamorComp comp, SConnection *pCon, 
         int argc, char *argv[]);
 int saveAmorComp(FILE *fd, char *name, char *compname, pamorComp comp);                 
 
 #endif
 
