/*------------------------------------------------------------------------------
                       R \"U N L I S T
                       
     LNS has this special scheme of collecting commands in R\"unbuffers
     (see buffer.h) and execute them from a stack. The R\"unbuffers are
     implemented in buffer.*. This implements the stack.
     
     BEWARE: The R\"unList code does not delete R\"unbuffers! 
     
     Mark Koennecke, January 1996
     
      
     copyright: see implementation file
------------------------------------------------------------------------------*/
#ifndef RUENSTACK
#define RUENSTACK

typedef struct {
  pObjectDescriptor pDes;
  int iList;
} RuenStack, *pRuenStack;

/*----------------------------- live & death --------------------------------*/
pRuenStack CreateRuenStack(void);
void DeleteRuenStack(void *self);

/*----------------------------- what can be done with it --------------------*/
int RuenStackAdd(pRuenStack self, pRuenBuffer pVictim);
int RuenStackUnlink(pRuenStack self, int iLine);
int RuenStackInsert(pRuenStack self, int iLine, pRuenBuffer pVictim);
int RuenStackList(pRuenStack self, SConnection * pCon);
int RuenStackRun(pRuenStack self, SConnection * pCon, SicsInterp * pSics);
int RuenStackBatch(pRuenStack self, SConnection * pCon,
                   SicsInterp * pSics);
/*------------------------- the command -------------------------------------*/
int RuenStackAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);

#endif
