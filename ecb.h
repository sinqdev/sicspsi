
/*-----------------------------------------------------------------------
  The ECB is a rack controller from Risoe based on a Z80 processor. 
  This module provides some functions for communicating with such a
  device.

  WARNING: This contains code which may be endian dependent!

  copyright: see file COPYRIGHT

  Mark Koennecke, January 2002, with some bits taken out of the
  original tascom code.
------------------------------------------------------------------------*/
#ifndef ECBCON
#define ECBCON
#include "gpibcontroller.h"


typedef struct {
  unsigned char d;              /* D register in Z80 */
  unsigned char e;              /* E register in Z80 */
  unsigned char b;              /* B register in Z80 */
  unsigned char c;              /* C register in Z80 */
} Z80_reg;

/*-----------------------------------------------------------------------*/

typedef struct __ECB *pECB;

int ecbExecute(pECB self, int func, Z80_reg in, Z80_reg * out);
int ecbRead(pECB self, unsigned short address,
            void *buffer, int byteCount);
int ecbWrite(pECB self, unsigned short address,
             void *buffer, int byteCount);
int ecbDMARead(pECB self, unsigned short address, void *buffer,
               unsigned short byteCount);
void ecbClear(pECB self);
int fixECBError(pECB self);
void ecbErrorDescription(pECB self, char *buffer, int maxBytes);
int ecbAssignEncoder(pECB self, int encoder, int motorNumber);
int ecbLoadEncoder(pECB self);





/*-----------------------------------------------------------------------*/

int MakeECB(SConnection * pCon, SicsInterp * pSics,
            void *pData, int ragc, char *argv[]);


/*----------------------------------------------------------------------
  for byte packing. result must be an 32 bit integer
----------------------------------------------------------------------*/
typedef union {                 /* Used to extract and load data to Z80 regs. */
  unsigned int result;
  struct {
    unsigned char byt0;         /* Least significant byte */
    unsigned char byt1;
    unsigned char byt2;
    unsigned char byt3;         /* Most significant byte */
  } b;
} Ecb_pack;

#endif
