
/* ----------------------------------------------------------------------
 Internal data structure for the switched motor module. For
 documentation see swmotor.tex.

 Mark Koennecke, May 2001
----------------------------------------------------------------------*/

        typedef struct __SWMOT {
                               pObjectDescriptor pDes;
                               pIDrivable pDriv;
                               pMotor pMaster;
                               int *selectedMotor;
                               int myNumber;
                               float positions[3];
                               char slaves[3][80];
                               char *switchFunc;
                               int errCode;
                               } SWmot, *pSWmot;

 
