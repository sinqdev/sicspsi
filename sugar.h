/*---------------------------------------------------------------------------
sugar.h

syntactic sugar for accessing object subvariables

Markus Zolliker, March 2005
----------------------------------------------------------------------------
*/

/* make an sugar object named "name", referring to "alias"
   which is normally an object and a parameter name separated with space
*/
int SugarMake(char *name, char *alias);
