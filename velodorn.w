\subsection{The hardware driver for the Dornier velocity selector at SINQ}
SANS uses a velocity selector provided by Dornier, Germany. This velocity
selector comes with a IBM--compatible PC for control. On this PC there is a
control program for the velocity selector. This program can be operated from
the PC's monitor and keyboard or remotely, from a host computer, 
through a RS--232 connection. The protocoll on this RS--232 connection is a
simple ASCII command protocoll detailed in the documentation delivered by
Dornier. At SINQ this velocity selcector control PC is connected via the
RS--232 connection to a Macintosh computer. This Mac is connected to the
network by TCP/IP. The Mac runs a terminal server program. This program
forwards commands sent by TCP/IP to the apropriate RS--232 port. Due to this
complicated setup, the hardware driver for the Dornier velocity selector has
three layers:
\begin{itemize}
\item A command forwarding layer, which sends command to the selector via
TCP/IP, the MAC and RS--232 and collects answers. This is fairly general
code, used for many SICS hardware drivers.
\item A primitive operations layer which implements simple command on the
selector. This layer is based on code provided by Dr. Emmelmann, HMI. 
\item At the end, there is the actual SICS hardware driver which builds the
interface to the SICS system.
\end{itemize}

The implementation of the Dornier velocity selector driver is further
complicated by the fact that it is very slow in responding to commands
and moreover due to the fact that it has different modes:
\begin{itemize}
\item Starting is just after starting the velocity selector
\item regel is the normal mode.
\item halting is the mode when the velocity selector has been asked to
stop. 
\end{itemize}
Unfortunately the actions necessary to run a Dornier velocity
selector to a desired speed depend on those modes. For instance if the
Dornier velocity
selector is stopped it first needs to be started and then, when a
certain speed has been reached, the command to set the desired
rotation speed can be sent. The necessary mode dependent switching is
done in the DornierStat function.  

@d dh @{
   int GetDornierStatus(void **pData, pDornierStatus pDornier);
   int DornierSend(void **pData, char *pCommand, char *pReply, int iLen);
   int DecodeNewDornierStatus(char *pText, pDornierStatus pDornier);
@}

@o velodorn.h -d @{
/*---------------------------------------------------------------------------
			V E L O D O R N

  a set of utility functions needed in order to communicate with a 
  Dornier velocity selector through the SINQ system. 

  Mark Koennecke, Juli 1997

  updated to support new format fo status messages, Mark Koennecke, July 2003

  copyright: see implementation file.
------------------------------------------------------------------------------*/
#ifndef VELODORN
#define VELODORN
/*-------------------- a data structure for status analysis ---------------*/
 typedef struct {
                 char echo[30]; /* echo of command */
                 char rm[10]; /* operation status: REG == adjusting, STB == idle, 
                         BRE == braking */
                 int nom_rpm; /* envisaged rotation */
                 int cur_rpm; /* actual rotation speed */        
                 int pwr;     /* loss current, only valid after check */
                 float curr;  /* electircal current */
                 int  rot_temp; /* temperature of rotor */
                 int cont_temp; /* temperature of housing */
                 int inl_temp;  /* in temperature of cooling water */
                 int outl_temp; /* temperature of cooling water after 
                                   velocity selector */
                 float cool_wat; /* cooling water flow (l/minute) */
                 float vacuum;  /* vacuum (mbar) */
                 float accel;   /* rotation acceleration (g) */
                 int komm;      /* communication status PC-Host,
                                   0 = enabled,  1 = disabled 
                                */
                 int iHz;
                 } DornierStatus, *pDornierStatus;                                   

/*--------- error codes */
#define NOCOMMAND -300
#define ECHOMISSING -302
#define BADANALYSIS -303
#define STARTTIMEOUT -304
#define INVALIDSTATUS -305
/*------ functions */
@<dh@>

#endif
@}

