/*--------------------------------------------------------------------------
  This is the implementation file for the TASMAD simulation module for
  SICS. The requirement is to make SICS look as much as TASMAD as 
  possible. This includes:
  - TASMAD is variable driven
  - Sometimes variables are accessed in storage order.
  - A complicated calculation has to be done for getting the instruments
    settings right. The appropriate F77 routine from TASMAD will be 
    reused.
  - The scan logic is different.
  - Output of ILL-formatted data files is required. 

  Mark Koennecke, November 2000
---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <fortify.h>
#include <sics.h>
#include <sicsvar.h>
#include <counter.h>
#include <motor.h>
#include "tas.h"

/*
  As variables may be accessed in storage order, it is necessary to 
  know the order of the motors
*/
extern char *tasMotorOrder[] = { "A1",
  "A2",
  "A3",
  "A4",
  "A5",
  "A6",
  "MCV",
  "SRO",
  "ACH",
  "MTL",
  "MTU",
  "STL",
  "STU",
  "ATL",
  "ATU",
  "MGL",
  "SGL",
  "SGU",
  "AGL",
  NULL
};

/*
  In order to initialise the variable array in the TAS data structure we
  need to know the names of the variables in the right order. This order
  has to match the order defined in tas.h through the defines.
*/
char *tasVariableOrder[] = {
  "WAV",
  "DM",
  "DA",
  "ETAM",
  "ETAA",
  "TI",
  "MN",
  "IF1V",
  "IF1H",
  "IF2V",
  "IF2H",
  "HELM",
  "AS",
  "BS",
  "CS",
  "AA",
  "BB",
  "CC",
  "ETAS",
  "AX",
  "AY",
  "AZ",
  "BX",
  "BY",
  "BZ",
  "EI",
  "KI",
  "EF",
  "KF",
  "QH",
  "QK",
  "QL",
  "EN",
  "QM",
  "HX",
  "HY",
  "HZ",
  "DA1",
  "DA2",
  "DA3",
  "DA4",
  "DA5",
  "DA6",
  "DMCV",
  "SRO",
  "DACH",
  "DMTL",
  "DMTU",
  "DSTL",
  "DSTU",
  "DATL",
  "DATU",
  "DMGL",
  "DSGL",
  "DSGU",
  "DAGL",
  "DEI",
  "DKI",
  "DEF",
  "DKF",
  "DQH",
  "DQK",
  "DEN",
  "DQM",
  "DT",
  "SM",
  "SS",
  "SA",
  "FX",
  "NP",
  "F1",
  "F2",
  NULL
};
